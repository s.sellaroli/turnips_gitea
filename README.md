# TurniPS

App per iOS e Android per la gestione dei turni polizia di stato.

### 18-02-2020

- Start progetto per GitLab

### 19-02-2020

- Nuova notifica in Home con possibilità di chiudere il messaggio.
- Corretto caricamento dati `avvioAPP`
- Corretto caricamento dati durante i login
- Aggiunto **fakeload** che visualizza il caricamento dei dati

### 01-03-2020

- Aggiunto nuovo sistema a tags `Creazione turnario`
- Aggiunto il nuovo sistema a tags per `Turni multipli`
- Aggiunta info app  memoria utilizzata e disponibile con dettaglio
- Nuovo modo di inserire lo straordinario in unica schemrata si scegliete il tipo
- Aggiunto in `elimina turni` la possibilità di eliminare anche: Assenze, straordinario o tutto.
- Nuovo sistema di log utente