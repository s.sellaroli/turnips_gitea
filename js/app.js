/*
 *  TurniPS - verione 4.1.5
 *  Author: Salvatore Sellaroli
 *  Date: 01/01/2020
 */
////////////////////////////////////////////////////////////////////////////////
console.log('--------------------->  app.js');

function LogoutUser() {
	localStorage.clear(); //Elimino tutti i dati salvati i localStorage
	location.reload(); // Riavvio l'app
}
////////////////////////////////////////////////////////////////////////////////
function versione() {
	var version = "4.1.8"; // TODO: inserire il numero realase corretto
	$('#build-app').html(version); // Visualizzo il numero di verisione nell'app
	localStorage['versione'] = version; // Salvo il numero di verione i localStorage
}
////////////////////////////////////////////////////////////////////////////////
// Box notifiche
function showToast(msg, classe, durata) {
	$('#box-notifica').addClass(classe);
	$("#box-notifica .msg").html(msg);
	setTimeout(function () {
		$("#box-notifica").removeClass(classe);
	}, durata);
}
////////////////////////////////////////////////////////////////////////////////
function dataAttuale() {
	var date = new Date();
	// Costruisco la data
	var day = date.getDate();
	var month = date.getMonth() + 1;
	var year = date.getFullYear();
	// Aggiungo lo zero ai mesi es. 08 invece che 8
	if (month < 10) month = "0" + month;
	// Aggiungo lo zero ai giorni es. 08 invece che 8
	if (day < 10) day = "0" + day;
	// Assemblo la data finale che sarà es. 2018-08-15
	var today = year + "-" + month + "-" + day;
	return today;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function avvioApp() {
	// Versione build app
	versione();
	var user = window.localStorage.getItem("user");
	var note_versione = window.localStorage.getItem("note-versione");
	var turnario_array = window.localStorage.getItem("turnario-array");
	localStorage['avvisoHome'] = false;

	if (isEmpty(user)) {
		console.log('%c!!!! FARE LOGIN !!!!', 'color:#FFFFFF; background-color: red;');
		myApp.loginScreen();
	} else {
		console.log('%c!!!! LOGIN OK !!!!', 'color:#FFFFFF; background-color: #40CC03;');
		caricaCalendario();
		chekUpdateDB(); // Verifico la versione del database
		logSessioni();
		sms(); // verifico se ci sono nuovi messaggi in chat
		avvisoHome(); // Notifica in home

		setTimeout(function () {
			// Dopo 10 secondi dall'avvio scarico di nuovo tutti i dati
			console.log('%c caricoEventiCalendario() riscarico tutti i dati', 'background: #222; color: #bada55');
			caricoEventiCalendario();
		}, 10000);

		// Visualizza immagini con novità di versione
		setTimeout(function () {
			var note_versione = window.localStorage.getItem("note-versione");
			if (note_versione == 0) {
				myApp.popup('.popup-novita-versione');
				localStorage['note-versione'] = 1;
			}
		}, 1000);
	}
} // avvioApp()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Serve per salvare il db se il messaggio iniziale è stato visto almeno 1 volta. Evitando di visualizzarlo ad ogni login
function notaVersione() {
	var id_utente = localStorage['id_user'];
	localStorage['note-gdpr'] = 1;
	var token = 1;
	$.ajax({
		type: 'POST',
		url: "https://turnips.it/db_sql/nota_versione.php",
		data: "id_utente=" + id_utente + "&token=" + token,
		dataType: "html",
		success: function (value) {
			//console.log(value);
		},
		error: function () {
			console.log("Errore update nota_versione");
		}
	});
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Serve per inserire nel menu i turni personalizzati
function turniUtente() {
	// Carico nelle select i turni personali dell'utente
	$('#orario').append(
		'<option value="NO">Seleziona</option><option value="19.00 - 00.00">19.00 - 00.00</option><option value="13.00 - 19.00">13.00 - 19.00</option><option value="07.00 - 13.00">07.00 - 13.00</option><option value="00.00 - 07.00">00.00 - 07.00</option><option disabled>  Turni Personali  </option>'
	);
	// Controllo su turni Predefiniti
	$("#orario").change(function () {
		$('#ore_notte').val('');
		// Se viene selezionato uno dei due turni standard viene selezionato il Ticket Pasto
		if (($.trim($(this).val()) == "19.00 - 00.00") || $.trim($(this).val()) ==
			"13.00 - 19.00") {
			$('#sl_idennita').find('option[value="sl_ticket_pasto"]').attr('selected',
				true);
			$('#text-smart-select').html('Ticket Pasto');
		} else {
			$('#sl_idennita').find('option[value="sl_ticket_pasto"]').attr('selected',
				false);
			$('#text-smart-select').html('');
		}
	});
	// Controllo su turni personali utente
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app
	$.ajax({
		type: 'POST',
		url: 'https://turnips.it/sql_db/app/3_0/turni_utente.php',
		data: "id_ute=" + id_utente,
		dataType: 'json',
		success: function (value) {
			for (var i = 0; i < value.length; i++) {
				$('#orario').append('<option value="' + value[i].orario_serv +
					'">' + value[i].orario_serv + '</option>');
			} // end for
			// In base al servizio scelto attribuisco i check
			$("#orario").change(function () {
				var nome_turno = $(this).val();
				$.each(value, function (key, value) {
					if (nome_turno == value.orario_serv) {
						// Ore notti
						$('#ore_notte').val(value.ore_notti);
						//  Cheked ticket pasto
						if ($.trim(value.ticket_pasto) == "1") {
							$('#sl_idennita').find('option[value="sl_ticket_pasto"]').attr(
								'selected', true);
							$('#text-smart-select').html('Ticket Pasto');
						} else {
							$('#sl_idennita').find('option[value="sl_ticket_pasto"]').attr(
								'selected', false);
							$('#text-smart-select').html('');
						};
					} //end if
				}); //end .each
			});
		} // end success
	});
} //turniUtente()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Serve per inserire nel menu i servizi personalizzati
function serviziUtente() {
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app
	// Carico nelle select i serivizi personali dell'utente
	$('#tipo_serv,#tipo_servizio_multiplo').append(
		'<option value="NO">Seleziona</option><option value="Volante">Volante</option><option value="Centralino">Centralino</option><option value="Corpo Guardia">Corpo Guardia</option><option value="Agg. Professionale">Agg. Professionale</option><option value="Teste">Teste</option><option disabled>  Servizi Personali  </option>'
	);
	// Controllo turni predefiniti
	$("#tipo_serv").change(function () {
		// valori a default
		$('#sl_idennita').find('option[value="sl_op_fuori"]').attr('selected',
			false);
		$('#sl_idennita').find('option[value="sl_op_pernotto"]').attr('selected',
			false);
		$('#sl_idennita').find('option[value="sl_op_sede"]').attr('selected',
			false);
		$('#sl_idennita').find('option[value="sl_servizio_esterno"]').attr(
			'selected', false);
		$('#sl_idennita').find('option[value="sl_servizio_missione"]').attr(
			'selected', false);
		$('#sl_idennita').find('option[value="sl_reperibilita"]').attr('selected',
			false);
		// Servizi standard
		if ($.trim($(this).val()) == "Volante") {
			$('#sl_idennita').find('option[value="sl_servizio_esterno"]').attr(
				'selected', true);
			$('#text-smart-select').html('Volante');
		} else {
			$('#sl_idennita').find('option[value="sl_servizio_esterno"]').attr(
				'selected', false);
			$('#text-smart-select').html('');
		}
	});
	// Controllo servizi personali
	$.ajax({
		type: 'POST',
		url: 'https://turnips.it/sql_db/app/3_0/servizi_utente.php',
		data: "id_utente=" + id_utente,
		dataType: 'json',
		success: function (value) {
			// Visualizzo i servizi personali nel menu
			for (var i = 0; i < value.length; i++) {
				$('#tipo_serv,#tipo_servizio_multiplo').append('<option value="' + value[i].nome_serv +
					'">' + value[i].nome_serv + '</option>');
			}
			// In base al servizio scelto attribuisco i check
			$("#tipo_serv").change(function () {
				var nome_turno = $(this).val();
				$.each(value, function (key, value) {
					if (nome_turno == value.nome_serv) {
						/////////////////////////////////////////////////////////////////////////////
						// Carico le indennità nella smart select
						if ($.trim(value.o_p_fuori) == "1") {
							$('#sl_idennita').find('option[value="sl_op_fuori"]').attr(
								'selected', true);
						} else {
							$('#sl_idennita').find('option[value="sl_op_fuori"]').attr(
								'selected', false);
							$('#text-smart-select').html('');
						};
						if ($.trim(value.o_p_pernot) == "1") {
							$('#sl_idennita').find('option[value="sl_op_pernotto"]').attr(
								'selected', true);
						} else {
							$('#sl_idennita').find('option[value="sl_op_pernotto"]').attr(
								'selected', false);
							$('#text-smart-select').html('');
						};
						if ($.trim(value.o_p_sede) == "1") {
							$('#sl_idennita').find('option[value="sl_op_sede"]').attr(
								'selected', true);
						} else {
							$('#sl_idennita').find('option[value="sl_op_sede"]').attr(
								'selected', false);
							$('#text-smart-select').html('');
						};
						if ($.trim(value.serv_estern) == "1") {
							$('#sl_idennita').find('option[value="sl_servizio_esterno"]').attr(
								'selected', true);
						} else {
							$('#sl_idennita').find('option[value="sl_servizio_esterno"]').attr(
								'selected', false);
							$('#text-smart-select').html('');
						};
						if ($.trim(value.missione) == "1") {
							$('#sl_idennita').find('option[value="sl_servizio_missione"]').attr(
								'selected', true);
						} else {
							$('#sl_idennita').find('option[value="sl_servizio_missione"]').attr(
								'selected', false);
							$('#text-smart-select').html('');
						};
						if ($.trim(value.reperi) == "1") {
							$('#sl_idennita').find('option[value="sl_reperibilita"]').attr(
								'selected', true);
						} else {
							$('#sl_idennita').find('option[value="sl_reperibilita"]').attr(
								'selected', false);
							$('#text-smart-select').html('');
						};
						/////////////////////////////////////////////////////////////////////////////
					} // if
				}); //end .each
			});
		} // end success
	});
} //serviziUtente()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Salva assenze tramite chiamata ajax
function salvaAssenza() {
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app
	var dati = $("#form_add_assenza").serialize();
	// Validazione del form
	if ($('#giorno_ass').val() === '') {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Non hai inserito la data!",
			"box-errore", "2000");
		return false;
	} else if ($('#tipo_ass').val() === 'NO') {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Non hai inserito il tipo di assenza!",
			"box-errore", "2000");
		return false;
	} else {
		// Loader Bottone
		$('#btn-salva-ass').html(
			'<span style="width:32px; height:32px; margin-top:5px;" class="preloader preloader-white">'
		);
		$('#btn-salva-ass').attr("disabled", true);
		$.ajax({
			type: 'POST',
			url: 'https://turnips.it/db_sql/_add/4_1_5/salva_assenza.php',
			data: dati + "&id_utente=" + id_utente + "&versione=" + ver,
			dataType: 'json',
			success: function (msg) {
				if ($.trim(msg.stato) === 'KO') {
					switch (msg.assenza) {
						case 1:
							var nome_msg = "assenza";
							break;
						default:
							var nome_msg = "turno";
					}
					myApp.alert('Hai gia inserito un ' + nome_msg + ' il ' + date2ita(msg.data), '<span class="rosso">Attenzione!</span>');
					$('#btn-salva-ass').html('Salva');
					$('#btn-salva-ass').attr("disabled", false);
				} else {
					loadAssenze();
					loadTurni();
					$('#form_add_assenza')[0].reset(); //Reset form
					// Visualizzo messaggio di conferma OK
					showToast(
						"<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Dati Salvati!",
						"box-ok", "2000");
					setTimeout(function () {
						$('#btn-salva-ass').html('Salva');
						$('#btn-salva-ass').attr("disabled", false);
						// Data corrente per input date
						document.getElementById('giorno_ass').value = dataAttuale();
						$('#calendar').swipeCalendar('refetchEvents');
					}, 2000);
				} // end
			},
			error: function () {
				console.log('Chiamata ajax fallita!');
			}
		}); //ajax
	} // end else
} // SalvaAssenza()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function salvaTurnoPersonale() {
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app
	// Dati dal form
	var dati = $("#form_add_turno_per").serialize();
	// Loader Bottone
	$('#btn-salva-turno-per').attr("disabled", true);
	$('#btn-salva-turno-per').html(
		'<span style="width:32px; height:32px; margin-top:5px;" class="preloader preloader-white">'
	);
	$.ajax({
		type: 'POST',
		url: 'https://turnips.it/sql_db/app/3_0/insert_turno_personale.php',
		data: dati + "&id_utente=" + id_utente + "&versione=" + ver,
		dataType: 'json',
		success: function (data) {
			$('#form_add_turno_per')[0].reset(); //Reset form
			// Loader Bottone
			$('#btn-salva-turno-per').attr("disabled", false);
			$('#btn-salva-turno-per').html('Salva');
			$.each(data, function (key, value) {
				// Visualizzo messaggio di conferma OK
				showToast(
					"<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Turno Personale Salvato!",
					"box-ok", "2000");
				// Aggiungo il turno alla lista
				$('#turni_li').prepend('<li class="item-content">' +
					'<div class="item-media"><i class="fa fa-clock-o" aria-hidden="true"></i></div>' +
					'<div class="item-inner">' +
					'<div class="item">' + value.orario_serv + '</div>' +
					'<div class="item-after"><i class="fa fa-trash-o" onclick="delTurnoPersonale(this);" aria-hidden="true"></i></div>' +
					'</div>' +
					'</li>'
				);
			}); //end .each
		},
		error: function () {
			console.log('Chiamata ajax fallita!');
		}
	}); //ajax
} // salvaTurnoPersonale()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funziona elimina Turno personalizzato
function delTurnoPersonale(id_li) {
	var id_utente = localStorage['id_user'];
	var id_tm = $(id_li).data('att');
	var li = $(id_li).closest('li');
	myApp.confirm('Vuoi eliminare il turno?', 'TurniPS',
		function () {
			// Quando si preme sul pulsante OK
			$.ajax({
				type: "POST",
				url: 'https://turnips.it/sql_db/app/3_0/elimina_turno_personale.php',
				data: "id_turno=" + id_tm + "&id_utente=" + id_utente,
				dataType: "html",
				success: function (msg) {
					li.css({
						'color': 'white',
						'text-decoration': 'line-through',
						'background-color': '#F44336'
					});
					li.fadeOut(1000, function () {
						li.remove();
					});
					return false;
				},
				error: function () {
					console.log('Chiamata ajax fallita!');
				}
			}); //ajax
		},
		function () {
			//Quando si preseme sul pulsante ANNULLA
		}
	); //  Fine alert scelta ok-annulla
} //delTurnoPersonale(id_li)
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Visualizza i turni personali
function setTurniPersonali() {
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app
	$.ajax({
		type: 'POST',
		url: 'https://turnips.it/sql_db/app/3_0/turni_utente.php',
		data: "id_ute=" + id_utente,
		dataType: 'json',
		success: function (data) {
			$.each(data, function (key, value) {
				$('#turni_li').append('<li class="item-content">' +
					'<div class="item-media"><i class="fa fa-clock-o" aria-hidden="true"></i></div>' +
					'<div class="item-inner">' +
					'<div class="item">' + value.orario_serv + '</div>' +
					'<div class="item-after"><i class="fa fa-trash-o" data-att="' +
					value.id_s_t +
					'" onclick="delTurnoPersonale(this);" aria-hidden="true"></i>' +
					'</div>' +
					'</li>'
				);
			}); //end .each
		}
	});
} //SetTurniPersonali()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Serve per visualizzare l'elenco dei servizi e se premuto sulle freccette visualizza il dettaglio.
function SetServiziPersonali() {
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app
	$.ajax({
		type: 'POST',
		url: 'https://turnips.it/sql_db/app/3_0/servizi_utente.php',
		data: "id_utente=" + id_utente,
		dataType: 'json',
		success: function (data) {
			$.each(data, function (key, value) {
				$('#servizi_li').append('<li class="item-content">' +
					'<div class="item-media"><i class="fa fa fa-taxi" aria-hidden="true"></i></div>' +
					'<div class="item-inner">' +
					'<div class="item">' + value.nome_serv + '</div>' +
					'<div class="item-after"><i class="fa fa-trash-o" data-att="' +
					value.id_serv_sp +
					'" onclick="delServizioPersonale(this);" aria-hidden="true"></i>' +
					'</div>' +
					'</li>'
				);
			}); //end .each
		}
	});
} // SetServiziPersonali()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function salvaServizioPersonale() {
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app
	var dati = $("#form_add_servizio_per").serialize();
	if ($('#nome_serv_sp').val() === '') {
		// Visualizzo messaggio di conferma OK
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Devi Inserire il nome del servizio!",
			"box-errore", "2000");
		return false;
	} else {
		// Loader Bottone
		$('#btn-salva-servizio').attr("disabled", true);
		$('#btn-salva-servizio').html(
			'<span style="width:32px; height:32px; margin-top:5px;" class="preloader preloader-white">'
		);
		$.ajax({
			type: 'POST',
			url: 'https://turnips.it/sql_db/app/3_0/insert_servizio_personale.php',
			data: dati + "&id_utente=" + id_utente + "&versione=" + ver,
			dataType: 'json',
			success: function (data) {
				$('#form_add_servizio_per')[0].reset(); //Reset form
				$.each(data, function (key, value) {
					// Visualizzo messaggio di conferma OK
					// Loader Bottone
					$('#btn-salva-servizio').attr("disabled", false);
					$('#btn-salva-servizio').html('Salva');
					showToast(
						"<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Servizio Personale Salvato!",
						"box-ok", "2000");
					// Aggiungo il turno alla lista
					$('#servizi_li').prepend('<li class="item-content">' +
						'<div class="item-media"><i class="fa fa fa-taxi" aria-hidden="true"></i></div>' +
						'<div class="item-inner">' +
						'<div class="item">' + value.nome_serv + '</div>' +
						'<div class="item-after"><i class="fa fa-trash-o" onclick="delServizioPersonale(this);" aria-hidden="true"></i></div>' +
						'</div>' +
						'</li>'
					);
				}); //end .each
			},
			error: function () {
				console.log('Chiamata ajax fallita!');
			}
		}); //ajax
	} // end else
} // salvaServizioPersonale()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funziona elimina Servizio personalizzato
function delServizioPersonale(id_li) {
	var id_utente = localStorage['id_user'];
	var id_tm = $(id_li).data('att');
	var li = $(id_li).closest('li');
	myApp.confirm('Vuoi eliminare il servizio?', 'TurniPS',
		function () {
			// Quando si preme sul pulsante OK
			$.ajax({
				type: "POST",
				url: 'https://turnips.it/sql_db/app/3_0/elimina_servizio_personale.php',
				data: "id_serv_sp=" + id_tm + "&id_utente=" + id_utente,
				dataType: "html",
				success: function (msg) {
					li.css({
						'color': 'white',
						'text-decoration': 'line-through',
						'background-color': '#F44336'
					});
					li.fadeOut(1000, function () {
						li.remove();
					});
					return false;
				},
				error: function () {
					myApp.alert('Errore di Connessione!', 'TurniPS');
				}
			}); //ajax
		},
		function () {
			//Quando si preseme sul pulsante ANNULLA
		}
	); //  Fine alert scelta ok-annulla
} // delServizioPersonale(id_li)
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Serve per la registrazione di un nuovo utente
function regUtente() {
	var ver = localStorage['versione']; // Versione app
	var dati = $("#form_registra").serialize();
	var user_log = $('#user_r').val();
	var pswd_log = $('#pass_r').val();
	if ($('#user_r').val() === '') {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Non hai inserito username!",
			"box-errore", "2000");
		return false;
	} else if ($('#pass_r').val() === '') {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Non hai inserito una password!",
			"box-errore", "2000");
		return false;
	} else if ($('#pass_r').val().length <= '3') {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;<span style='font-size: 11px;'>La password deve essere composta di almeno 6 caratteri</span>",
			"box-errore", "2000");
		return false;
	} else if ($('#mail_r').val() === '') {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Non hai inserito una email valida",
			"box-errore", "2000");
		return false;
	} else {
		// Loader
		$('#btn_reg').html(
			'<span style="width:32px; height:32px; margin-top:5px;" class="preloader preloader-white">'
		);
		$.ajax({
			type: "POST",
			url: 'https://turnips.it/db_sql/settings/registrazione_utente.php',
			data: dati + "&versione=" + ver,
			dataType: "html",
			success: function (msg) {
				var data = msg;
				if ($.trim(data) === "ok") {
					myApp.popup('.popup-ok-reg');
					// Attivo animazione cerchietto con ok
					setTimeout(function () {
						$('.circle-loader').toggleClass('load-complete');
						$('.checkmark').toggle();
						$('#form_registra').get(0).reset();
						$('#footer-nav').css('display', 'block');
						$('#btn_reg').html('Salva');
						$('#user-login').val(user_log);
						$('#pass-login').val(pswd_log);
					}, 600);
				} else {
					showToast(
						"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;" +
						msg + "", "box-errore", "5000");
					setTimeout(function () {
						$('#btn_reg').html('Salva');
					}, 5000);
				};
			},
			error: function () {
				console.log('Chiamata ajax fallita!');
			}
		}); //ajax
	} // end else
} // regUtente()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Serve per il login
function loginUtente() {
	// Dati dal form
	var dati = $("#form_login").serialize();
	var nomes = $('#user-login').val();
	var passs = $('#pass-login').val();
	// Verifico se user e pass sono stati inseriti
	if (isEmpty(nomes)) {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Non hai inserito la username!",
			"box-errore", "2000");
		return false;
	} else if (isEmpty(passs)) {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Non hai inserito la password!",
			"box-errore", "2000");
		return false;
	} else {
		$('#text-btn-login').html(
			'<span style="width:24px; height:24px" class="preloader preloader-green"></span>'
		);
		// Chiamata ajax
		$.ajax({
			type: "POST",
			url: 'https://turnips.it/db_sql/login.php',
			data: dati,
			dataType: "json",
			success: function (data) {
				// Se i dati ricevuti contiente un id allora vado avanti con il login
				if (data.id != "") {
					// Salvo in localStorage prima id, user e tutti i dati in un array
					localStorage['id_user'] = data.id;
					localStorage['user'] = data.user;
					localStorage['note-versione'] = data.nota_versione;
					localStorage['note-gdpr'] = data.nota_gdpr;
					localStorage['licenza-app'] = data.livello_app;
					localStorage['licenza-app-durata'] = data.durata_licenza;
					localStorage['versione-db-user'] = data.versione_db_user;
					localStorage.setItem("dati-utente", JSON.stringify([data]));
					// Dopo 1/2 sec visualizzo la notifica con avvenuto accesso e carico i dati del calendario
					showToast(
						"<i class=\"fa fa-check-circle fa-lg\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Accesso effettuato...!",
						"box-ok", "2000");
					$('#text-btn-login').html('<i class="fa fa-sign-in"></i>&nbsp;Accedi');
					setTimeout(function () {
						myApp.closeModal(); // Chiusura modal login
						domLoaded(); // Carico la pubblicità
					}, 500);
					setTimeout(function () {
						caricoEventiCalendario(); // Scarico tutti gli eventi (Turni,Assenze,Strao,Turnario)
						fakeLoadEventi(); // Visualizzo a video che sto caricando i dati
					}, 800);
					setTimeout(function () {
						console.log('%c!!!! Calendario post login !!!!', 'color:#FFFFFF; background-color: #2980b9;');
						caricaCalendario(); // Ricarico i calendario con tutti gli eventi scaricati
					}, 4500);
					// Visualizzo il popup con le novità
					setTimeout(function () {
						var note_versione = window.localStorage.getItem("note-versione");
						if (note_versione == 0) {
							myApp.popup('.popup-novita-versione');
							localStorage['note-versione'] = 1;
						}
					}, 3500);
					// Visualizzo avviso se si usa l'account demo
					setTimeout(function () {
						var utente = data.id;
						if (utente == '6') {
							myApp.addNotification({
								title: 'Avviso',
								message: 'Stai utilizzando un account di prova dove tutti hanno accesso. Se ti piace l\'app, ti consiglio di crea un tuo account personale.'
							});
						}
					}, 15000);
				} else {
					// Visualizzo messaggio di errore in caso di errore di login es. password o user errato
					showToast(
						"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Errore Accesso!",
						"box-errore", "2000");
					$('#text-btn-login').html('<i class="fa fa-sign-in"></i>&nbsp;Accedi');
				}
				logSessioni();
			},
			error: function () {
				// Visualizzo messaggio di erroe in caso di errore chiamata ajax
				$('#text-btn-login').html('<i class="fa fa-sign-in"></i>&nbsp;Accedi');
				showToast(
					"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Errore Accesso!",
					"box-errore", "2000");
			}
		}); //ajax
	}
} // loginUtente()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Serve per recuperare la password di accesso
function RecPassword() {
	// Variabili Globali
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app
	var email = $("#email-rec").val();
	var motivo = $("#motivo_reset_psw").val();
	if (isEmpty(email)) {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Devi inserire un email valida!",
			"box-errore", "2500");
		$('#notifica-rec-password').addClass("box-errore").html();
		return false;
	} else if (motivo == 'no') {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Seleziona un motivo",
			"box-errore", "2500");
		$('#notifica-rec-password').addClass("box-errore").html();
		return false;
	} else {
		// Loader Bottone
		$('#btn-rec-pass').html(
			'<span style="width:32px; height:32px; margin-top:5px;" class="preloader preloader-white">'
		);
		$.ajax({
			type: "POST",
			url: 'https://turnips.it/db_sql/settings/recupero_password.php',
			data: "id_utente=" + id_utente + "&versione=" + ver + "&email=" + email + "&motivo=" + motivo,
			dataType: "html",
			success: function (msg) {
				var data = msg;
				if ($.trim(data) === "NO") {
					showToast(
						"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;E-Mail non presente!",
						"box-errore", "5000");
					$('#btn-rec-pass').html('Invia');
				} else {
					showToast(
						"<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Operazione competata!",
						"box-ok", "2000");
					$('#ok-rec-pass').css('display', 'block');
					$('#btn-rec-pass').html('Invia');
				}
			},
			error: function () {
				console.log('Chiamata ajax fallita!');
			}
		}); //ajax
	}
} // RecPassword()
// Serve per azzerare il campo input e nascondere il div con il messaggio di ok.
// Quando si clicca sul pulsante indietro.
function resetRecPassword() {
	$('#ok-rec-pass').css('display', 'none');
	$("#email-rec").val('');
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function datiPersonali() {
	var dati = JSON.parse(localStorage.getItem("dati-utente"));
	var licenza_app = window.localStorage.getItem('licenza-app');
	var licenza_app_durata = window.localStorage.getItem('licenza-app-durata');
	$.each(dati, function (key, value) {
		$("#nome_u").val(value.nome);
		$("#cognome_u").val(value.cognome);
		$('#username_u').val(value.user);
		$("#email_u").val(value.mail);
		$("#luogo_u").val(value.luogo);
		$("#grado_u").val(value.grado);
		$('#livello_app_u').html(licenza_app);
		if (licenza_app == "Premium") {
			$('#end_premium_app').html('&ensp;Fino al&nbsp;' +  ((isEmpty(licenza_app_durata)) ? '' : licenza_app_durata));
		}
	}); //end .each*/
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funzione per aggiornare i dati personali del utente
function editUser() {
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app
	var dati = $("#form_edit_dati_utente").serialize();
	// Loader
	$('#btn_edit_user').html(
		'<span style="width:32px; height:32px; margin-top:5px;" class="preloader preloader-white">'
	);
	$.ajax({
		type: "POST",
		url: 'https://turnips.it/db_sql/settings/edit_dati_user.php',
		data: dati + "&id_utente=" + id_utente + "&versione=" + ver,
		dataType: "json",
		success: function (msg) {
			showToast(
				"<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Dati Aggiornati!",
				"box-ok", "2000");
			localStorage.setItem("dati-utente", JSON.stringify([msg]));
			setTimeout(function () {
				$("#btn_edit_user").html("Aggiorna");
			}, 2000);
		},
		error: function () {
			console.log('Chiamata ajax fallita!');
		}
	}); //ajax
} // editUser()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funzione per cambiare la password del utente
function cambiaPassword() {
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app
	var dati = $("#form_cambia_password").serialize();
	if ($('#pass_old').val() === '') {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;Non hai la password vecchia!",
			"box-errore", "2000");
		return false;
	} else if ($('#pass_new').val() === '') {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;Non hai inserito la nuova password!",
			"box-errore", "2000");
		return false;
	} else if (id_utente === '6') {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;Non puoi cambiare la password DEMO",
			"box-errore", "2000");
		return false;
	} else {
		// Loader
		$('#btn_cambia_pass').html(
			'<span style="width:32px; height:32px; margin-top:5px;" class="preloader preloader-white">'
		);
		$.ajax({
			type: "POST",
			url: 'https://turnips.it/sql_db/app/3_0/edit_pass_user.php',
			data: dati + "&id_utente=" + id_utente,
			dataType: "html",
			success: function (msg) {
				if ($.trim(msg) === "NO") {
					showToast(
						"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;La vecchia password non è corretta!",
						"box-errore", "5000");
					$("#btn_cambia_pass").html("Salva");
				} else {
					showToast(
						"<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Password Cambiata!",
						"box-ok", "2000");
					setTimeout(function () {
						$("#btn_cambia_pass").html("Salva");
					}, 2000);
				}
			},
			error: function () {
				console.log('Chiamata ajax fallita!');
			}
		}); //ajax
	}
} // cambiaPassword()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function eliminaAccount() {
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app
	var dati = $("#form_elimina_account").serialize();
	// Validazione del form
	if (id_utente === '6') {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;Non puoi eliminare l'account DEMO!",
			"box-errore", "2000");
		return false;
	} else {
		// Loader
		$('#btn_elimina_user').html(
			'<span style="width:32px; height:32px; margin-top:5px;" class="preloader preloader-white">'
		);
		$.ajax({
			type: "POST",
			url: 'https://turnips.it/db_sql/_delete/elimina_utente.php',
			data: dati + "&id_utente=" + id_utente,
			dataType: "html",
			success: function (msg) {
				if ($.trim(msg) === "NO") {
					showToast(
						"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;La  password non è corretta!",
						"box-errore", "4000");
					setTimeout(function () {
						$("#btn_elimina_user").html("Elimina il mio account");
					}, 4000);
				} else {
					showToast(
						"<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Account Eliminato!",
						"box-ok", "2000");
					setTimeout(function () {
						$("#btn_elimina_user").html("Elimina il mio account");
						localStorage.clear();
					}, 2000);
					setTimeout("window.location.reload()", 2500); //end .timeout
				}
			},
			error: function () {
				console.log('Chiamata ajax fallita!');
			}
		}); //ajax
	} // else
} // eliminaAccount()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Salva o aggiorna le impostazioni in Gestione Assenze
function salvaSettingAss() {
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app
	var dati = $("#form_setting_assenze").serialize();
	// Loader
	$('#btn_setting_ass').html(
		'<span style="width:32px; height:32px; margin-top:5px;" class="preloader preloader-white">'
	);
	$.ajax({
		type: "POST",
		url: 'https://turnips.it/db_sql/settings/edit_assenze.php',
		data: dati + "&id_utente=" + id_utente,
		dataType: "html",
		success: function (msg) {
			showToast(
				"<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Dati Salvati!",
				"box-ok", "2000");
			//localStorage.setItem("dati-utente", JSON.stringify([msg]));
			setTimeout(function () {
				$("#btn_setting_ass").html("Salva");
			}, 2000);
		},
		error: function () {
			console.log('Chiamata ajax fallita!');
		}
	}); //ajax
} //salvaSettingAss()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Carica i dati in gestione assenze
function caricaSettingAss() {
	var id_utente = localStorage['id_user'];
	$.ajax({
		type: "POST",
		url: 'https://turnips.it/db_sql/settings/assenze.php',
		data: "id_utente=" + id_utente,
		dataType: "json",
		success: function (msg) {
			$.each(msg, function (key, value) {
				$("#c_o_pre_s").val(value.c_o_prec);
				$("#c_o_corrente_s").val(value.c_o_corrente);
				$("#rec_riposo_s").val(value.rec_riposo);
				$("#rec_lex_s").val(value.rec_lex);
				$("#c_s_s").val(value.c_s);
				$("#aspettativa_s").val(value.aspettativa);
				$("#malattia_bimbo_s").val(value.malattia_bimbo);
				$("#parentale_s").val(value.parentale);
				$("#rec_riposo_s").val(value.rec_riposo);
				// Visualizzo il totale
				$("#c_o_pre_s-tot").html(value.c_o_prec);
				$("#c_o_corrente_s-tot").html(value.c_o_corrente);
				$("#rec_lex_s-tot").html(value.rec_lex);
				$("#c_s_s-tot").html(value.c_s);
				$("#aspettativa_s-tot").html(value.aspettativa);
				$("#malattia_bimbo_s-tot").html(value.malattia_bimbo);
				$("#parentale_s-tot").html(value.parentale);
				$('#label-scadenza-parentale').html('Scadenza il: ' + date2ita(value.scadenza_parentale));
				$('#scad_parentale').val(value.scadenza_parentale);
				$("#rec_riposo_s-tot").html(value.rec_riposo);
				// Imposto il valore max della progress bar, in base al profilo utente
				$("#pr-c-o-a-p").attr('max', value.c_o_prec);
				$("#pr-c-o-c").attr('max', value.c_o_corrente);
				$("#pr-rec-lex").attr('max', value.rec_lex);
				$("#pr-c-s-s").attr('max', value.c_s);
				$("#pr-aspettativa").attr('max', value.aspettativa);
				$("#pr-malattia-bimbo").attr('max', value.malattia_bimbo);
				$("#pr-parentale").attr('max', value.parentale);
				$("#pr-rec-riposo").attr('max', value.rec_riposo);
				// Data santo patrono
				var str = value.santo_patrono;
				var myarr = str.split("-");
				if (str == '0000-00-00') {
					var nome_mese = '';
					var anno = '';
					$('#picker-describe').val(anno + " " + nome_mese);
				} else {
					switch (myarr[1]) {
						case '01':
							var nome_mese = 'Gennaio';
							break;
						case '02':
							var nome_mese = 'Febbraio';
							break;
						case '03':
							var nome_mese = 'Marzo';
							break;
						case '04':
							var nome_mese = 'Aprile';
							break;
						case '05':
							var nome_mese = 'Maggio';
							break;
						case '06':
							var nome_mese = 'Giugno';
							break;
						case '07':
							var nome_mese = 'Luglio';
							break;
						case '08':
							var nome_mese = 'Agosto';
							break;
						case '09':
							var nome_mese = 'Settembre';
							break;
						case '10':
							var nome_mese = 'Ottobre';
							break;
						case '11':
							var nome_mese = 'Novembre';
							break;
						case '12':
							var nome_mese = 'Dicembre';
							break;
					}
					$('#picker-describe').val(myarr[2] + " " + nome_mese);
				}
			});
		},
		error: function () {
			console.log('Chiamata ajax fallita!');
		}
	}); //ajax
} //caricaSettingAss()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function caricaAssenze() {
	var id_utente = localStorage['id_user'];
	$.ajax({
		type: 'POST',
		url: 'https://turnips.it/db_sql/riepilogo/assenze.php',
		data: {
			id_utente: id_utente
		},
		dataType: 'json',
		success: function (data) {
			countNumber();
			// Usufruiti
			$("#ass-coa").html(data.coa);
			$("#ass-coc").html(data.coc);
			$("#ass-rip-legge").html(data.riposo_legge);
			$("#ass-cong-strao-mala").html(data.con_malattia);
			$("#ass-aspettativa").html(data.aspettativa);
			$("#ass-parentale").html(data.parentale);
			$('#ass-recupero_riposo').html(data.recupero_risposo_close);
			// Spettanti
			$("#set-ass-coa").html(data.set_coa);
			$("#set-ass-coc").html(data.set_coc);
			$("#set-ass-rip-legge").html(data.set_riposo_legge);
			$("#set-ass-cong-strao-mala").html(data.set_con_malattia);
			$("#set-ass-aspettativa").html(data.set_aspettativa);
			$("#set-ass-parentale").html(data.set_parentale);
			// Disponibili
			$("#disp-ass-coa").html(data.disp_coa);
			$("#disp-ass-coc").html(data.disp_coc);
			$("#disp-ass-rip-legge").html(data.disp_riposo_legge);
			$("#disp-ass-cong-strao-mala").html(data.disp_con_malattia);
			$("#disp-ass-aspettativa").html(data.disp_aspettativa);
			$("#disp-ass-parentale").html(data.disp_parentale);
			$('#disp-ass-recupero-riposo').html(data.giorno_recupero);
			// Assenze Singole
			$('#ass-malattia-bimbo').html(data.bimbo);
			$('#ass-dona-sangue').html(data.sangue);
			$('#ass-lex-104').html(data.l104);
			$('#ass-studio').html(data.studio);
			$('#ass-mandato-am').html(data.mandato);
			if (!isEmpty(data.scadenza_parentale)) {
				$('#data-scadenza-parentale').html('Scadenza il: ' + date2ita(data.scadenza_parentale));
			}
			$('#anno_idennita_assenze').val(data.anno_assenze);
		} //end success
	}) // end ajax
} //caricaAssenze()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function inndenitaMese() {
	var id_utente = localStorage['id_user'];
	$('#ore_notte_r').html('<span class="preloader preloader-red"></span>');
	$('#ore_stra_r').html('<span class="preloader preloader-red"></span>');
	$('#serv_esterno_r').html('<span class="preloader preloader-red"></span>');
	$('#ticket_pasto_r').html('<span class="preloader preloader-red"></span>');
	$('#cambio_turno_r').html('<span class="preloader preloader-red"></span>');
	$('#recuper_riposo_r').html('<span class="preloader preloader-red"></span>');
	$('#rip_festivo').html('<span class="preloader preloader-red"></span>');
	$('#o_p_r').html('<span class="preloader preloader-red"></span>');
	$('#o_p_fuori_r').html('<span class="preloader preloader-red"></span>');
	$('#o_p_pernotto_r').html('<span class="preloader preloader-red"></span>');
	$('#pres_festivo_r').html('<span class="preloader preloader-red"></span>');
	$('#reperibilita_r').html('<span class="preloader preloader-red"></span>');
	$('#super_festivo').html('<span class="preloader preloader-red"></span>');
	$('#serv_missione').html('<span class="preloader preloader-red"></span>');
	$('#sl_autostrada').html('<span class="preloader preloader-red"></span>');
	$('#rip_com_r').html('<span class="preloader preloader-red"></span>');
	$('#mancati_pasto_r').html('<span class="preloader preloader-red"></span>');
	$.ajax({
		type: 'POST',
		url: 'https://turnips.it/db_sql/riepilogo/carico_indennita.php',
		data: "id_utente=" + id_utente,
		dataType: 'json',
		success: function (value) {

			setTimeout(function () {
				$('#ore_notte_r').html('<div class="animated bounceIn">' + value.a +
					'</div>');
				$('#ore_stra_r').html('<div class="animated bounceIn">' + value.b +
					'</div>');
				$('#serv_esterno_r').html('<div class="animated bounceIn">' + value.f +
					'</div>');
				$('#ticket_pasto_r').html('<div class="animated bounceIn">' + value.c +
					'</div>');
				$('#cambio_turno_r').html('<div class="animated bounceIn">' + value.d +
					'</div>');
				$('#recuper_riposo_r').html('<div class="animated bounceIn">' + value.e +
					'</div>');
				$('#rip_festivo').html('<div class="animated bounceIn">' + value.festivo +
					'</div>');
				$('#o_p_r').html('<div class="animated bounceIn">' + value.g +
					'</div>');
				$('#o_p_fuori_r').html('<div class="animated bounceIn">' + value.h +
					'</div>');
				$('#o_p_pernotto_r').html('<div class="animated bounceIn">' + value.i +
					'</div>');
				$('#pres_festivo_r').html('<div class="animated bounceIn">' + value.n +
					'</div>');
				$('#reperibilita_r').html('<div class="animated bounceIn">' + value.m +
					'</div>');
				$('#super_festivo').html('<div class="animated bounceIn">' + value.o +
					'</div>');
				$('#serv_missione').html('<div class="animated bounceIn">' + value.p +
					'</div>');
				$('#rip_com_r').html('<div class="animated bounceIn">' + value.q +
					'</div>');
				$('#mancati_pasto_r').html('<div class="animated bounceIn">' + value.mp +
					'</div>');
				$('#sl_autostrada').html('<div class="animated bounceIn">' + value.sl_autostrada +
					'</div>');
				$('#anno_idennita-s').val(value.anno);
				$('#mese_idennita-s').val(value.mese);
			}, 500);
		}
	});
} // inndenitaMese()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Visualizza il riepilogo mensile delle indennità. Il mese corrente o uno scelto dal menu
function inndenitaMeseScelta() {
	var id_utente = localStorage['id_user'];
	$("#mese_idennita-s").change(function () {

		var anno = $('#anno_idennita-s').val();
		var mese = $('#mese_idennita-s').val();
		//Loader
		$('#ore_notte_r').html('<span class="preloader preloader-red"></span>');
		$('#ore_stra_r').html('<span class="preloader preloader-red"></span>');
		$('#serv_esterno_r').html('<span class="preloader preloader-red"></span>');
		$('#ticket_pasto_r').html('<span class="preloader preloader-red"></span>');
		$('#cambio_turno_r').html('<span class="preloader preloader-red"></span>');
		$('#recuper_riposo_r').html('<span class="preloader preloader-red"></span>');
		$('#rip_festivo').html('<span class="preloader preloader-red"></span>');
		$('#o_p_r').html('<span class="preloader preloader-red"></span>');
		$('#o_p_fuori_r').html('<span class="preloader preloader-red"></span>');
		$('#o_p_pernotto_r').html('<span class="preloader preloader-red"></span>');
		$('#pres_festivo_r').html('<span class="preloader preloader-red"></span>');
		$('#reperibilita_r').html('<span class="preloader preloader-red"></span>');
		$('#super_festivo').html('<span class="preloader preloader-red"></span>');
		$('#serv_missione').html('<span class="preloader preloader-red"></span>');
		$('#rip_com_r').html('<span class="preloader preloader-red"></span>');
		$('#mancati_pasto_r').html('<span class="preloader preloader-red"></span>');
		$.ajax({
			type: 'POST',
			url: 'https://turnips.it/sql_db/app/3_0/carico_indennita_scelta.php',
			data: {
				id_utente: id_utente,
				anno: anno,
				mese: mese
			},
			dataType: 'json',
			success: function (value) { // setTimeput per visualizzare loader
				// Ore straordinario
				var str1 = value.ore_stra_time.split(':');
				var st1 = str1[0];
				var st2 = str1[1];

				setTimeout(function () {
					$('#ore_notte_r').html('<div class="animated bounceIn">' + value.notte +
						'</div>');
					$('#ore_stra_r').html('<div class="animated bounceIn">' + st1 +
						' : ' + st2 + '</div>');
					$('#ticket_pasto_r').html('<div class="animated bounceIn">' + value
						.pasto + '</div>');
					$('#cambio_turno_r').html('<div class="animated bounceIn">' + value
						.cambio + '</div>');
					$('#recuper_riposo_r').html('<div class="animated bounceIn">' +
						value.recupero + '</div>');
					$('#rip_festivo').html('<div class="animated bounceIn">' + value.festivo +
						'</div>');
					$('#serv_esterno_r').html('<div class="animated bounceIn">' + value
						.esterno + '</div>');
					$('#o_p_r').html('<div class="animated bounceIn">' + value.op +
						'</div>');
					$('#o_p_fuori_r').html('<div class="animated bounceIn">' + value.op_f +
						'</div>');
					$('#o_p_pernotto_r').html('<div class="animated bounceIn">' + value
						.op_per + '</div>');
					$('#pres_festivo_r').html('<div class="animated bounceIn">' + value
						.aa + '</div>');
					$('#reperibilita_r').html('<div class="animated bounceIn">' + value
						.ab + '</div>');
					$('#super_festivo').html('<div class="animated bounceIn">' + value.ac +
						'</div>');
					$('#serv_missione').html('<div class="animated bounceIn">' + value.ad +
						'</div>');
					$('#rip_com_r').html('<div class="animated bounceIn">' + value.ae +
						'</div>');
					$('#mancati_pasto_r').html('<div class="animated bounceIn">' +
						value.mp + '</div>');
					$('#view_mese').html(value.mese_ita);
				}, 1000);
			}
		});
	});
} // inndenitaMeseScelta
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funziona elimina Turno personalizzato
function eliminaAssenza() {
	var dati = $("#form_edit_assenza").serialize();
	var id_utente = localStorage['id_user'];
	myApp.confirm('Vuoi eliminare l\'assenza?', 'TurniPS',
		function () {
			// Quando si preme sul pulsante OK
			// Loader
			$('#btn-elimina-ass').html(
				'<span style="width:32px; height:32px; margin-top:5px;" class="preloader preloader-white">'
			);
			$.ajax({
				type: "POST",
				url: 'https://turnips.it/db_sql/_delete/elimina_assenza.php',
				data: dati + "&id_utente=" + id_utente,
				dataType: "html",
				success: function (msg) {
					loadAssenze();
					// Visualizzo messaggio di conferma OK
					showToast(
						"<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Assenza Eliminata!",
						"box-ok", "2000");
					setTimeout(function () {
						$('#btn-elimina-ass').html('<i class="fa fa-trash-o"></i> Elimina');
						$('#calendar').swipeCalendar('refetchEvents');
					}, 2000);
					setTimeout(function () {
						mainView.router.loadPage('index.html');
					}, 2200);
				},
				error: function () {
					console.log('Chiamata ajax fallita!');
				}
			}); //ajax
		},
		function () {
			//Quando si preseme sul pulsante ANNULLA
		}
	); //  Fine alert scelta ok-annulla
} //eliminaAssenza()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function aggiornaAssenza() {
	var dati = $("#form_edit_assenza").serialize();
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app
	myApp.confirm('Vuoi aggiornare l\'assenza?', 'TurniPS',
		function () {
			// Quando si preme sul pulsante OK
			// Loader
			$('#btn-aggiorna-ass').html(
				'<span style="width:32px; height:32px; margin-top:5px;" class="preloader preloader-white">'
			);
			$.ajax({
				type: "POST",
				url: 'https://turnips.it/db_sql/_edit/aggiorna_assenza.php',
				data: dati + "&id_utente=" + id_utente + "&versione=" + ver,
				dataType: "html",
				success: function (msg) {
					loadAssenze();
					// Visualizzo messaggio di conferma OK
					showToast(
						"<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Assenza Aggiornata!",
						"box-ok", "2000");
					setTimeout(function () {
						$("#last_up_ass").html('Ultima modifica effettuata il: ' + msg);
						$('#btn-aggiorna-ass').html(
							'<i class="fa fa-refresh"></i> Aggiorna');
						$('#calendar').swipeCalendar('refetchEvents');
					}, 2000);
				},
				error: function () {
					console.log('Chiamata ajax fallita!');
				}
			}); //ajax
		},
		function () {
			//Quando si preseme sul pulsante ANNULLA
		}
	); //  Fine alert scelta ok-annulla
} //aggiornaAssenza()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funziona elimina Scalo straordinario
function eliminaScalo() {
	var dati = $("#form_edit_scalo").serialize();
	var id_utente = localStorage['id_user'];
	myApp.confirm('Vuoi eliminare l\'assenza?', 'TurniPS',
		function () {
			// Quando si preme sul pulsante OK
			// Loader
			$('#btn-elimina-scalo').html(
				'<span style="width:32px; height:32px; margin-top:5px;" class="preloader preloader-white">'
			);
			$.ajax({
				type: "POST",
				url: 'https://turnips.it/sql_db/app/3_0/elimina_scalo.php',
				data: dati + "&id_utente=" + id_utente,
				dataType: "html",
				success: function (msg) {
					loadTurni();
					// Visualizzo messaggio di conferma OK
					showToast(
						"<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Assenza Eliminata!",
						"box-ok", "2000");
					setTimeout(function () {
						$('#btn-elimina-scalo').html(
							'<i class="fa fa-trash-o"></i> Elimina');
						$('#calendar').swipeCalendar('refetchEvents');
					}, 2000);
					setTimeout(function () {
						mainView.router.loadPage('index.html');
					}, 2200);
				},
				error: function () {
					console.log('Chiamata ajax fallita!');
				}
			}); //ajax
		},
		function () {
			//Quando si preseme sul pulsante ANNULLA
		}
	); //  Fine alert scelta ok-annulla
} //eliminaScalo()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function aggiornaScalo() {
	var dati = $("#form_edit_scalo").serialize();
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app
	myApp.confirm('Vuoi aggiornare l\'assenza?', '',
		function () {
			// Quando si preme sul pulsante OK
			// Loader
			$('#btn-aggiorna-scalo').html(
				'<span style="width:32px; height:32px; margin-top:5px;" class="preloader preloader-white">'
			);
			$.ajax({
				type: "POST",
				url: 'https://turnips.it/sql_db/app/3_5/aggiorna_scalo.php',
				data: dati + "&id_utente=" + id_utente + "&versione=" + ver,
				dataType: "html",
				success: function (msg) {
					//console.log(msg);
					loadTurni();
					// Visualizzo messaggio di conferma OK
					showToast(
						"<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Assenza Aggiornata!",
						"box-ok", "2000");
					setTimeout(function () {
						$("#last_up_scalo").html('Ultima modifica effettuata il: ' + msg);
						$('#btn-aggiorna-scalo').html(
							'<i class="fa fa-refresh"></i> Aggiorna');
						$('#calendar').swipeCalendar('refetchEvents');
					}, 2000);
				},
				error: function () {
					console.log('Chiamata ajax fallita!');
				}
			}); //ajax
		},
		function () {
			//Quando si preseme sul pulsante ANNULLA
		}
	); //  Fine alert scelta ok-annulla
} //aggiornaScalo()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funziona elimina Turno
function eliminaTurno() {
	var dati = $("#form_edit_turno").serialize();
	var id_utente = localStorage['id_user'];
	myApp.confirm('Vuoi eliminare il Turno?', 'TurniPS',
		function () {
			// Quando si preme sul pulsante OK
			// Loader
			$('#btn-elimina-turno').html(
				'<span style="width:32px; height:32px; margin-top:5px;" class="preloader preloader-white">'
			);
			$.ajax({
				type: "POST",
				url: 'https://turnips.it/db_sql/_delete/elimina_turno.php',
				data: dati + "&id_utente=" + id_utente,
				dataType: "html",
				success: function (msg) {

					loadTurni();
					// Visualizzo messaggio di conferma OK
					showToast(
						"<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Turno Eliminato!",
						"box-ok", "2000");
					setTimeout(function () {
						$('#btn-elimina-turno').html(
							'<i class="fa fa-trash-o"></i> Elimina');
						$('#calendar').swipeCalendar('refetchEvents');
					}, 2000);
					setTimeout(function () {
						mainView.router.loadPage('index.html');
					}, 2200);
				},
				error: function () {
					console.log('Chiamata ajax fallita!');
				}
			}); //ajax
		},
		function () {
			//Quando si preseme sul pulsante ANNULLA
		}
	); //  Fine alert scelta ok-annulla
} //eliminaTurno()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Salva turno tramite chiamata ajax
function SalvaTurno() {
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app
	var dati = $("#form_add_turno").serialize();
	// Validazione del form
	if ($('#giorno').val() === '') {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;Non hai inserito la data!",
			"box-errore", "2000");
		return false;
	} else if ($('#orario').val() === 'NO') {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;Non hai inserito l'orario!",
			"box-errore", "2000");
		return false;
	} else if ($('#tipo_serv').val() === 'NO') {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;Non hai inserito il servizio!",
			"box-errore", "2000");
		return false;
	} else {
		// Loader Bottone
		$('#btn-salva-turno').html(
			'<span style="width:32px; height:32px; margin-top:5px;" class="preloader preloader-white">'
		);
		$('#btn-salva-turno').attr("disabled", true);
		$.ajax({
			type: "POST",
			url: 'https://turnips.it/db_sql/_add/4_1_5/salva_turno.php',
			data: dati + "&id_utente=" + id_utente + "&versione=" + ver,
			dataType: "json",
			success: function (msg) {
				switch (msg.turno) {
					case 1:
						var nome_msg = "turno";
						break;
					default:
						var nome_msg = "assenza";
				}
				if ($.trim(msg.stato) === 'KO') {
					var data = $('#giorno').val();
					myApp.alert('Hai gia inserito un ' + nome_msg + ' il ' + date2ita(data), '<span class="rosso">Attenzione!</span>');
					$('#btn-salva-turno').html('Salva');
					$('#btn-salva-turno').attr("disabled", false);
				} else {
					loadTurni();
					$('#form_add_turno')[0].reset(); //Reset form
					// Visualizzo messaggio di conferma OK
					showToast(
						"<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Dati Salvati!",
						"box-ok", "2000");
					setTimeout(function () {
						$('#btn-salva-turno').html('Salva');
						$('#btn-salva-turno').attr("disabled", false);
						// Data corrente per input date
						document.getElementById('giorno').value = dataAttuale();
						$('#calendar').swipeCalendar('refetchEvents');
					}, 2000);
				}
			},
			error: function () {
				console.log('Chiamata ajax fallita!');
			}
		}); //ajax
	} // enf if validazione form
} // SalvaTurno()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function aggiornaTurno() {
	var dati = $("#form_edit_turno").serialize();
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app
	myApp.confirm('Vuoi aggiornare il Turno', 'TurniPS',
		function () {
			// Quando si preme sul pulsante OK
			// Loader
			$('#btn-aggiorna-turno').html(
				'<span style="width:32px; height:32px; margin-top:5px;" class="preloader preloader-white">'
			);

			$.ajax({
				type: "POST",
				url: 'https://turnips.it/db_sql/_edit/4_1_5/aggiorna_turno.php',
				data: dati + "&id_utente=" + id_utente + "&versione=" + ver,
				dataType: "html",
				success: function (msg) {

					loadTurni();
					// Visualizzo messaggio di conferma OK
					showToast(
						"<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Turno Aggiornato!",
						"box-ok", "2000");
					setTimeout(function () {
						$("#last_up_turno").html('Ultima modifica effettuata il: ' + msg);
						$('#btn-aggiorna-turno').html(
							'<i class="fa fa-refresh"></i> Aggiorna');
						$('#calendar').swipeCalendar('refetchEvents');
					}, 2000);
				},
				error: function () {
					console.log('Chiamata ajax fallita!');
				}
			}); //ajax
		},
		function () {
			//Quando si preseme sul pulsante ANNULLA
		}
	); //  Fine alert scelta ok-annulla
} //aggiornaTurno()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function caricoStraordinario() {
	var id_utente = localStorage['id_user'];
	$('#ore-d-ec').html('<span class="preloader preloader-red"></span>');
	$('#ore-n-o-f-ec').html('<span class="preloader preloader-red"></span>');
	$('#ore-n-e-f-ec').html('<span class="preloader preloader-red"></span>');
	$('#ore-d-pr').html('<span class="preloader preloader-red"></span>');
	$('#ore-n-o-f-pr').html('<span class="preloader preloader-red"></span>');
	$('#ore-n-e-f-pr').html('<span class="preloader preloader-red"></span>');
	$('#ore-totali').html('<span class="preloader preloader-red"></span>');
	$.ajax({
		type: 'POST',
		url: 'https://turnips.it/sql_db/app/3_0/carico_strao.php',
		data: {
			id_utente: id_utente
		},
		dataType: 'json',
		success: function (value) {
			$('#ore-d-ec').html(value.ae);
			$('#ore-n-o-f-ec').html(value.be);
			$('#ore-n-e-f-ec').html(value.ce);
			$('#ore-d-pr').html(value.pa);
			$('#ore-n-o-f-pr').html(value.pb);
			$('#ore-n-e-f-pr').html(value.pc);
			$('#ore-totali').html(value.totale_ore);
			$('#select-strao-anno').val(value.anno);
			$('#select-strao-mese').val(value.mese);
			//Gestione Ore Straordinario
			switch (value.mese) {
				case '01':
					var nome_mese = 'Gennaio';
					break;
				case '02':
					var nome_mese = 'Febbraio';
					break;
				case '03':
					var nome_mese = 'Marzo';
					break;
				case '04':
					var nome_mese = 'Aprile';
					break;
				case '05':
					var nome_mese = 'Maggio';
					break;
				case '06':
					var nome_mese = 'Giugno';
					break;
				case '07':
					var nome_mese = 'Luglio';
					break;
				case '08':
					var nome_mese = 'Agosto';
					break;
				case '09':
					var nome_mese = 'Settembre';
					break;
				case '10':
					var nome_mese = 'Ottobre';
					break;
				case '11':
					var nome_mese = 'Novembre';
					break;
				case '12':
					var nome_mese = 'Dicembre';
					break;
			}
			$('#anno-gestione-strao').html(value.anno);
			$('#mese-gestione-strao').html(nome_mese);
			$('#ore-gestione-strao').html(value.totale_ore);
			$('#totale_ore').val(value.totali_min);
			$('#mese_anno').val(value.anno + '-' + value.mese);
			//Totale strao
			$('#totale-strao-eme').html('Tot.&thinsp;' + value.totale_eme + '&thinsp;');
			$('#totale-strao-pr').html('Tot.&thinsp;' + value.totale_pr + '&thinsp;');
			// Gestione strao
			$('#ore_pagate').val(value.orePagate);
			$('#min_pagate').val(value.minPagate);
			$('#ore_da_pagare').val(value.oreDaPagare);
			$('#min_da_pagare').val(value.minDaPagare);
			$('#ore_in_rc').val(value.oreRiposo);
			$('#min_in_rc').val(value.minRiposo);
			// Ore non pagate

			var ore_mese = value.da_pagare;

			for (var i = 0; i < ore_mese.length; i++) {

				switch (ore_mese[i].anno_mese.substring(12, 5)) {
					case '01':
						$('#ore_gennaio').html(ore_mese[i].da_pagare.slice(0, 5));
						break;
					case '02':
						$('#ore_febbraio').html(ore_mese[i].da_pagare.slice(0, 5));
						break;
					case '03':
						$('#ore_marzo').html(ore_mese[i].da_pagare.slice(0, 5));
						break;
					case '04':
						$('#ore_aprile').html(ore_mese[i].da_pagare.slice(0, 5));
						break;
					case '05':
						$('#ore_maggio').html(ore_mese[i].da_pagare.slice(0, 5));
						break;
					case '06':
						$('#ore_giugno').html(ore_mese[i].da_pagare.slice(0, 5));
						break;
					case '07':
						$('#ore_luglio').html(ore_mese[i].da_pagare.slice(0, 5));
						break;
					case '08':
						$('#ore_agosto').html(ore_mese[i].da_pagare.slice(0, 5));
						break;
					case '09':
						$('#ore_settembre').html(ore_mese[i].da_pagare.slice(0, 5));
						break;
					case '10':
						$('#ore_ottobre').html(ore_mese[i].da_pagare.slice(0, 5));
						break;
					case '11':
						$('#ore_novembre').html(ore_mese[i].da_pagare.slice(0, 5));
						break;
					case '12':
						$('#ore_dicembre').html(ore_mese[i].da_pagare.slice(0, 5));
						break;
				}

			} // end for

		} //end success
	}) // end ajax
} //caricoStraordinario()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function caricoStraordinarioScelta() {
	$("#select-strao-mese").change(function () {
		// Dati ricerca
		var id_utente = localStorage['id_user'];
		var anno = $('#select-strao-anno').val();
		var mese = $('#select-strao-mese').val();
		//Loader
		$('#ore-d-ec').html('<span class="preloader preloader-red"></span>');
		$('#ore-n-o-f-ec').html('<span class="preloader preloader-red"></span>');
		$('#ore-n-e-f-ec').html('<span class="preloader preloader-red"></span>');
		$('#ore-d-pr').html('<span class="preloader preloader-red"></span>');
		$('#ore-n-o-f-pr').html('<span class="preloader preloader-red"></span>');
		$('#ore-n-e-f-pr').html('<span class="preloader preloader-red"></span>');
		$('#ore-totali').html('<span class="preloader preloader-red"></span>');
		$.ajax({
			type: 'POST',
			url: 'https://turnips.it/sql_db/app/3_0/carico_strao_scelta.php',
			data: {
				id_utente: id_utente,
				anno: anno,
				mese: mese
			},
			dataType: 'json',
			success: function (value) {
				$('#ore-d-ec').text(value.ae);
				$('#ore-n-o-f-ec').html(value.be);
				$('#ore-n-e-f-ec').html(value.ce);
				$('#ore-d-pr').html(value.pa);
				$('#ore-n-o-f-pr').html(value.pb);
				$('#ore-n-e-f-pr').html(value.pc);
				$('#ore-totali').html(value.totale_ore);
				//Gestione Ore Straordinario
				switch (value.mese) {
					case '01':
						var nome_mese = 'Gennaio';
						break;
					case '02':
						var nome_mese = 'Febbraio';
						break;
					case '03':
						var nome_mese = 'Marzo';
						break;
					case '04':
						var nome_mese = 'Aprile';
						break;
					case '05':
						var nome_mese = 'Maggio';
						break;
					case '06':
						var nome_mese = 'Giugno';
						break;
					case '07':
						var nome_mese = 'Luglio';
						break;
					case '08':
						var nome_mese = 'Agosto';
						break;
					case '09':
						var nome_mese = 'Settembre';
						break;
					case '10':
						var nome_mese = 'Ottobre';
						break;
					case '11':
						var nome_mese = 'Novembre';
						break;
					case '12':
						var nome_mese = 'Dicembre';
						break;
				}
				$('#anno-gestione-strao').html(value.anno);
				$('#mese-gestione-strao').html(nome_mese);
				$('#ore-gestione-strao').html(value.totale_ore);
				$('#mese_anno').val(value.anno + '-' + value.mese);
				//Totale strao
				$('#totale-strao-eme').html('Tot.&thinsp;' + value.totale_eme +
					'&thinsp;');
				$('#totale-strao-pr').html('Tot.&thinsp;' + value.totale_pr +
					'&thinsp;');
				// Gestione strao
				$('#ore_pagate').val(value.orePagate);
				$('#min_pagate').val(value.minPagate);
				$('#ore_da_pagare').val(value.oreDaPagare);
				$('#min_da_pagare').val(value.minDaPagare);
				$('#ore_in_rc').val(value.oreRiposo);
				$('#min_in_rc').val(value.minRiposo);
			} //end success
		}) // end ajax
	}); // end change
} //caricoStraordinarioScelta()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function eliminaTurnario() {
	// Dati dal form
	var id_utente = localStorage['id_user']; // ID utente
	myApp.confirm('Confermi di voler eliminare la data?', 'TurniPS',
		function () {
			// Quando si preme sul pulsante OK
			myApp.showPreloader('Attendere...')
			$.ajax({
				type: "POST",
				url: 'https://turnips.it/sql_db/app/3_0/calendario/elimina_data_turnario.php',
				data: "id_utente=" + id_utente,
				dataType: "html",
				success: function (msg) {

					localStorage.removeItem("turnario-data-start");
					localStorage.removeItem("turnario-schema");
					localStorage.removeItem("turnario-array");
					$('#btn-edit-turnario').css('display', 'none');
					$('#btn-salva-turnario').css('display', 'block');
					setTimeout(function () {
						// Nasconde il loader
						myApp.hidePreloader();
						showToast("<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Data Eliminata!", "box-ok", "2000");
						// Rest dei campi
						$('#start-data-turnario').val(dataAttuale());
						// Visualizzo nelle select lo schema salvato
						$('#turnario_select').val('');
					}, 1000); //end .timeout
				},
				error: function () {
					console.log('Chiamata ajax fallita!');
				}
			}) // end ajax
		},
		function () {
			//Quando si preseme sul pulsante ANNULLA
		}
	); //  Fine alert scelta ok-annulla
} // eliminaTurnario()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function caricoBustaPaga() {
	var id_utente = localStorage['id_user'];
	myApp.showPreloader('Caricamento');
	$.ajax({
		type: 'POST',
		url: 'https://turnips.it/db_sql/riepilogo/cedolino.php',
		data: {
			id_utente: id_utente
		},
		dataType: 'json',
		success: function (value) {
			// Nasconde il loader
			myApp.hidePreloader();

			$('#select-anno-paga').val(value.anno);
			$('#select-mese-paga').val(value.mese);
			if (value.totale_busta_paga == "0,00") {
				$('#result-busta_paga').append(
					'<p class="animated bounceInUp" style="text-align: center;">Non hai inserito nessun turno.<br>Pertanto non possiamo fare alcun conteggio.</p>'
				);

			} else {
				$('#result-busta_paga').append(
					'<div class="data-table card">' +
					'<table>' +
					'<thead>' +
					'<tr>' +
					'<th class="label-cell">Tipo</th>' +
					'<th class="numeric-cell">Q.tà</th>' +
					'<th class="numeric-cell">Imp. €</th>' +
					'<th class="numeric-cell">Totale €</th>' +
					'</tr>' +
					'</thead>' +
					'<tbody id="tabella_busta_paga">' +
					'</tbody>' +
					'</table>'
				);

				if (value.sum_ore_diurne != "00:00") {

					$('#tabella_busta_paga').append(
						'<tr>' +
						'<td class="label-cell">St. Diurno</td>' +
						'<td class="numeric-cell">' + value.sum_ore_diurne + '</td>' +
						'<td class="numeric-cell">11,54</td>' +
						'<td class="numeric-cell">' + value.sum_costo_diurne + '</td>' +
						'</tr>'
					);
				} // Fine ST. diurno
				if (value.sum_ore_notte_festive != "00:00") {

					$('#tabella_busta_paga').append(
						'<tr>' +
						'<td class="label-cell txt-long">St. Notturno o Festivo</td>' +
						'<td class="numeric-cell">' + value.sum_ore_notte_festive + '</td>' +
						'<td class="numeric-cell">13,04</td>' +
						'<td class="numeric-cell">' + value.sum_costo_notte_festive + '</td>' +
						'</tr>'
					);
				} // Fine ST. Notturno o Festivo
				if (value.sum_ore_notte_e_festive != "00:00") {

					$('#tabella_busta_paga').append(
						'<tr>' +
						'<td class="label-cell txt-long">St. Notturno Festivo</td>' +
						'<td class="numeric-cell">' + value.sum_ore_notte_e_festive + '</td>' +
						'<td class="numeric-cell">15,05</td>' +
						'<td class="numeric-cell">' + value.sum_costo_notte_e_festive +
						'</td>' +
						'</tr>'
					);
				} // Fine ST. Notturno o Festivo
				if (value.serv_esterno >= 1) {

					$('#tabella_busta_paga').append(
						'<tr>' +
						'<td class="label-cell txt-long">Ind. Serv. Esterno</td>' +
						'<td class="numeric-cell">' + value.serv_esterno + '</td>' +
						'<td class="numeric-cell">6,00</td>' +
						'<td class="numeric-cell">' + value.serv_esterno_c + '</td>' +
						'</tr>'
					);
				} // Fine Ind. Serv. Esterno
				if (value.ore_notti >= 1) {

					$('#tabella_busta_paga').append(
						'<tr>' +
						'<td class="label-cell txt-long">Ind. Serv. Notturno</td>' +
						'<td class="numeric-cell">' + value.ore_notti + '</td>' +
						'<td class="numeric-cell">4,10</td>' +
						'<td class="numeric-cell">' + value.ore_notti_c + '</td>' +
						'</tr>'
					);
				} // Fine Ind. Serv. Notturno
				if (value.festivo >= 1) {

					$('#tabella_busta_paga').append(
						'<tr>' +
						'<td class="label-cell txt-long">Ind. Serv. Festivo</td>' +
						'<td class="numeric-cell">' + value.festivo + '</td>' +
						'<td class="numeric-cell">12,00</td>' +
						'<td class="numeric-cell">' + value.festivo_c + '</td>' +
						'</tr>'
					);
				} // Fine Ind. Serv. Festivo
				if (value.op >= 1) {

					$('#tabella_busta_paga').append(
						'<tr>' +
						'<td class="label-cell">O.P. in Sede</td>' +
						'<td class="numeric-cell">' + value.op + '</td>' +
						'<td class="numeric-cell">13,00</td>' +
						'<td class="numeric-cell">' + value.op_c + '</td>' +
						'</tr>'
					);
				} // Fine O.P. in Sede
				if (value.op_esterno >= 1) {

					$('#tabella_busta_paga').append(
						'<tr>' +
						'<td class="label-cell txt-long">O.P. Fuori Sede</td>' +
						'<td class="numeric-cell">' + value.op_esterno + '</td>' +
						'<td class="numeric-cell">18,20</td>' +
						'<td class="numeric-cell">' + value.op_esterno_c + '</td>' +
						'</tr>'
					);
				} // Fine O.P. Fuori Sede
				if (value.o_p_pernotto >= 1) {

					$('#tabella_busta_paga').append(
						'<tr>' +
						'<td class="label-cell txt-long">O.P. con Pernotto</td>' +
						'<td class="numeric-cell">' + value.o_p_pernotto + '</td>' +
						'<td class="numeric-cell">26,00</td>' +
						'<td class="numeric-cell">' + value.o_p_pernotto_c + '</td>' +
						'</tr>'
					);
				} // Fine O.P. con Pernotto
				if (value.super_festivo >= 1) {

					$('#tabella_busta_paga').append(
						'<tr>' +
						'<td class="label-cell txt-long">Super Festivo</td>' +
						'<td class="numeric-cell">' + value.super_festivo + '</td>' +
						'<td class="numeric-cell">40,00</td>' +
						'<td class="numeric-cell">' + value.super_festivo_c + '</td>' +
						'</tr>'
					);
				} // Fine Super Festivo
				if (value.sl_autostrada >= 1) {

					$('#tabella_busta_paga').append(
						'<tr>' +
						'<td class="label-cell txt-long">Indenn. Autostradale</td>' +
						'<td class="numeric-cell">' + value.sl_autostrada + '</td>' +
						'<td class="numeric-cell">10,00</td>' +
						'<td class="numeric-cell">' + value.sl_autostrada_c + '</td>' +
						'</tr>'
					);
				} // Fine Indennità Autostradale
				if (value.flag_scalo == 1) {

					$('#tabella_busta_paga').append(
						'<tr class="bg_riposo_compensativo">' +
						'<td class="label-cell txt-long">Riposo Compensativo</td>' +
						'<td class="numeric-cell">' + value.ore_scalo + '</td>' +
						'<td class="numeric-cell">---</td>' +
						'<td class="numeric-cell">-' + value.ore_scalo_costo + '</td>' +
						'</tr>'
					);
				} // Fine SCALO STRAORDINARIO ore RIPOSO COMPENSATIVO
				$('#tabella_busta_paga').append(
					'<tr>' +
					'<td colspan="1" class="label-cell"></td>' +
					'<td colspan="2" class="label-cell" style="color: #c0392b; font-style: italic;">Totale Lordo</td>' +
					'<td colspan="1" class="numeric-cell" style="color: #c0392b; font-weight: bold; font-style: italic;">' +
					value.totale_busta_paga + '€</td>' +
					'</tr>' +
					'</div>'
				);
			}
		}, //end success
		error: function () {
			// Nasconde il loader
			myApp.hidePreloader();
			console.log('errore ajax');
		}
	}) // end ajax
} //caricoBustaPaga()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function caricoPagaScelta() {
	$("#select-mese-paga").change(function () {
		myApp.showPreloader('Caricamento');
		$("#result-busta_paga").empty();
		// Dati ricerca
		var id_utente = localStorage['id_user'];
		var anno = $('#select-anno-paga').val();
		var mese = $('#select-mese-paga').val();
		$.ajax({
			type: 'POST',
			url: 'https://turnips.it/sql_db/app/3_5/b_paga_scelta.php',
			data: {
				id_utente: id_utente,
				anno: anno,
				mese: mese
			},
			dataType: 'json',
			success: function (value) {

				// Nasconde il loader
				myApp.hidePreloader();
				if (value.totale_busta_paga == "0,00") {
					$('#result-busta_paga').append(

						'<div class="card">' +
						'<div class="card-content">' +
						'<div class="card-content-inner">Non hai inserito nessun dato per questo mese. Pertanto non non possiamo fare conteggi.</div>' +
						'</div>' +
						'</div>'
					);
				} else {
					$('#result-busta_paga').append(
						'<div class="data-table card">' +
						'<table>' +
						'<thead>' +
						'<tr>' +
						'<th class="label-cell">Tipo</th>' +
						'<th class="numeric-cell">Q.tà</th>' +
						'<th class="numeric-cell">Imp. €</th>' +
						'<th class="numeric-cell">Totale €</th>' +
						'</tr>' +
						'</thead>' +
						'<tbody id="tabella_busta_paga">' +
						'</tbody>' +
						'</table>'
					);
					if (value.sum_ore_diurne != "00:00") {

						$('#tabella_busta_paga').append(
							'<tr>' +
							'<td class="label-cell">St. Diurno</td>' +
							'<td class="numeric-cell">' + value.sum_ore_diurne + '</td>' +
							'<td class="numeric-cell">11,54</td>' +
							'<td class="numeric-cell">' + value.sum_costo_diurne + '</td>' +
							'</tr>'
						);
					} // Fine ST. diurno
					if (value.sum_ore_notte_festive != "00:00") {

						$('#tabella_busta_paga').append(
							'<tr>' +
							'<td class="label-cell txt-long">St. Notturno o Festivo</td>' +
							'<td class="numeric-cell">' + value.sum_ore_notte_festive +
							'</td>' +
							'<td class="numeric-cell">13,04</td>' +
							'<td class="numeric-cell">' + value.sum_costo_notte_festive +
							'</td>' +
							'</tr>'
						);
					} // Fine ST. Notturno o Festivo
					if (value.sum_ore_notte_e_festive != "00:00") {

						$('#tabella_busta_paga').append(
							'<tr>' +
							'<td class="label-cell txt-long">St. Notturno Festivo</td>' +
							'<td class="numeric-cell">' + value.sum_ore_notte_e_festive +
							'</td>' +
							'<td class="numeric-cell">15,05</td>' +
							'<td class="numeric-cell">' + value.sum_costo_notte_e_festive +
							'</td>' +
							'</tr>'
						);
					} // Fine ST. Notturno o Festivo
					if (value.serv_esterno >= 1) {

						$('#tabella_busta_paga').append(
							'<tr>' +
							'<td class="label-cell txt-long">Ind. Serv. Esterno</td>' +
							'<td class="numeric-cell">' + value.serv_esterno + '</td>' +
							'<td class="numeric-cell">6,00</td>' +
							'<td class="numeric-cell">' + value.serv_esterno_c + '</td>' +
							'</tr>'
						);
					} // Fine Ind. Serv. Esterno
					if (value.ore_notti >= 1) {

						$('#tabella_busta_paga').append(
							'<tr>' +
							'<td class="label-cell txt-long">Ind. Serv. Notturno</td>' +
							'<td class="numeric-cell">' + value.ore_notti + '</td>' +
							'<td class="numeric-cell">4,10</td>' +
							'<td class="numeric-cell">' + value.ore_notti_c + '</td>' +
							'</tr>'
						);
					} // Fine Ind. Serv. Notturno
					if (value.festivo >= 1) {

						$('#tabella_busta_paga').append(
							'<tr>' +
							'<td class="label-cell txt-long">Ind. Serv. Festivo</td>' +
							'<td class="numeric-cell">' + value.festivo + '</td>' +
							'<td class="numeric-cell">12,00</td>' +
							'<td class="numeric-cell">' + value.festivo_c + '</td>' +
							'</tr>'
						);
					} // Fine Ind. Serv. Festivo
					if (value.op >= 1) {

						$('#tabella_busta_paga').append(
							'<tr>' +
							'<td class="label-cell">O.P. in Sede</td>' +
							'<td class="numeric-cell">' + value.op + '</td>' +
							'<td class="numeric-cell">13,00</td>' +
							'<td class="numeric-cell">' + value.op_c + '</td>' +
							'</tr>'
						);
					} // Fine O.P. in Sede
					if (value.op_esterno >= 1) {

						$('#tabella_busta_paga').append(
							'<tr>' +
							'<td class="label-cell txt-long">O.P. Fuori Sede</td>' +
							'<td class="numeric-cell">' + value.op_esterno + '</td>' +
							'<td class="numeric-cell">18,20</td>' +
							'<td class="numeric-cell">' + value.op_esterno_c + '</td>' +
							'</tr>'
						);
					} // Fine O.P. Fuori Sede
					if (value.o_p_pernotto >= 1) {

						$('#tabella_busta_paga').append(
							'<tr>' +
							'<td class="label-cell txt-long">O.P. con Pernotto</td>' +
							'<td class="numeric-cell">' + value.o_p_pernotto + '</td>' +
							'<td class="numeric-cell">26,00</td>' +
							'<td class="numeric-cell">' + value.o_p_pernotto_c + '</td>' +
							'</tr>'
						);
					} // Fine O.P. con Pernotto
					if (value.super_festivo >= 1) {

						$('#tabella_busta_paga').append(
							'<tr>' +
							'<td class="label-cell txt-long">Super Festivo</td>' +
							'<td class="numeric-cell">' + value.super_festivo + '</td>' +
							'<td class="numeric-cell">40,00</td>' +
							'<td class="numeric-cell">' + value.super_festivo_c + '</td>' +
							'</tr>'
						);
					} // Fine Super Festivo
					if (value.flag_scalo == 1) {

						$('#tabella_busta_paga').append(
							'<tr class="bg_riposo_compensativo">' +
							'<td class="label-cell txt-long">Riposo Compensativo</td>' +
							'<td class="numeric-cell">' + value.ore_scalo + '</td>' +
							'<td class="numeric-cell">---</td>' +
							'<td class="numeric-cell">-' + value.ore_scalo_costo + '</td>' +
							'</tr>'
						);
					} // Fine SCALO STRAORDINARIO ore RIPOSO COMPENSATIVO
					$('#tabella_busta_paga').append(
						'<tr>' +
						'<td colspan="1" class="label-cell"></td>' +
						'<td colspan="2" class="label-cell" style="color: #c0392b; font-style: italic;">Totale Lordo</td>' +
						'<td colspan="1" class="numeric-cell" style="color: #c0392b; font-weight: bold; font-style: italic;">' +
						value.totale_busta_paga + '€</td>' +
						'</tr>' +
						'</div>'
					);
				}
			}, //end success
			error: function () {
				// Nasconde il loader
				myApp.hidePreloader();
				console.log('errore ajax');
			}
		}) // end ajax
	}); // end change
} //caricoPagaScelta()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function inviaEmail() {
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app
	var dati = $("#form_invia_email").serialize();
	// Validazione del form
	if ($('#oggetto_email').val() === 'NO') {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Non hai scelto l'oggetto!",
			"box-errore", "2000");
		return false;
	} else if ($('#messaggio_email').val() === '') {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Non hai inserito il messaggio!",
			"box-errore", "2000");
		return false;
	} else if (id_utente === '6') {
		myApp.alert(
			'Non puoi usare il modulo contatti, con l\'account <span class="red_bold">DEMO</span>. Inviaci un e-mail a:<br><br><a class="external" href="mailto:info@turnips.it?subject=Segnalazioni-Richieste">info@turnips.it</a>',
			'TurniPS');

		return false;
	} else {
		var dati_utente = JSON.parse(localStorage.getItem("dati-utente"));
		$.each(dati_utente, function (key, value) {
			var user = value.user;
			var nome = value.nome;
			var cognome = value.cognome;
			var email = value.mail;
			// Loader Bottone
			$('#btn-invia-email').html('<span style="width:32px; height:32px; margin-top:5px;" class="preloader preloader-white">');
			$.ajax({
				type: "POST",
				url: 'https://turnips.it/sql_db/app/3_0/invia_email.php',
				data: dati + "&id_utente=" + id_utente + "&versione=" + ver + "&nome=" +
					nome + "&cognome=" + cognome + "&email=" + email + "&user=" + user,
				dataType: "html",
				success: function (msg) {

					$('#form_invia_email')[0].reset(); //Reset form
					// Visualizzo messaggio di conferma OK
					showToast(
						"<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Email Inviata!",
						"box-ok", "2000");
					$('#btn-invia-email').html('Invia Email');
				},
				error: function () {
					myApp.alert('Errore di Connessione!', 'TurniPS');
				}
			}); //ajax
		}); //end .each*/
	} // end if
} // SalvaAssenza()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function chekUpdateDB() {
	var id_utente = localStorage['id_user'];
	// Controllo servizi personali
	$.ajax({
		type: 'POST',
		url: 'https://turnips.it/db_sql/settings/versione_database.php',
		data: "id_utente=" + id_utente,
		dataType: 'json',
		success: function (value) {

			$.each(value, function (key, value) {

				if ($.trim(value.esito) === "KO") {

					$('#update-db').addClass('active-up');
					var container = $('.progressbar-load-hide p:first-child');
					if (container.children('.progressbar').length) return; //don't run all this if there is a current progressbar loading
					myApp.showProgressbar(container, 0);
					// Simluate Loading Something
					var progress = 0;

					function simulateLoading() {
						setTimeout(function () {
							var progressBefore = progress;
							progress += Math.random() * 20;
							myApp.setProgressbar(container, progress);
							if (progressBefore < 100) {
								simulateLoading(); //keep "loading"
							} else myApp.hideProgressbar(container); //hide
						}, 400);
					}
					simulateLoading();
					setTimeout(function () {
						$('#update-db').removeClass('active-up');
					}, 6000);
				} else {
					//console.log('Ver. DB in Linea');
					//localStorage['setting-etichetta-turnario'] = value.etichetta;
				};
			});
		} // end success
	});
} //chekUpdateDB
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Salva assenze tramite chiamata ajax
function salvaStraoEmergente() {
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app
	var dati = $("#form_add_strao_eme").serialize();

	// Validazione del form
	if ($('#data_strao_eme').val() === '') {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Non hai inserito la data!",
			"box-errore", "2000");
		return false;
	} else {
		// Loader Bottone
		$('#btn-salva-stra-eme').html(
			'<span style="width:32px; height:32px; margin-top:5px;" class="preloader preloader-white">'
		);
		$.ajax({
			type: "POST",
			url: 'https://turnips.it/sql_db/app/3_0/salva_strao_eme.php',
			data: dati + "&id_utente=" + id_utente + "&versione=" + ver,
			dataType: "html",
			success: function (msg) {
				locadStraoEme();
				$('#form_add_strao_eme')[0].reset(); //Reset form
				// Visualizzo messaggio di conferma OK
				showToast(
					"<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Dati Salvati!",
					"box-ok", "2000");
				setTimeout(function () {
					$('#btn-salva-stra-eme').html('Salva');
					// Data corrente per input date
					document.getElementById('data_strao_eme').value = dataAttuale();
					$('#calendar').swipeCalendar('refetchEvents');
				}, 2000);
			},
			error: function () {
				console.log('Chiamata ajax fallita!');
			}
		}); //ajax
	} // end if
} // salvaStraoEmergente()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function salvaStraordinario() {
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app
	var dati = $("#form_add_straordinario").serialize();

	// Validazione del form
	if (isEmpty($('#tipo_straordinario').val())) {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Non hai inserito il tipo!",
			"box-errore", "4000");
		return false;
	} else if (isEmpty($('#data_straordinario').val())) {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Non hai inserito la data!",
			"box-errore", "4000");
		return false;
	} else if (isEmpty($('#titolo_straordinario').val())) {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Non hai inserito una descrizione!",
			"box-errore", "4000");
		return false;
	} else if (isEmpty($('#straordinario_start').val())) {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Non hai inserito l'orario inziale!",
			"box-errore", "4000");
		return false;
	} else if ($('#straordinario_end').val() == $('#straordinario_start').val()) {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;L'orario finale non puo uguale a quello inziale",
			"box-errore", "4000");
		return false;
	} else {
		// Loader Bottone
		$('#btn-salva-stra-eme').html(
			'<span style="width:32px; height:32px; margin-top:5px;" class="preloader preloader-white">'
		);

		if ($('#tipo_straordinario').val() == 'pro') {
			urlApi = 'https://turnips.it/sql_db/app/3_0/2020_salva_strao_pr.php';
		} else {
			urlApi = 'https://turnips.it/sql_db/app/3_0/2020_salva_strao_eme.php';
		}

		//data: dati + "&id_utente=" + id_utente + "&versione=" + ver,
		$.ajax({
			type: "POST",
			url: urlApi,
			data: dati + "&id_utente=" + id_utente + "&versione=" + ver,
			dataType: "html",
			success: function (msg) {
				locadStraoEme();
				locadStraoProg();
				$('#form_add_straordinario')[0].reset(); //Reset form
				// Visualizzo messaggio di conferma OK
				showToast(
					"<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Dati Salvati!",
					"box-ok", "2000");
				setTimeout(function () {
					$('#btn-salva-stra-eme').html('Salva');
					// Data corrente per input date
					document.getElementById('data_straordinario').value = dataAttuale();
					$('#calendar').swipeCalendar('refetchEvents');
				}, 2000);
			},
			error: function () {
				console.log('Chiamata ajax fallita!');
			}
		}); //ajax
	} // end if
} // salvaStraoEmergente()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funziona elimina straordinario emergente
function eliminaStraoEmergente() {
	var dati = $("#form_edit_strao_eme").serialize();
	var id_utente = localStorage['id_user'];
	myApp.confirm('Vuoi eliminare lo straordinario?', 'TurniPS',
		function () {
			// Quando si preme sul pulsante OK
			// Loader
			$('#btn-elimina-strao-eme').html(
				'<span style="width:32px; height:32px; margin-top:5px;" class="preloader preloader-white">'
			);
			$.ajax({
				type: "POST",
				url: 'https://turnips.it/sql_db/app/3_0/elimina_strao_eme.php',
				data: dati + "&id_utente=" + id_utente,
				dataType: "html",
				success: function (msg) {

					locadStraoEme();
					// Visualizzo messaggio di conferma OK
					showToast(
						"<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Straordinario Eliminato!",
						"box-ok", "2000");
					setTimeout(function () {
						$('#btn-elimina-ass').html('<i class="fa fa-trash-o"></i> Elimina');
						$('#calendar').swipeCalendar('refetchEvents');
					}, 2000);
					setTimeout(function () {
						mainView.router.loadPage('index.html');
					}, 2200);
				},
				error: function () {
					console.log('Chiamata ajax fallita!');
					$('#btn-elimina-strao-eme').html(
						'<i class="fa fa-trash-o"></i> Elimina');
				}
			}); //ajax
		},
		function () {
			//Quando si preseme sul pulsante ANNULLA
		}
	); //  Fine alert scelta ok-annulla
} //eliminaStraoEmergente()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function aggiornaStraoEmergente() {
	var dati = $("#form_edit_strao_eme").serialize();
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app
	myApp.confirm('Vuoi aggiornare lo straordinario?', 'TurniPS',
		function () {
			// Quando si preme sul pulsante OK
			// Loader
			$('#btn-aggiorna-strao-eme').html(
				'<span style="width:32px; height:32px; margin-top:5px;" class="preloader preloader-white">'
			);
			$.ajax({
				type: "POST",
				url: 'https://turnips.it/sql_db/app/3_0/aggiorna_strao_eme.php',
				data: dati + "&id_utente=" + id_utente + "&versione=" + ver,
				dataType: "html",
				success: function (msg) {

					locadStraoEme();
					// Visualizzo messaggio di conferma OK
					showToast(
						"<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Straordinario Aggiornato!",
						"box-ok", "2000");
					setTimeout(function () {
						$("#last_up_ass_strao_eme").html('Ultima modifica effettuata il: ' +
							msg);
						$('#btn-aggiorna-strao-eme').html(
							'<i class="fa fa-refresh"></i> Aggiorna');
						$('#calendar').swipeCalendar('refetchEvents');
					}, 2000);
				},
				error: function () {
					console.log('Chiamata ajax fallita!');
					$('#btn-aggiorna-strao-eme').html(
						'<i class="fa fa-refresh"></i> Aggiorna');
				}
			}); //ajax
		},
		function () {
			//Quando si preseme sul pulsante ANNULLA
		}
	); //  Fine alert scelta ok-annulla
} // aggiornaStraoEmergente()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Salva straordinario programmato tramite chiamata ajax
function salvaStraoProg() {
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app
	var dati = $("#form_add_strao_pr").serialize();
	// Validazione del form
	if ($('#data_strao_pr').val() === '') {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Non hai inserito la data!",
			"box-errore", "2000");
		return false;
	} else {
		// Loader Bottone
		$('#btn-salva-stra-pr').html(
			'<span style="width:32px; height:32px; margin-top:5px;" class="preloader preloader-white">'
		);
		$.ajax({
			type: "POST",
			url: 'https://turnips.it/sql_db/app/3_0/salva_strao_pr.php',
			data: dati + "&id_utente=" + id_utente + "&versione=" + ver,
			dataType: "html",
			success: function (msg) {
				locadStraoProg();
				$('#form_add_strao_pr')[0].reset(); //Reset form
				// Visualizzo messaggio di conferma OK
				showToast(
					"<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Dati Salvati!",
					"box-ok", "2000");
				setTimeout(function () {
					$('#btn-salva-stra-pr').html('Salva');
					// Data corrente per input date
					document.getElementById('data_strao_pr').value = dataAttuale();
					$('#calendar').swipeCalendar('refetchEvents');
				}, 2000);
			},
			error: function () {
				console.log('Chiamata ajax fallita!');
			}
		}); //ajax
	} // end else
} // salvaStraoProg()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function aggiornaStraoPr() {
	var dati = $("#form_edit_strao_pr").serialize();
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app

	myApp.confirm('Vuoi aggiornare lo straordinario?', 'TurniPS',
		function () {
			// Quando si preme sul pulsante OK
			// Loader
			$('#btn-aggiorna-strao-pr').html(
				'<span style="width:32px; height:32px; margin-top:5px;" class="preloader preloader-white">'
			);
			$.ajax({
				type: "POST",
				url: 'https://turnips.it/sql_db/app/3_0/aggiorna_strao_pr.php',
				data: dati + "&id_utente=" + id_utente + "&versione=" + ver,
				dataType: "html",
				success: function (msg) {

					locadStraoProg();
					// Visualizzo messaggio di conferma OK
					showToast(
						"<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Straordinario Aggiornato!",
						"box-ok", "2000");
					setTimeout(function () {
						$("#last_up_ass_strao_pr").html('Ultima modifica effettuata il: ' +
							msg);
						$('#btn-aggiorna-strao-pr').html(
							'<i class="fa fa-refresh"></i> Aggiorna');
						$('#calendar').swipeCalendar('refetchEvents');
					}, 2000);
				},
				error: function () {
					console.log('Chiamata ajax fallita!');
					$('#btn-aggiorna-strao-pr').html(
						'<i class="fa fa-refresh"></i> Aggiorna');
				}
			}); //ajax
		},
		function () {
			//Quando si preseme sul pulsante ANNULLA
		}
	); //  Fine alert scelta ok-annulla
} // aggiornaStraoPr()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function eliminaStraoPr() {
	var dati = $("#form_edit_strao_pr").serialize();
	var id_utente = localStorage['id_user'];
	myApp.confirm('Vuoi eliminare lo straordinario?', 'TurniPS',
		function () {
			// Quando si preme sul pulsante OK
			// Loader
			$('#btn-elimina-strao-pr').html(
				'<span style="width:32px; height:32px; margin-top:5px;" class="preloader preloader-white">'
			);
			$.ajax({
				type: "POST",
				url: 'https://turnips.it/sql_db/app/3_0/elimina_strao_pr.php',
				data: dati + "&id_utente=" + id_utente,
				dataType: "html",
				success: function (msg) {

					locadStraoProg();
					// Visualizzo messaggio di conferma OK
					showToast(
						"<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Straordinario Eliminato!",
						"box-ok", "2000");
					setTimeout(function () {
						$('#btn-elimina-strao-pr').html(
							'<i class="fa fa-trash-o"></i> Elimina');
						$('#calendar').swipeCalendar('refetchEvents');
					}, 2000);
					setTimeout(function () {
						mainView.router.loadPage('index.html');
					}, 2200);
				},
				error: function () {
					console.log('Chiamata ajax fallita!');
					$('#btn-elimina-strao-pr').html(
						'<i class="fa fa-trash-o"></i> Elimina');
				}
			}); //ajax
		},
		function () {
			//Quando si preseme sul pulsante ANNULLA
		}
	); //  Fine alert scelta ok-annulla
} // eliminaStraoPr()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function turniUtenteEdit() {
	$('#giorno-edit').append(
		'<option value="NO">Seleziona</option><option value="19.00 - 00.00">19.00 - 00.00</option><option value="13.00 - 19.00">13.00 - 19.00</option><option value="07.00 - 13.00">07.00 - 13.00</option><option value="00.00 - 07.00">00.00 - 07.00</option><option disabled>  Turni Personali  </option>'
	);
	// Controllo su turni Predefiniti
	$("#orario-edit").change(function () {
		$('#ore_notte-edit').val('');
		// Se viene selezionato uno dei due turni standard viene selezionato il Ticket Pasto
		if (($.trim($(this).val()) == "19.00 - 00.00") || $.trim($(this).val()) ==
			"13.00 - 19.00") {
			$('#sl_idennita_edit').find('option[value="sl_ticket_pasto-edit"]').attr(
				'selected', true);
			$('#text-smart-select').html('Ticket Pasto');
		} else {
			$('#sl_idennita_edit').find('option[value="sl_ticket_pasto-edit"]').attr(
				'selected', false);
			$('#text-smart-select').html('');
		}
	});
	// Controllo su turni personali utente
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app
	$.ajax({
		type: 'POST',
		url: 'https://turnips.it/sql_db/app/3_0/turni_utente.php',
		data: "id_ute=" + id_utente,
		dataType: 'json',
		success: function (value) {
			for (var i = 0; i < value.length; i++) {
				$('#orario-edit').append('<option value="' + value[i].orario_serv + '">' +
					value[i].orario_serv + '</option>');
			} // end for
			// In base al servizio scelto attribuisco i check
			$("#orario-edit").change(function () {
				var orario_turno = $(this).val();

				$.each(value, function (key, value) {
					if (orario_turno == value.orario_serv) {
						// Ore notti
						$('#ore_notte-edit').val(value.ore_notti);
						//  Cheked ticket pasto
						if ($.trim(value.ticket_pasto) == "1") {
							$('#sl_idennita_edit').find(
								'option[value="sl_ticket_pasto-edit"]').attr(
								'selected', true);
							$('#text-smart-select').html('Ticket Pasto');
						} else {
							$('#sl_idennita_edit').find(
								'option[value="sl_ticket_pasto-edit"]').attr(
								'selected', false);
							$('#text-smart-select').html('');
						};
					} //end if
				}); //end .each
			});
		} // end success
	});
} //turniUtenteEdit()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function serviziUtenteEdit() {
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app
	// Carico nelle select i serivizi personali dell'utente
	$('#tipo_serv-edit').append(
		'<option value="NO">Seleziona</option><option value="Volante">Volante</option><option value="Centralino">Centralino</option><option value="Corpo Guardia">Corpo Guardia</option><option value="Agg. Professionale">Agg. Professionale</option><option value="Teste">Teste</option><option disabled>  Servizi Personali  </option>'
	);
	// Controllo turni predefiniti
	$("#tipo_serv-edit").change(function () {
		// valori a default
		$('#sl_idennita_edit').find('option[value="sl_op_fuori-edit"]').attr(
			'selected', false);
		$('#sl_idennita_edit').find('option[value="sl_op_pernotto-edit"]').attr(
			'selected', false);
		$('#sl_idennita_edit').find('option[value="sl_op_sede-edit"]').attr(
			'selected', false);
		$('#sl_idennita_edit').find('option[value="sl_servizio_esterno-edit"]').attr(
			'selected', false);
		$('#sl_idennita_edit').find('option[value="sl_servizio_missione-edit"]').attr(
			'selected', false);
		$('#sl_idennita_edit').find('option[value="sl_reperibilita-edit"]').attr(
			'selected', false);
		// Servizi standard
		if ($.trim($(this).val()) == "Volante") {
			$('#sl_idennita_edit').find('option[value="sl_servizio_esterno-edit"]').attr(
				'selected', true);
			$('#text-smart-select').html('Volante');
		} else {
			$('#sl_idennita_edit').find('option[value="sl_servizio_esterno-edit"]').attr(
				'selected', false);
			$('#text-smart-select').html('');
		}
	});
	// Controllo servizi personali
	$.ajax({
		type: 'POST',
		url: 'https://turnips.it/sql_db/app/3_0/servizi_utente.php',
		data: "id_utente=" + id_utente,
		dataType: 'json',
		success: function (value) {
			// Visualizzo i servizi personali nel menu
			for (var i = 0; i < value.length; i++) {
				$('#tipo_serv-edit').append('<option value="' + value[i].nome_serv +
					'">' + value[i].nome_serv + '</option>');
			}
			// In base al servizio scelto attribuisco i check
			$("#tipo_serv-edit").change(function () {
				var nome_turno = $(this).val();
				$.each(value, function (key, value) {
					if (nome_turno == value.nome_serv) {
						////////////////////////////////////////////////////////////////////////////////////////////////////////
						// Carico le indennità nella smart select
						if ($.trim(value.o_p_fuori) == "1") {
							$('#sl_idennita_edit').find('option[value="sl_op_fuori-edit"]').attr(
								'selected', true);
						} else {
							$('#sl_idennita_edit').find('option[value="sl_op_fuori-edit"]').attr(
								'selected', false);
							$('#text-smart-select').html('');
						};
						if ($.trim(value.o_p_pernot) == "1") {
							$('#sl_idennita_edit').find('option[value="sl_op_pernotto-edit"]')
								.attr('selected', true);
						} else {
							$('#sl_idennita_edit').find('option[value="sl_op_pernotto-edit"]')
								.attr('selected', false);
							$('#text-smart-select').html('');
						};
						if ($.trim(value.o_p_sede) == "1") {
							$('#sl_idennita_edit').find('option[value="sl_op_sede-edit"]').attr(
								'selected', true);
						} else {
							$('#sl_idennita_edit').find('option[value="sl_op_sede-edit"]').attr(
								'selected', false);
							$('#text-smart-select').html('');
						};
						if ($.trim(value.serv_estern) == "1") {
							$('#sl_idennita_edit').find(
								'option[value="sl_servizio_esterno-edit"]').attr('selected',
								true);
						} else {
							$('#sl_idennita_edit').find(
								'option[value="sl_servizio_esterno-edit"]').attr('selected',
								false);
							$('#text-smart-select').html('');
						};
						if ($.trim(value.missione) == "1") {
							$('#sl_idennita_edit').find(
								'option[value="sl_servizio_missione-edit"]').attr('selected',
								true);
						} else {
							$('#sl_idennita_edit').find(
								'option[value="sl_servizio_missione-edit"]').attr('selected',
								false);
							$('#text-smart-select').html('');
						};
						if ($.trim(value.reperi) == "1") {
							$('#sl_idennita_edit').find(
								'option[value="sl_reperibilita-edit"]').attr('selected', true);
						} else {
							$('#sl_idennita_edit').find(
								'option[value="sl_reperibilita-edit"]').attr('selected', false);
							$('#text-smart-select').html('');
						};
						////////////////////////////////////////////////////////////////////////////////////////////////////////
					} // if
				}); //end .each
			});
		} // end success
	});
} //serviziUtenteEdit()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function refreshApp() {
	// Rimuovo gli array in localStorage di Turni,Assenze,Turnario
	localStorage.removeItem('cal-turni');
	localStorage.removeItem('cal-assenze');
	localStorage.removeItem('cal-strao-eme');
	localStorage.removeItem('cal-strao-pro');
	localStorage.removeItem('turnario-array');
	setTimeout(function () {
		caricoEventiCalendario();
	}, 1000); //end .timeout
	$('#update-db').addClass('active-up');
	var container = $('.progressbar-load-hide p:first-child');
	if (container.children('.progressbar').length) return; //don't run all this if there is a current progressbar loading
	myApp.showProgressbar(container, 0);
	// Simluate Loading Something
	var progress = 0;

	function simulateLoading() {
		setTimeout(function () {
			var progressBefore = progress;
			progress += Math.random() * 20;
			myApp.setProgressbar(container, progress);
			if (progressBefore < 100) {
				simulateLoading(); //keep "loading"
			} else myApp.hideProgressbar(container); //hide
		}, 150);
	}
	simulateLoading();
	setTimeout(function () {
		$('#update-db').removeClass('active-up');
		window.location.reload();
	}, 2000);
	//
} //refreshApp()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Visualizza la timeline con le novità degli aggiornamenti al database
function changeLogDB() {
	$('#timeline-change-log-db').load('https://turnips.it/sql_db/app/3_0/timeline_changelog_db.php');
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// "Modifica versione 3.4 del 18-06-2017"
function caricoDayRiposo() {
	var id_utente = localStorage['id_user']; // ID utente
	$('#lista-recupero-day-recuperati').append(
		'<li><span id="loader-recuperati" class="preloader loader-rec"></span></li>'
	);
	$.ajax({
		type: 'POST',
		url: 'https://turnips.it/sql_db/app/3_0/recupero_riposo_close.php',
		data: "id_utente=" + id_utente,
		dataType: 'json',
		success: function (data) {
			setTimeout(function () {
				$('#loader-recuperati').remove();
				if (data != null) {
					$.each(data, function (key, value) {
						$('#lista-recupero-day-recuperati').append(
							'<li class="li-recuperato">' +
							'<a href="pages/edit-turno-rec.html" onclick="turnoRecRiposo(this);" id="' +
							value.id + '" class="item-link item-content">' +
							'<div class="item-media"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></div>' +
							'<div class="item-inner">' +
							'<div class="item-title-rec-day-recuperato bbb"><span class="label-rec-day">Data:</span>' +
							date2ita(value.data) +
							'<span class="label-rec-day">&nbsp;&nbsp;Recuperato il:</span>' +
							date2ita(value.data_rec) + '</div>' +
							'</div>' +
							'</a>' +
							'</li>'
						);
					}); //end .each
				} else {
					$('#no-rec-recuperati').css('display', 'block');
					console.log('null');
				}
			}, 1000); //end .timeout
		} // success
	});
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////0
function caricoDayRiposoOpen() {
	var id_utente = localStorage['id_user']; // ID utente
	$('#lista-recupero-day').append(
		'<li><span id="loader-da-recuperare" class="preloader loader-rec"></span></li>'
	);
	$.ajax({
		type: 'POST',
		url: 'https://turnips.it/sql_db/app/3_0/recupero_riposo.php',
		data: "id_utente=" + id_utente,
		dataType: 'json',
		success: function (data) {

			setTimeout(function () {
				$('#loader-da-recuperare').remove();
				if (data != null) {
					$.each(data, function (key, value) {
						$('#lista-recupero-day').append('<li>' +
							'<a href="pages/edit-turno-rec.html" onclick="turnoRecRiposo(this);" id="' +
							value.id + '" class="item-link item-content">' +
							'<div class="item-media"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></div>' +
							'<div class="item-inner">' +
							'<div class="item-title-rec-day"><span class="label-rec-day">Data:</span>' +
							date2ita(value.data) + '</div>' +
							'</div>' +
							'</a>' +
							'</li>'
						);
						$('#no-rec').css('display', 'none');
					}); //end .each
				} else {
					$('#no-rec').css('display', 'block');
				}
			}, 1000); //end .timeout
		} // success
	});
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Converte le date in italiano (Solo nella visualizzazione, le date vengono sempre salvata in formato US)
function date2ita(data) {

	if(isEmpty(!data)){
		return data.replace(/(\d{4}).(\d{2}).(\d{2})/, "$3.$2.$1");
	}
} // date2ita()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function turnoRecRiposo(id_turno_off) {
	var id_utente = localStorage['id_user']; // ID utente
	var id_turno = $(id_turno_off).attr("id");
	// Visualizza il loader
	//console.log(id_turno);
	myApp.showPreloader('Caricamento')
	$.ajax({
		type: 'POST',
		url: 'https://turnips.it/sql_db/app/3_0/calendario/turno_singolo.php',
		data: {
			id_utente: id_utente,
			id_turno: id_turno,
		},
		dataType: 'json',
		success: function (msg) {
			setTimeout(function () {
				// Nasconde il loader
				myApp.hidePreloader();

				$.each(msg, function (key, value) {
					$('#id_turno_edit').val(value.id_turno);
					$('#giorno-edit').val(value.giorno);
					$('#orario-edit').val(value.orario);
					$('#tipo_serv-edit').val(value.tipo_serv);
					$('#ore_notte-edit').val(value.ore_notti);
					$('#nota-edit').val(value.nota);
					// Attivo il toogle se ci sono indennità doppie
					if ($.trim(value.ticket_pasto) == "2") {
						$('#doppio_pasto-ck-edit').prop("checked", true);
						$('#sl_idennita_edit').find(
							'option[value="sl_ticket_pasto-edit"]').attr('selected', true);
					};
					if ($.trim(value.ser_esterno) == "2") {
						$('#doppia_esterna-ck-edit').prop("checked", true);
						$('#sl_idennita_edit').find(
							'option[value="sl_servizio_esterno-edit"]').attr('selected',
							true);
					};
					if ($.trim(value.recuper_riposo) == "1") {
						$('#recupero_giorno-ck-edit').prop("checked", true);

					};
					// Carico le indennità nella smart select
					if ($.trim(value.ticket_pasto) == "1") {
						$('#sl_idennita_edit').find(
							'option[value="sl_ticket_pasto-edit"]').attr('selected', true);
					};
					if ($.trim(value.mancati_pasto) == "1") {
						$('#sl_idennita_edit').find(
							'option[value="sl_ticket_mancato_pasto-edit"]').attr(
							'selected', true);
					};
					if ($.trim(value.o_p_fuori) == "1") {
						$('#sl_idennita_edit').find('option[value="sl_op_fuori-edit"]')
							.attr('selected', true);
					};
					if ($.trim(value.o_p_cek) == "1") {
						$('#sl_idennita_edit').find('option[value="sl_op_sede-edit"]').attr(
							'selected', true);
					};
					if ($.trim(value.o_p_pernotto) == "1") {
						$('#sl_idennita_edit').find(
							'option[value="sl_op_pernotto-edit"]').attr('selected', true);
					};
					if ($.trim(value.ser_esterno) == "1") {
						$('#sl_idennita_edit').find(
							'option[value="sl_servizio_esterno-edit"]').attr('selected',
							true);
					};
					if ($.trim(value.reperibilita) == "1") {
						$('#sl_idennita_edit').find(
							'option[value="sl_reperibilita-edit"]').attr('selected', true);
					};
					if ($.trim(value.cambio_turno) == "1") {
						$('#sl_idennita_edit').find(
							'option[value="sl_cambio_turno-edit"]').attr('selected', true);
					};
					if ($.trim(value.rip_com) == "1") {
						$('#sl_idennita_edit').find(
							'option[value="sl_id_compensazione-edit"]').attr('selected',
							true);
					};
					if ($.trim(value.serv_missione) == "1") {
						$('#sl_idennita_edit').find(
							'option[value="sl_servizio_missione-edit"]').attr('selected',
							true);
					};
					// Se presente visualizzo data ultimo aggiornamento
					if ($.trim(value.data_aggiornamento) != "") {
						$("#last_up_turno").html('Ultima modifica effettuata il: ' +
							value.data_aggiornamento);
					};
				}); //end .each
			}, 1000); //end .timeout
		}, // Fine success
		error: function () {
			// Nasconde il loader
			myApp.hidePreloader();
			console.log('errore ajax loadTurni');
		}
	}); // Fine chiamata ajax
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function caricoDayRiposoAssenza() {
	var id_utente = localStorage['id_user']; // ID utente
	$.ajax({
		type: 'POST',
		url: 'https://turnips.it/sql_db/app/3_0/recupero_riposo_aperti.php',
		data: "id_utente=" + id_utente,
		dataType: 'json',
		success: function (data) {
			$.each(data, function (key, value) {
				$('#rec-day-assenza').append('<li>' +
					'<label class="label-checkbox item-content">' +
					'<input type="radio" name="ck-day-off" value="' + value.id + '">' +
					'<div class="item-media">' +
					'<i class="icon icon-form-checkbox"></i>' +
					'</div>' +
					'<div class="item-inner">' +
					'<div class="item-title-rec-rip">' + date2ita(value.data) + '</div>' +
					'</div>' +
					'</label>' +
					'</li>'
				);
			}); //end .each
		} // success
	});
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function dettaglioAssenza(nome, tipo) {
	var tipo_assenza = tipo;
	var titolo_assenza = nome;
	var anno = $('#anno_idennita_assenze').val();
	var id_utente = localStorage['id_user'];
	$.ajax({
		type: 'POST',
		url: 'https://turnips.it/db_sql/riepilogo/assenze_dettaglio.php',
		data: {
			id_utente: id_utente,
			tipo_assenza: tipo_assenza,
			anno: anno
		},
		dataType: 'json',
		success: function (data) {
			$('#titolo_assenza_dettaglio').html(nome);
			if (data != null) {
				$.each(data, function (key, value) {
					$('#lista-dettaglio-assenza').append('<li>' +
						'<a href="pages/edit-assenza-ind.html" onclick="editAssenzaInd(this);" id="' +
						value.id + '" class="item-link item-content">' +
						'<div class="item-media"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></div>' +
						'<div class="item-inner">' +
						'<div class="item-title-rec-day"><span class="label-rec-day">Data:</span>' +
						date2ita(value.data) + '</div>' +
						'</div>' +
						'</a>' +
						'</li>'
					);
				}); //end .each
			} else {
				$('#no-rec').css('display', 'block');
			}
		} // success
	});
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function dettaglioAssenzaCS(nome, tipo) {
	var tipo_assenza = tipo;
	var titolo_assenza = nome;
	var anno = $('#anno_idennita_assenze').val();
	var id_utente = localStorage['id_user'];
	$.ajax({
		type: 'POST',
		url: 'https://turnips.it/db_sql/riepilogo/assenze_dettaglio.php',
		data: {
			id_utente: id_utente,
			tipo_assenza: tipo_assenza,
			anno: anno
		},
		dataType: 'json',
		success: function (data) {
			$('#titolo_assenza_dettaglio').html(nome);
			if (data != null) {
				$.each(data, function (key, value) {
					$('#lista-dettaglio-assenza').append('<li>' +
						'<a href="pages/edit-assenza-ind.html" onclick="editAssenzaInd(this);" id="' +
						value.id + '" class="item-link item-content">' +
						'<div class="item-media"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></div>' +
						'<div class="item-inner">' +
						'<div class="item-title-rec-day"><span class="label-rec-day">Data:</span>' +
						date2ita(value.data) + '</div><div class="badge" style="font-size: 9px;">' + value.assenza + '</div>' +
						'</div>' +
						'</a>' +
						'</li>'
					);
				}); //end .each
			} else {
				$('#no-rec').css('display', 'block');
			}
		} // success
	});
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function countNumber() {
	setTimeout(function () {
		$('.count').each(function () {
			$(this).prop('Counter', 0).animate({
				Counter: $(this).text()
			}, {
				duration: 800,
				easing: 'swing',
				step: function (now) {
					$(this).text(Math.ceil(now));
				}
			});
		});
	}, 300);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function editAssenzaInd(id) {
	var id_utente = localStorage['id_user']; // ID utente
	var id_assenza = $(id).attr("id");
	// Visualizza il loader
	myApp.showPreloader('Caricamento')
	$.ajax({
		type: 'POST',
		url: 'https://turnips.it/sql_db/app/3_0/calendario/assenza_singola.php',
		data: {
			id_utente: id_utente,
			id_assenza: id_assenza,
		},
		dataType: 'json',
		success: function (msg) {
			setTimeout(function () {
				// Nasconde il loader
				myApp.hidePreloader();

				$.each(msg, function (key, value) {
					$('#id_ass_edit').val(value.id_ass);
					$('#giorno_ass_edit').val(value.giorno_ass);
					$("#tipo_ass_edit").val(value.tipo_ass);
					$('#note_ass_edit').val(value.note_ass);
					// Se presente visualizzo data ultimo aggiornamento
					if ($.trim(value.data_aggiornamento_ass) != "") {
						$("#last_up_ass").html('Ultima modifica effettuata il: ' +
							value.data_aggiornamento_ass);
					};
				}); //end .each
			}, 1000); //end .timeout
		}, // Fine success
		error: function () {
			// Nasconde il loader
			myApp.hidePreloader();
			console.log('errore ajax loadTurni');
		}
	}); // Fine chiamata ajax
} // editAssenzaInd(id)
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function dettaglioIndennita(nome, tipo) {
	var tipo_indennita = tipo;
	var titolo_indennita = nome;
	var id_utente = localStorage['id_user'];
	var mese = $('#mese_idennita-s').val();
	var anno = $('#anno_idennita-s').val();
	$.ajax({
		type: 'POST',
		url: 'https://turnips.it/db_sql/riepilogo/dettaglio_indennita.php',

		data: {
			id_utente: id_utente,
			tipo_indennita: tipo_indennita,
			mese: mese,
			anno: anno
		},
		dataType: 'json',
		success: function (data) {
			//console.log(data);
			$('#titolo_indennita_dettaglio').html(nome);
			if (data != null) {
				$.each(data, function (key, value) {
					$('#lista-dettaglio-indennita').append('<li>' +
						'<a href="pages/edit-turno-indennita.html" onclick="editSingolaInd(this);" id="' +
						value.id + '" class="item-link item-content">' +
						'<div class="item-media"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></div>' +
						'<div class="item-inner">' +
						'<div class="item-title-rec-day"><span class="label-rec-day">Data:</span>' +
						date2ita(value.data) + '</div>' +
						'</div>' +
						'</a>' +
						'</li>'
					);
				}); //end .each
			} else {
				$('#no-rec').css('display', 'block');
			}
		} // success
	});
} // dettaglioIndennita(nome, tipo)
////////////////////////////////////////////////////////////////////////////////////////////////////////////
/* SINGOLA INDENNITA' */
function editSingolaInd(id) {
	var id_utente = localStorage['id_user']; // ID utente
	var id_turno = $(id).attr("id");
	// Visualizza il loader
	myApp.showPreloader('Caricamento')
	$.ajax({
		type: 'POST',
		url: 'https://turnips.it/sql_db/app/3_0/calendario/turno_singolo.php',
		data: {
			id_utente: id_utente,
			id_turno: id_turno,
		},
		dataType: 'json',
		success: function (msg) {
			setTimeout(function () {
				// Nasconde il loader
				myApp.hidePreloader();
				//console.log(msg);
				$.each(msg, function (key, value) {
					$('#id_turno_edit').val(value.id_turno);
					$('#giorno-edit').val(value.giorno);
					$('#orario-edit').val(value.orario);
					$('#tipo_serv-edit').val(value.tipo_serv);
					$('#ore_notte-edit').val(value.ore_notti);
					$('#nota-edit').val(value.nota);
					// Attivo il toogle se ci sono indennità doppie
					if ($.trim(value.ticket_pasto) == "2") {
						$('#doppio_pasto-ck-edit').prop("checked", true);
						$('#sl_idennita_edit').find(
							'option[value="sl_ticket_pasto-edit"]').attr('selected', true);
					};
					if ($.trim(value.ser_esterno) == "2") {
						$('#doppia_esterna-ck-edit').prop("checked", true);
						$('#sl_idennita_edit').find(
							'option[value="sl_servizio_esterno-edit"]').attr('selected',
							true);
					};
					if ($.trim(value.recuper_riposo) == "1") {
						$('#recupero_giorno-ck-edit').prop("checked", true);
					};
					// Carico le indennità nella smart select
					if ($.trim(value.ticket_pasto) == "1") {
						$('#sl_idennita_edit').find(
							'option[value="sl_ticket_pasto-edit"]').attr('selected', true);
					};
					if ($.trim(value.mancati_pasto) == "1") {
						$('#sl_idennita_edit').find(
							'option[value="sl_ticket_mancato_pasto-edit"]').attr(
							'selected', true);
					};
					if ($.trim(value.o_p_fuori) == "1") {
						$('#sl_idennita_edit').find('option[value="sl_op_fuori-edit"]')
							.attr('selected', true);
					};
					if ($.trim(value.o_p_cek) == "1") {
						$('#sl_idennita_edit').find('option[value="sl_op_sede-edit"]').attr(
							'selected', true);
					};
					if ($.trim(value.o_p_pernotto) == "1") {
						$('#sl_idennita_edit').find(
							'option[value="sl_op_pernotto-edit"]').attr('selected', true);
					};
					if ($.trim(value.ser_esterno) == "1") {
						$('#sl_idennita_edit').find(
							'option[value="sl_servizio_esterno-edit"]').attr('selected',
							true);
					};
					if ($.trim(value.reperibilita) == "1") {
						$('#sl_idennita_edit').find(
							'option[value="sl_reperibilita-edit"]').attr('selected', true);
					};
					if ($.trim(value.cambio_turno) == "1") {
						$('#sl_idennita_edit').find(
							'option[value="sl_cambio_turno-edit"]').attr('selected', true);
					};
					if ($.trim(value.rip_com) == "1") {
						$('#sl_idennita_edit').find(
							'option[value="sl_id_compensazione-edit"]').attr('selected',
							true);
					};
					if ($.trim(value.serv_missione) == "1") {
						$('#sl_idennita_edit').find(
							'option[value="sl_servizio_missione-edit"]').attr('selected',
							true);
					};
					// Se presente visualizzo data ultimo aggiornamento
					if ($.trim(value.data_aggiornamento) != "") {
						$("#last_up_turno").html('Ultima modifica effettuata il: ' +
							value.data_aggiornamento);
					};
				}); //end .each
			}, 1000); //end .timeout
		}, // Fine success
		error: function () {
			// Nasconde il loader
			myApp.hidePreloader();
			console.log('errore ajax loadTurni');
		}
	}); // Fine chiamata ajax
} // editSingolaInd(id)
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Serve per verifica se è possibile scalare ore di straordinario /RC
function verificaScaloStrao() {
	var id_utente = localStorage['id_user'];
	var scalo_strao = $('#scalo_strao').val();
	var data_utente = $('#giorno_ass').val();

	$.ajax({
		type: 'POST',
		url: 'https://turnips.it/db_sql/_add/verifica_scalo_strao.php',
		data: {
			id_utente: id_utente,
			data_utente: data_utente,
			scalo_strao: scalo_strao,
		},
		dataType: 'json',
		success: function (data) {
			if (data.risultato == "0") {
				myApp.alert(
					'Non hai abbastanze ore da poterle convertire in un <br> <strong>Riposo Compensativo!</strong><br><span style="color:red; font-size:10px;">(Ore disponibili:' +
					data.ore_disponibili + ')</span> ', 'Attenzione!');
			} else if (data.risultato == "2") {
				myApp.modal({
					title: 'Attenzione!',
					text: 'Per il mese di <strong>' + data.mese_alert + '</strong> ancora non hai impostato le ore di straordinario, da poter utilizzare come RC.<br>Vuoi farlo?',
					buttons: [{
						text: '<span style="color:red">NO</span>'
					}, {
						text: 'SI',
						bold: true,
						onClick: function () {
							mainView.router.loadPage('pages/ind-strao.html');
						}
					}, ]
				})
			}
		} // success
	});
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Visualizza il riepilogo mensile delle indennità. Il mese corrente o uno scelto dal menu
function caricaAssenzeScelta() {
	var id_utente = localStorage['id_user'];
	$("#anno_idennita_assenze").change(function () {
		var anno = $('#anno_idennita_assenze').val();
		$.ajax({
			type: 'POST',
			url: 'https://turnips.it/db_sql/riepilogo/assenze_scelta.php',
			data: {
				id_utente: id_utente,
				anno: anno
			},
			dataType: 'json',
			success: function (data) {
				countNumber();
				// Usufruiti
				$("#ass-coa").html(data.coa);
				$("#ass-coc").html(data.coc);
				$("#ass-rip-legge").html(data.riposo_legge);
				$("#ass-cong-strao-mala").html(data.con_malattia);
				$("#ass-aspettativa").html(data.aspettativa);
				$("#ass-parentale").html(data.parentale);
				$('#ass-recupero_riposo').html(data.recupero_risposo_close);
				// Spettanti
				$("#set-ass-coa").html(data.set_coa);
				$("#set-ass-coc").html(data.set_coc);
				$("#set-ass-rip-legge").html(data.set_riposo_legge);
				$("#set-ass-cong-strao-mala").html(data.set_con_malattia);
				$("#set-ass-aspettativa").html(data.set_aspettativa);
				$("#set-ass-parentale").html(data.set_parentale);
				// Disponibili
				$("#disp-ass-coa").html(data.disp_coa);
				$("#disp-ass-coc").html(data.disp_coc);
				$("#disp-ass-rip-legge").html(data.disp_riposo_legge);
				$("#disp-ass-cong-strao-mala").html(data.disp_con_malattia);
				$("#disp-ass-aspettativa").html(data.disp_aspettativa);
				$("#disp-ass-parentale").html(data.disp_parentale);
				$('#disp-ass-recupero-riposo').html(data.giorno_recupero);
				// Assenze Singole
				$('#ass-malattia-bimbo').html(data.bimbo);
				$('#ass-dona-sangue').html(data.sangue);
				$('#ass-lex-104').html(data.l104);
				$('#ass-studio').html(data.studio);
				$('#ass-mandato-am').html(data.mandato);
				$('#data-scadenza-parentale').html('Scadenza il: ' + date2ita(data.scadenza_parentale));
				$('#anno_idennita_assenze').val(data.anno_assenze);
			} //end success
		}) // end ajax
	}); //change
} // caricaAssenzeScelta
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Carica il testo della privacy
function policePrivacy() {

	$('#legal_box').load('https://turnips.it/db_sql/testo_privacy.php');
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function avvisoHome() {

	$.ajax({
		type: 'POST',
		url: 'https://turnips.it/db_sql/settings/avviso_home.php',
		dataType: 'json',
		success: function (value) {
			var lettoNotificaHome = localStorage['avvisoHome'];
			if (value.ntf == '1' && lettoNotificaHome == "false") {
				// $('#titolo-notifica').html(value.titolo);
				// $('#messaggio-box-notifica-home').html(value.messaggio);
				// $('#box-notifica-home').css('display', 'block');
				myApp.addNotification({
					title: value.titolo,
					//subtitle: 'New message from John Doe',
					message: value.messaggio,
					media: value.icona,
					onClose: function () {
						localStorage['avvisoHome'] = true;
					}
				});
			}
		} // end success
	});

}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function SalvaTurnoMultiplo() {
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app
	var dati = $("#form_add_turno_multiplo").serialize();
	// Validazione del form
	var dataOggi = dataMinYear(dataAttuale(), 183);

	if (isEmpty(id_utente)) {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;Errore salvataggio code #1",
			"box-errore", "5000");
		return false;
	} else if (isEmpty($('#giorno_inizio_multiplo').val())) {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;Non hai inserito la data inzio turno",
			"box-errore", "5000");
		return false;
	} else if (isEmpty($('#giorno_fine_multiplo').val())) {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;Non hai inserito la data fine turno",
			"box-errore", "5000");
		return false;
	} else if ($('#tipo_servizio_multiplo').val() === 'NO') {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;Non hai inserito il servizio!",
			"box-errore", "5000");
		return false;
	} else if ($.trim($("#resultTag").html()) == '') {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;Non hai inserito lo schema.",
			"box-errore", "5000");
		return false;
	} else if ($('#giorno_fine_multiplo').val() > dataOggi) {

		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;Puoi inserire turni  per un massimo di 6 mesi alla volta",
			"box-errore", "5000");
		return false;
	} else {
		// Loader Bottone
		$('#btn-salva-turno-multiplo').html('<span style="width:32px; height:32px; margin-top:5px;" class="preloader preloader-white">');
		$('#btn-salva-turno-multiplo').attr("disabled", true);

		var getSchema = getTags();
		var schema = getSchema.toString();

		$.ajax({
			type: "POST",
			url: 'https://turnips.it/db_sql/_add/4_1_8/salva_turno_multiplo.php',
			data: dati + "&id_utente=" + id_utente + "&versione=" + ver + "&schema=" + schema,
			dataType: "html",
			success: function (msg) {

				loadTurni();
				loadAssenze();
				$('#form_add_turno_multiplo')[0].reset(); //Reset form
				// Visualizzo messaggio di conferma OK
				showToast(
					"<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Dati Salvati!",
					"box-ok", "2000");
				setTimeout(function () {
					$('#btn-salva-turno-multiplo').html('Salva');
					$('#btn-salva-turno-multiplo').attr("disabled", false);
					// Data corrente per input date
					document.getElementById('giorno_inizio_multiplo').value = dataAttuale();
					$('#calendar').swipeCalendar('refetchEvents');
				}, 2000);
			},
			error: function () {
				console.log('Chiamata ajax fallita!');
			}
		}); //ajax
	} // enf if validazione form
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funziona elimina tutti i turni le assenze gli strao inseriti, senza però eliminare l'account
function resetTuttiDati() {
	var id_utente = localStorage['id_user'];
	myApp.confirm('Sei sicuro di eliminare tutti i dati?<br><span class="blue">Una volta confermato non sarà più possibile recuperare i dati!</span>', '<span class="rosso">Attenzione!</span>',
		function () {
			// Quando si preme sul pulsante OK
			$.ajax({
				type: "POST",
				url: 'https://turnips.it/db_sql/_delete/elimina_tutti_dati.php',
				data: "id_utente=" + id_utente,
				dataType: "html",
				success: function (msg) {

					loadAssenze();
					loadTurni();
					locadStraoProg();
					locadStraoEme();
					// Visualizzo messaggio di conferma OK
					showToast(
						"<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Dati Eliminati!",
						"box-ok", "2000");
					setTimeout(function () {
						$('#calendar').swipeCalendar('refetchEvents');
					}, 2000);
				},
				error: function () {
					console.log('Chiamata ajax fallita!');
				}
			}); //ajax
		},
		function () {
			//Quando si preseme sul pulsante ANNULLA
		}
	); //  Fine alert scelta ok-annulla
} //eliminaAssenza()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funziona elimina tutti i turni le assenze gli strao inseriti, senza però eliminare l'account
function eliminaTurniMultipli() {
	var id_utente = localStorage['id_user']; // ID utente
	var dati = $("#form_elimina_turni_multipli").serialize();
	// Validazione del form
	if (isEmpty($('#tipo').val())) {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;Non hai selezionato il tipo!",
			"box-errore", "3000");
		return false;
	}
	else if (isEmpty($('#giorno_inizio_multiplo').val())) {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;Non hai inserito la data iniziale!",
			"box-errore", "3000");
		return false;
	} else if (isEmpty($('#giorno_fine_multiplo').val())) {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;Non hai inserito la data finale.",
			"box-errore", "3000");
		return false;
	} else if ($('#giorno_fine_multiplo').val() < $('#giorno_inizio_multiplo').val()) {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;La data finale deve essere maggiore<br> rispetto al quella inziale",
			"box-errore", "4000");
		return false;
	} else {
		myApp.confirm('Sei sicuro di proseguire verranno eliminati i dati dal ' + date2ita($('#giorno_inizio_multiplo').val()) + ' al ' + date2ita($('#giorno_fine_multiplo').val()) + '?<br><span class="blue">Una volta confermato non sarà più possibile recuperare i dati!</span>', '<span class="rosso">Attenzione!</span>',
			function () {
				// Quando si preme sul pulsante OK
				$.ajax({
					type: "POST",
					url: 'https://turnips.it/db_sql/_delete/elimina_multiplo.php',
					data: dati + "&id_utente=" + id_utente,
					dataType: "html",
					success: function (msg) {
						$('#form_elimina_turni_multipli')[0].reset(); //Reset form
						loadAssenze();
						loadTurni();
						locadStraoEme();
						locadStraoProg();
						// Visualizzo messaggio di conferma OK
						showToast(
							"<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Dati Eliminati!",
							"box-ok", "2000");
						setTimeout(function () {
							$('#calendar').swipeCalendar('refetchEvents');
						}, 2000);
					},
					error: function () {
						console.log('Chiamata ajax fallita!');
					}
				}); //ajax
			},
			function () {
				//Quando si preme sul pulsante ANNULLA
			}
		); //  Fine alert scelta ok-annulla
	}
} //eliminaAssenza()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Converte le date in italiano (Solo nella visualizzazione, le date vengono sempre salvata in formato US)
function date2ita(data) {
	return data.replace(/(\d{4}).(\d{2}).(\d{2})/, "$3.$2.$1");
} // date2ita()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function salvaVisualeEtichetta() {
	var id_utente = localStorage['id_user']; // ID utente
	var dati = $("#form_scelta_etichetta").serialize();

	// Loader Bottone
	$('#btn-salva-visuale-etichetta').html('<span style="width:32px; height:32px; margin-top:5px;" class="preloader preloader-white">');
	$('#btn-salva-visuale-etichetta').attr("disabled", true);
	$.ajax({
		type: "POST",
		url: 'https://turnips.it/db_sql/calendario/scelta_etichetta.php',
		data: dati + "&id_utente=" + id_utente,
		dataType: "html",
		success: function (msg) {
			caricoEventiCalendario();
			chekUpdateDB();
			$('#form_scelta_etichetta')[0].reset(); //Reset form
			// Visualizzo messaggio di conferma OK
			showToast(
				"<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Dati Salvati!",
				"box-ok", "2000");
			setTimeout(function () {
				$('#btn-salva-visuale-etichetta').html('Salva');
				$('#btn-salva-visuale-etichetta').attr("disabled", false);
				$('#calendar').swipeCalendar('refetchEvents');
			}, 2000);
		},
		error: function () {
			console.log('Chiamata ajax fallita!');
		}
	}); //ajax
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funzione per cambiare la password del utente
function gestioneOreStrao() {

	var id_utente = localStorage['id_user']; // ID utente
	var dati = $("#gestione_strao_mensile").serialize();

	var ore_del_mese = parseInt($('#totale_ore').val());
	// dal form
	var ore_pagate = parseInt(((isEmpty($('#ore_pagate').val())) ? '0' : $('#ore_pagate').val() * 60));
	var min_pagate = parseInt(((isEmpty($('#min_pagate').val())) ? '0' : $('#min_pagate').val()));
	var ore_da_pagare = parseInt(((isEmpty($('#ore_da_pagare').val())) ? '0' : $('#ore_da_pagare').val() * 60));
	var min_da_pagare = parseInt(((isEmpty($('#min_da_pagare').val())) ? '0' : $('#min_da_pagare').val()));
	var ore_in_rc = parseInt(((isEmpty($('#ore_in_rc').val())) ? '0' : $('#ore_in_rc').val() * 60));
	var min_in_rc = parseInt(((isEmpty($('#min_in_rc').val())) ? '0' : $('#min_in_rc').val()));

	var totale = ore_pagate + min_pagate + ore_da_pagare + min_da_pagare + ore_in_rc + min_in_rc;

	if (ore_del_mese < totale) {
		myApp.alert('Non hai abbastanze ore!', 'Attenzione!');
	} else {
		// Loader
		$('#btn_gestione_ore_stro').html('<span style="width:32px; height:32px; margin-top:5px;" class="preloader preloader-white">');
		$.ajax({
			type: "POST",
			url: 'https://turnips.it/db_sql/_add/4_1_5/gestione_strao.php',
			data: dati + "&id_utente=" + id_utente,
			dataType: "html",
			success: function (msg) {

				showToast("<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Dati Salvati!",
					"box-ok", "2000");
				setTimeout(function () {
					$("#btn_gestione_ore_stro").html("Salva");
				}, 2000);

			},
			error: function () {
				console.log('Chiamata ajax fallita!');
			}
		}); //ajax
	}
} // gestioneOreStrao()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function isEmpty(value) {
	try {

		if (value === null || value === undefined || value === '' || value.length === 0) {
			return true;
		}

	} catch (ex) {
		console.log(ex);

	}
	return false;

}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function caricaFaq() {

	$.ajax({
		type: "POST",
		url: 'https://turnips.it/db_sql/faq.php',
		dataType: "json",
		success: function (data) {

			$.each(data, function (key, value) {
				$('#ul_faq').append('<li class="accordion-item"><a href="#" class="item-content item-link">' +
					'<div class="item-inner">' +
					'<div class="item-title text-blod">' + value.titolo + '</div>' +
					'</div>' +
					'</a>' +
					'<div class="accordion-item-content">' +
					'<div class="content-block">' +
					'<p>' + value.descrizione + '</p>' +
					'</div>' +
					'</div>' +
					'</li>'
				);
			}); //end .each
		},
		error: function () {
			console.log('Chiamata ajax fallita!');
		}
	}); //ajax
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function getTurnari() {

	myApp.showPreloader('Caricamento...')
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app
	$.ajax({
		type: 'POST',
		url: 'https://turnips.it/db_sql/calendario/4_1_5/getTurnari.php',
		data: "id_utente=" + id_utente,
		dataType: 'json',
		success: function (data) {
			myApp.hidePreloader();
			$('#turni_li').empty();
			$.each(data, function (key, value) {
				if (isEmpty(value.nome)) {
					var nome = 'nessun nome';
				} else {
					var nome = value.nome;
				}
				$('#turni_li').append('<li>' +
					'<a href="#" class="item-link item-content"  onclick="dettaglioTurnario(' + value.id_turnario + ');">' +
					'<div class="item-media"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></div>' +
					'<div class="item-inner">' + nome + '</div>' +
					'</a>' +
					'</li>'
				);
			}); //end .each
		}
	});
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function dettaglioTurnario(value) {

	myApp.showPreloader('Caricamento...')

	var id_utente = localStorage['id_user']; // ID utente

	$.ajax({
		type: 'POST',
		url: 'https://turnips.it/db_sql/calendario/4_1_5/getSingoloTurnario.php',
		data: "id_utente=" + id_utente + "&id_turnario=" + value,
		dataType: 'json',
		success: function (data) {
			myApp.hidePreloader();
			myApp.popup('.popup-nuovo-turnario');
			$.each(data, function (key, value) {
				if (isEmpty(value.nome)) {
					var nome = 'nessun nome';
				} else {
					var nome = value.nome;
				}

				var schema_turnario = JSON.parse(value.schema_turnario);

				$('#id-turnario').val(value.id_turnario);
				$('#nome-turnario').val(nome);
				$('#start-data-turnario').val(value.data_start);
				$('#end-data-turnario').val(value.data_end);
				//$('#turnario_select').val(schema_turnario);
				var idTurnario = $('#id-turnario').val();
				// Gestione Bottoni
				if (isEmpty(idTurnario)) {
					$('#btn-edit-turnario').css('display', 'none');
					$('#btn-salva-turnario').css('display', 'block');
				} else {
					$('#btn-edit-turnario').css('display', 'block');
					$('#btn-salva-turnario').css('display', 'none');
				}

				initTags(schema_turnario, 'carico');

			}); //end .each
		}
	});

}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function salvaDataTurnarioSingola(value) {

	// Dati dal form
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app

	if ($('#nome-turnario').val() === '') {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;Non hai inserito il nome",
			"box-errore", "3000");
		return false;
	} else if ($('#start-data-turnario').val() === '') {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;Non hai inserito la data iniziale",
			"box-errore", "3000");
		return false;
	} else if ($('#end-data-turnario').val() === '') {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;Non hai inserito la data finale",
			"box-errore", "3000");
		return false;
	} else if ($('#end-data-turnario').val() == $('#start-data-turnario').val()) {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;La data finale deve essere diversa da quella inziale",
			"box-errore", "3000");
		return false;
	} else if ($('#end-data-turnario').val() < $('#start-data-turnario').val()) {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;La data finale non puo essere minore di quella inziale",
			"box-errore", "3000");
		return false;
	} else if ($.trim($("#resultTag").html()) == '') {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;Non hai inserito lo schema.",
			"box-errore", "3000");
		return false;
	} else {

		var id_turnario = $('#id-turnario').val();
		var data_start = $('#start-data-turnario').val();
		var data_end = $('#end-data-turnario').val();
		var nome_turnario = $('#nome-turnario').val();
		var schema = JSON.stringify(getTags());

		myApp.showPreloader('Caricamento...')

		$.ajax({
			type: "POST",
			url: 'https://turnips.it/db_sql/calendario/4_1_5/salva_data_turnario.php',
			data: {
				id_utente: id_utente,
				update: value,
				id_turnario: id_turnario,
				data_start: data_start,
				data_end: data_end,
				nome_turnario: nome_turnario,
				schema: schema
			},
			dataType: "json",
			success: function (msg) {

				setTimeout(function () {
					// Nasconde il loader
					myApp.hidePreloader();
					showToast("<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Data Salvata!", "box-ok", "2000");
					getTurnari();
					loadTurnario();
				}, 1000); //end .timeout
				setTimeout(function () {
					myApp.closeModal();
				}, 1500); //end .timeout
			},
			error: function () {
				console.log('Chiamata ajax fallita!');
			}
		}) // end ajax

	} // end else
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function eliminaTurnarioSingolo() {

	var id_utente = localStorage['id_user']; // ID utente
	var id_turnario = $('#id-turnario').val(); // ID utente

	myApp.confirm('Confermi di voler eliminare il turnario?', 'TurniPS',
		function () {
			// Quando si preme sul pulsante OK
			myApp.showPreloader('Attendere...')
			$.ajax({
				type: "POST",
				url: 'https://turnips.it/db_sql/calendario/4_1_5/eliminaSingoloTurnario.php',
				data: "id_utente=" + id_utente + "&id_turnario=" + id_turnario,
				dataType: "html",
				success: function (msg) {
					$('#btn-edit-turnario').css('display', 'none');
					$('#btn-salva-turnario').css('display', 'block');

					setTimeout(function () {
						// Nasconde il loader
						myApp.hidePreloader();
						showToast("<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Data Eliminata!", "box-ok", "2000");
						getTurnari();
						loadTurnario();
					}, 1000); //end .timeout

					setTimeout(function () {
						myApp.closeModal();

					}, 3500);

				},
				error: function () {
					console.log('Chiamata ajax fallita!');
				}
			}) // end ajax
		},
		function () {
			//Quando si preseme sul pulsante ANNULLA
		}
	); //  Fine alert scelta ok-annulla
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function restBtnTurnario() {
	// Gestione Bottoni

	var idTurnario = $('#id-turnario').val();

	if (isEmpty(idTurnario)) {
		$('#btn-edit-turnario').css('display', 'none');
		$('#btn-salva-turnario').css('display', 'block');
	} else {
		$('#btn-edit-turnario').css('display', 'block');
		$('#btn-salva-turnario').css('display', 'none');
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fakeLoadEventi() {

	var modalCaricamento = myApp.modal({
		title: 'Caricamento Calendario',
		text: `<div class="list-block inset">
		<ul>
		  <li>
			<div class="item-content">
			  <div class="item-media"><i class="fa fa-calendar" aria-hidden="true"></i></div>
			  <div class="item-inner">
				<div class="item-title">Turni</div>
				<div class="item-after" id="fakeload1"><span class="preloader preloader-red" style="width:24px; height:24px" ></span></div>
			  </div>
			</div>
		  </li>
		  <li>
			<div class="item-content">
			  <div class="item-media"><i class="fa fa-coffee" aria-hidden="true"></i></div>
			  <div class="item-inner">
				<div class="item-title">Assenze</div>
				<div class="item-after" id="fakeload2"><span class="preloader preloader-red" style="width:24px; height:24px" ></span></div>
			  </div>
			</div>
		  </li>
		  <li>
			<div class="item-content">
			  <div class="item-media"><i class="fa fa-hourglass-start" aria-hidden="true"></i></div>
			  <div class="item-inner">
				<div class="item-title">Straordinario</div>
				<div class="item-after" id="fakeload3"><span class="preloader preloader-red" style="width:24px; height:24px" ></span></div>
			  </div>
			</div>
		  </li>
		  <li>
			<div class="item-content">
			  <div class="item-media"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></div>
			  <div class="item-inner">
				<div class="item-title">Turnario</div>
				<div class="item-after" id="fakeload4"><span class="preloader preloader-red" style="width:24px; height:24px" ></span></div>
			  </div>
			</div>
		  </li>

		  </ul>
		  <div  id="fakeload5" class="list-block-label animated bounceIn" style="display:none;"></div>
	</div>`
	});

	$('.modal-title ').css('font-size', '16px');
	$('.modal-inner').addClass('fakeLoad');
	$('.list-block').css('margin', '5px 0');

	setTimeout(function () {
		$('#fakeload1').html('<span class="animated bounceIn color-verde"><i class="fa fa-check-circle fa-lg" aria-hidden="true"></i></span>');
	}, 1500);
	setTimeout(function () {
		$('#fakeload2').html('<span class="animated bounceIn color-verde"><i class="fa fa-check-circle fa-lg" aria-hidden="true"></i></span>');
	}, 2500);
	setTimeout(function () {
		$('#fakeload3').html('<span class="animated bounceIn color-verde"><i class="fa fa-check-circle fa-lg" aria-hidden="true"></i></span>');
	}, 3500);
	setTimeout(function () {
		$('#fakeload4').html('<span class="animated bounceIn color-verde"><i class="fa fa-check-circle fa-lg" aria-hidden="true"></i></span>');
	}, 4500);
	setTimeout(function () {
		$('#fakeload5').css('display', 'block');

		$('#fakeload5').addClass('fakeLoadFine');

		$('#fakeload5').html('<span class="animated flip color-verde ">Caricamento completato</span>');
	}, 5000);
	setTimeout(function () {
		myApp.closeModal(modalCaricamento);
	}, 6000);

}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function IDGenerator() {

	this.length = 4;
	this.timestamp = +new Date;

	var _getRandomInt = function (min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	this.generate = function () {
		var ts = this.timestamp.toString();
		var parts = ts.split("").reverse();
		var id = "";

		for (var i = 0; i < this.length; ++i) {
			var index = _getRandomInt(0, parts.length - 1);
			id += parts[index];
		}

		return 'tags-' + id;
	}


}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function dataMinYear(date_str, incrementor) {

	var parts = date_str.split("-");
	var dt = new Date(
		parseInt(parts[0], 10), // year
		parseInt(parts[1], 10) - 1, // month (starts with 0)
		parseInt(parts[2], 10) // date
	);
	dt.setTime(dt.getTime() + incrementor * 86400000);
	parts[0] = "" + dt.getFullYear();
	parts[1] = "" + (dt.getMonth() + 1);
	if (parts[1].length < 2) {
		parts[1] = "0" + parts[1];
	}
	parts[2] = "" + dt.getDate();
	if (parts[2].length < 2) {
		parts[2] = "0" + parts[2];
	}
	return parts.join("-");
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
var arrTags = new Array();

function addTurnoSchema(nome, sigla) {

	var generator = new IDGenerator();

	var index = generator.generate();

	var tagHtml = `<div class="chip animated bounceIn" id="${index}">
	<div class="chip-label"><i class="fa fa-angle-double-right" aria-hidden="true"></i> ${nome}</div>
	<a href="#" class="chip-delete" onclick="removeSchema('${index}')"></a>
	</div>`;

	$('#resultTag').append(tagHtml);

	var tagsArr = {
		'id': index,
		'name': sigla
	}

	arrTags.push(tagsArr);

}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function removeSchema(idTag) {

	var chip = $('#' + idTag);

	myApp.confirm('Vuoi eliminare?', 'TurniPS', function () {

		chip.remove();

		var apps = arrTags;

		for (var i = 0; i < apps.length; i++) {

			if (apps[i].id == idTag) {
				apps.splice(i, 1);
				break;
			}

		}

		if ($.trim($("#resultTag").html()) == '') {
			console.log('vuoto');
		} else {
			console.log('pieno');

		}

	});
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function getTags() {

	var fine = []

	var cc = arrTags;

	for (var i = 0; i < cc.length; i++) {

		fine.push(arrTags[i].name);

	}

	return fine;

}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function initTags(obj, tipo) {

	arrTags = new Array()

	console.log(obj);

	if (tipo == 'star') {
		$("#resultTag").empty();
	}

	for (var i = 0; i < obj.length; i++) {

		switch (obj[i]) {
			case 's':
				var nome = "Sera";
				break;
			case 'n':
				var nome = "Notte";
				break;
			case 'p':
				var nome = "Pomeriggio";
				break;
			case 'm':
				var nome = "Mattina";
				break;
			case 'r':
				var nome = "Riposo";
				break;
			default:
				var nome = "";
		}

		addTurnoSchema(nome, obj[i])
	}

}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function spazioAppUtilizzato() {

	var data = '';

	try {
		for (var key in window.localStorage) {

			if (window.localStorage.hasOwnProperty(key)) {
				data += window.localStorage[key];

				var spazio = ((window.localStorage[key].length * 16) / (8 * 1024)).toFixed(2) + ' KB';

				if (dettaglioSpazioDaVisualizzare(key)) {

					var li = `<li class="item-content">
						<div class="item-media"><i class="fa fa-hdd-o" aria-hidden="true"></i></div>
						<div class="item-inner">
							<div class="item-title">${nomeSpazioDaVisualizzare(key)}</div>
							<div class="item-after">${spazio}</div>
						</div>
						</li>`;

					$('#liSpazioLocalstorage').append(li);
				}
			}
			$('#spaziotot').html(((data.length * 16) / (8 * 1024)).toFixed(2) + ' KB');
			var sconto = calcolaPerc(5120, ((data.length * 16) / (8 * 1024)).toFixed(2));
			$('#spaziototpercentuale').html(sconto + '%');

		}
	} catch (ex) {
		console.log(ex);
	}

}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function dettaglioSpazioDaVisualizzare(key) {

	switch (key) {
		case 'cal-strao-eme':
		case 'cal-assenze':
		case 'cal-turni ':
		case 'dati-utente':
		case 'cal-strao-pro':
		case 'turnario-array':
			return true;
			break;
		default:
			return false
	}

}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function nomeSpazioDaVisualizzare(key) {

	switch (key) {
		case 'cal-strao-eme':
			return "Straordinario Emergente";
			break;
		case 'cal-assenze':
			return "Assenze";
			break;
		case 'cal-turni ':
			return "Turni";
			break;
		case 'dati-utente':
			return "Dati Utente";
			break;
		case 'cal-strao-pro':
			return "Straordinario Programmato";
			break;
		case 'turnario-array':
			return "Turnario";
			break;

		default:
			return false
	}

}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function calcolaPerc(tot, num) {
	return ((num / tot) * 100).toFixed(0);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function logSessioni() {
	var id_utente = localStorage['id_user'];
	var ver = localStorage['versione']; // Versione app
	// Controllo servizi personali
	$.ajax({
		type: 'POST',
		url: 'https://turnips.it/db_sql/settings/logAccessi.php',
		data: "id_utente=" + id_utente + "&versione=" + ver,
		dataType: 'json',
		success: function (value) {

			//console.log(value);

		} // end success
	});
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////