var admobid = {};
// select the right Ad Id according to platform
if (/(android)/i.test(navigator.userAgent)) {
  admobid = { // for Android
    banner: 'ca-app-pub-5254404342158275/3393525740',
    interstitial: 'ca-app-pub-5254404342158275/4870258941'
  };
} else if (/(ipod|iphone|ipad)/i.test(navigator.userAgent)) {
  admobid = { // for iOS
    banner: 'ca-app-pub-5254404342158275/3393525740',
    interstitial: 'ca-app-pub-5254404342158275/4870258941'
  };
} else {
  admobid = { // for Windows Phone
    banner: 'ca-app-pub-5254404342158275/3393525740',
    interstitial: 'ca-app-pub-5254404342158275/4870258941'
  };
}
console.log('--------------------->  script.js');

function domLoaded() {
  document.addEventListener("deviceready", onDeviceReady, false);
}
//////////////////////////////////////////////////////////////////////////////
function onDeviceReady() {
  // Verifico licenza app
  licenza();
  setTimeFullAdv();
  // Salvo il token nel DB dopo 10 sec dall'apertura dell'app.
  setTimeout(function () {
    saveTokenFirebase()
  }, 10000); //end .timeout
  ///////////////////////////////////////////////////
  // Ricezione della Notifica
  FCMPlugin.onNotification(
    function (data) {
      //Quando si  clicca sulla notifica
      if (data.wasTapped) {
        // Applicazione chiusa o in background
        if (data.tipologia == 1) {
          // Tipologia 1 viene utilizzata per i messaggi dalla chat
          mainView.router.loadPage('pages/chat.html');
        } else if (data.tipologia == 2) {
          // Tipologia 2 viene utilizzata per le donazioni
          myApp.alert(data.messaggio, data.titolo, function () {
            refreshApp();
            licenza();
          });
        } else {
          // Per tutte le altre notifiche con messaggio.
          myApp.alert(data.messaggio, data.titolo);
        }
        // fine wasTapped
      } else {
        // Applicazione aperta
        if (data.tipologia == 1) {
          // Tipologia 1 viene utilizzata per i messaggi dalla chat
          $('.notifica-chat').addClass('notifica-chat-attiva');
          setTimeout(function () {
            $('.notifica-chat').removeClass('notifica-chat-attiva');
          }, 7000);
        } else if (data.tipologia == 2) {
          // Tipologia 2 viene utilizzata per le donazioni
          myApp.alert(data.messaggio, data.titolo, function () {
            refreshApp();
            AdMob.removeBanner();
            licenza();
          });
        } else {
          myApp.alert(data.messaggio, data.titolo);
        }
        // fine else
      }
    },
    function (msg) {
      console.log('onNotification callback successfully registered: ' + msg);
    },
    function (err) {
      app.showError('Error registering onNotification callback: ' + err);
      console.log('Error registering onNotification callback: ' + err);
    }
  );
  ///////////////////////////////////////////////////
}
//////////////////////////////////////////////////////////////////////////////
function licenza() {
  var id_utente = localStorage['id_user'];
  // Controllo servizi personali
  $.ajax({
    type: 'POST',
    url: 'https://turnips.it/db_sql/settings/controllo_licenza.php',
    data: "id_utente=" + id_utente,
    dataType: 'json',
    success: function (value) {
      // Versione database user
      localStorage['versione-db-user'] = value.versione_db_user;
      if ($.trim(value.licenza) == "Free") {
        localStorage['licenza-app'] = 'Free';
        adSetter();
      } else {
        localStorage['licenza-app'] = 'Premium';
        localStorage['licenza-app-durata'] = value.durata_licenza;
      }
    } // end success
  });
} // licenza()
///////////////////////////////////////////////////////////////////////////////
// Salvataggio/Update Token Firebase
function saveTokenFirebase() {
  FCMPlugin.getToken(function (token) {
    //myApp.alert(token , 'TOKEN');
    var id_utente = localStorage['id_user'];
    $.ajax({
      type: 'POST',
      url: "https://turnips.it/sql_db/app/3_0/token_firebase.php",
      data: "id_utente=" + id_utente + "&token=" + token,
      dataType: "html",
      success: function (value) {
        console.log(value);
      },
      error: function () {
        console.log("Errore update token");
      }
    });
  });
} // saveTokenFirebase()
///////////////////////////////////////////////////////////////////////////////
function adSetter() {
  console.log('adSetter');
  console.log(AdMob);
  if (AdMob) AdMob.createBanner({
    adId: admobid.banner,
    bgColor: '#35b1f1',
    bgColor: 'pink',
    overlap: false,
    adSize: 'FULL_BANNER',
    position: AdMob.AD_POSITION.BOTTOM_CENTER,
    isTesting: true, // TODO: remove this line when release
    autoShow: true
  });
  if (AdMob) AdMob.prepareInterstitial({
    adId: admobid.interstitial,
    isTesting: true, // TODO: remove this line when release
    autoShow: false
  });

} // adSetter()
///////////////////////////////////////////////////////////////////////////////
function setTimeFullAdv() {

  var y = new Date();

  var z = y.setMinutes(y.getMinutes() + 1)

  localStorage.mytime = y;

  x = new Date(localStorage.mytime);

  console.log('INZIO ' + x);

}
///////////////////////////////////////////////////////////////////////////////
controllo = false;

function FullAdv() {

  var licenza = window.localStorage.getItem('licenza-app');

  if (licenza == 'Free') {

    if (!controllo) {

      if (AdMob) AdMob.showInterstitial();
      controllo = true;

    }
    console.log('controllo: ' + controllo);
    var dataOra = new Date();

    if (dataOra > x) {
      console.log('OK APRO ADV');
      if (AdMob) AdMob.prepareInterstitial({
        adId: admobid.interstitial,
        isTesting: true, // TODO: remove this line when release
        autoShow: false
      });

      setTimeout(function () {
        if (AdMob) AdMob.showInterstitial();
      }, 5000);

      setTimeFullAdv();

    } else {
      console.log('ancora devono passare i 2 minuti');
    }
  }
}