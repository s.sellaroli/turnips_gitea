////////////////////////////////////////////////////////////////////////////////
// Versione 4.1.5 del 01-05-2019
////////////////////////////////////////////////////////////////////////////////
// Variabili Globali
var id_utente = localStorage['id_user']; // ID utente
var ver = localStorage['versione']; // Versione app
////////////////////////////////////////////////////////////////////////////////

console.log('--------------------->  calendario.js');

function caricaCalendario() {

	try {
		myApp.hidePreloader();
		console.log('%c!!!! Carico Calendario !!!!', 'color:#333; background-color: yellow;');
		// Inzializzo il tema
		var storeTurni = JSON.parse(localStorage.getItem("cal-turni"));
		var storeAssenze = JSON.parse(localStorage.getItem("cal-assenze"));
		var storeStraoEme = JSON.parse(localStorage.getItem("cal-strao-eme"));
		var storeStraoPro = JSON.parse(localStorage.getItem("cal-strao-pro"));
		var storeTurnario = JSON.parse(localStorage.getItem("turnario-array"));

		if (isEmpty(storeTurni) || isEmpty(storeAssenze) || isEmpty(storeStraoEme) || isEmpty(storeStraoPro) || isEmpty(storeTurnario)) {

			caricoEventiCalendario();

			myApp.showPreloader('Caricamento...');

			setTimeout(function () {
				caricaCalendario();
				console.log('ricaricato');

			}, 1000);

			return false;
		}

		// Array con festivita 2018
		var Festivi = [{
			"title": "Anno Nuovo",
			"start": "2020-01-01T08:30:00",
			"holiday": "1",
			"tipo": "festivo"
		}, {
			"title": "Epifania",
			"start": "2020-01-06T08:30:00",
			"holiday": "1",
			"tipo": "festivo"
		}, {
			"title": "Pasqua",
			"start": "2020-04-12T08:30:00",
			"holiday": "1",
			"tipo": "festivo"
		}, {
			"title": "Pasquetta",
			"start": "2020-04-13T08:30:00",
			"holiday": "1",
			"tipo": "festivo"
		}, {
			"title": "Liberazione",
			"start": "2020-04-25T08:30:00",
			"holiday": "1",
			"tipo": "festivo"
		}, {
			"title": "Lavoro",
			"start": "2020-05-01T08:30:00",
			"holiday": "1",
			"tipo": "festivo"
		}, {
			"title": "Repubblica",
			"start": "2020-06-02T08:30:00",
			"holiday": "1",
			"tipo": "festivo"
		}, {
			"title": "F.Agosto",
			"start": "2020-08-15T08:30:00",
			"holiday": "1",
			"tipo": "festivo"
		}, {
			"title": "T.Santi",
			"start": "2020-11-01T08:30:00",
			"holiday": "1",
			"tipo": "festivo"
		}, {
			"title": "Immacolata",
			"start": "2020-12-08T08:30:00",
			"holiday": "1",
			"tipo": "festivo"
		}, {
			"title": "Natale",
			"start": "2020-12-25T08:30:00",
			"holiday": "1",
			"tipo": "festivo"
		}, {
			"title": "S.Stefano",
			"start": "2020-12-26T08:30:00",
			"holiday": "1",
			"tipo": "festivo"
		}, {
			"title": "Capodanno",
			"start": "2021-01-01T08:30:00",
			"holiday": "1",
			"tipo": "festivo"
		}, {
			"title": "Epifania",
			"start": "2021-01-06T08:30:00",
			"holiday": "1",
			"tipo": "festivo"
		}];

		$('#calendar').swipeCalendar({
			swipeEffect: 'slide', // Can be 'slide', 'coverflow' or 'cube'
			swipeSpeed: 250,
			swipeTitlePosition: 'left',
			locale: "it",
			header: {
				left: '',
				center: '',
				right: 'today,myCustomButton'
			},
			themeSystem: 'standard',
			height: 'parent',
			defaultView: 'month',
			displayEventTime: false,
			fixedWeekCount: false,
			dayClick: function (date) {

				var dateFormat = date.format();

				var buttons1 = [{
					text: '<div class="label-modal-inserisci"><i class="fa fa-calendar" aria-hidden="true"></i>Turno di Servizio</div>',
					onClick: function () {
						mainView.router.loadPage('pages/view-turni.html');
						// Data corrente per input date
						setTimeout(function () {
							document.getElementById('giorno').value = dateFormat;
						}, 500);
					}
				}, {
					text: '<div class="label-modal-inserisci"><i class="fa fa-coffee" aria-hidden="true"></i>Assenza</div>',
					onClick: function () {
						mainView.router.loadPage('pages/view-assenze.html');
						// Data corrente per input date
						setTimeout(function () {
							document.getElementById('giorno_ass').value = dateFormat;
						}, 500);
					}
				}, {
					text: '<div class="label-modal-inserisci"><i class="fa fa-hourglass-end" aria-hidden="true"></i>Straordinario</div>',
					onClick: function () {
						mainView.router.loadPage('pages/view-straordinario.html');
						// Data corrente per input date
						setTimeout(function () {
							document.getElementById('data_straordinario').value = dateFormat;
						}, 500);
					}
				}, {
					text: '<div class="label-modal-inserisci"><i class="fa fa-calendar-check-o" aria-hidden="true"></i> Turnario</div>',
					onClick: function () {
						mainView.router.loadPage('pages/gestione-turnario.html');
					}
				}];
				var buttons2 = [{
					text: 'Annulla',
					color: 'red'
				}];
				var groups = [buttons1, buttons2];
				myApp.actions(groups);
			},
			customButtons: {

				myCustomButton: {
					text: 'Vai..',
					click: function () {

						myApp.modal({
							title: 'Vai al...',
							text: 'Inserisci la data che vuoi visualizzare',
							afterText: '<div class="list-block inset ombra">' +
								'<ul>' +
								'<li>' +
								'<div class="item-content">' +
								'<div class="item-media"><i class="fa fa-calendar" aria-hidden="true"></i></div>' +
								'<div class="item-inner">' +
								'<div class="item-input">' +
								'<input type="date" placeholder="Vai a data" id="data_vai" name="data_vai">' +
								'</div>' +
								'</div>' +
								'</div>' +
								'</li>' +
								'</ul>' +
								'</div>',
							buttons: [{
									text: '<span class="color-red">Annulla</span>',
								},
								{
									text: 'OK',
									bold: true,
									onClick: function () {

										var data_vai = $('#data_vai').val();

										if (isEmpty(data_vai)) {
											showToast(
												"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Non hai inserito la data!",
												"box-errore", "2000");

										} else {
											$('#calendar').swipeCalendar('gotoDate', data_vai);
										}
									}
								},
							]
						});

						document.getElementById('data_vai').value = dataAttuale();
					}
				}
			},
			eventClick: function (event, element) {
				if ((event.tipo == "turno") && (event.scalo != "RC")) {

					mainView.router.loadPage('pages/edit-turno.html');
					var id_utente = localStorage['id_user']; // ID utente
					var id_turno = event.id;
					// Visualizza il loader
					myApp.showPreloader('Caricamento')
					$.ajax({
						type: 'POST',
						url: 'https://turnips.it/db_sql/calendario/singolo-turno.php',
						data: {
							id_utente: id_utente,
							id_turno: id_turno,
						},
						dataType: 'json',
						success: function (msg) {
							setTimeout(function () {
								// Nasconde il loader
								myApp.hidePreloader();
								$.each(msg, function (key, value) {
									$('#after-etichetta-edit .item-after').css('background', value.colore_etichetta);
									switch (value.colore_etichetta) {
										case '#2ECC71':
											var colore_turno = 'Standard';
											break;
										case '#f44336':
											var colore_turno = 'Rosso';
											break;
										case '#2196f2':
											var colore_turno = 'Blu';
											break;
										case '#4cae50':
											var colore_turno = 'Verde';
											break;
										case '#fe9800':
											var colore_turno = 'Arancione';
											break;
										case '#3f51b4':
											var colore_turno = 'Indaco';
											break;
										case '#ccdb39':
											var colore_turno = 'Lime';
											break;
										case '#03a8f3':
											var colore_turno = 'Azzurro';
											break;
										case '#fec007':
											var colore_turno = 'Ambra';
											break;
										case '#795548':
											var colore_turno = 'Marrone';
											break;
										case '#00bbd3':
											var colore_turno = 'Ciano';
											break;
										case '#feea3b':
											var colore_turno = 'Giallo';
											break;
										case '#e81e63':
											var colore_turno = 'Rosa';
											break;
										case '#9e9e9e':
											var colore_turno = 'Grigio';
											break;
										case '#009688':
											var colore_turno = 'Verde Acqua';
											break;
										case '#9c27af':
											var colore_turno = 'Viola';
											break;
									}
									$('#after-etichetta-edit .item-after').text(colore_turno);
									$('#id_turno_edit').val(value.id_turno);
									$('#giorno-edit').val(value.giorno);
									$('#orario-edit').val(value.orario);
									$('#tipo_serv-edit').val(value.tipo_serv);
									$('#ore_notte-edit').val(value.ore_notti);
									$('#nota-edit').val(value.nota);
									$('#colore_etichetta_edit').val(value.colore_etichetta);
									// Attivo il toogle se ci sono indennità doppie
									if ($.trim(value.ticket_pasto) == "2") {
										$('#sl_idennita_edit').find('option[value="sl_ticket_pasto-edit"]').attr('selected', true);
										$('#sl_idennita_edit').find('option[value="sl_doppio_ticket_pasto-edit"]').attr('selected', true);
									};
									if ($.trim(value.mancati_pasto) == "2") {
										$('#sl_idennita_edit').find('option[value="sl_ticket_mancato_pasto-edit"]').attr('selected', true);
										$('#sl_idennita_edit').find('option[value="sl_doppio_mancato_pasto-edit"]').attr('selected', true);
									};
									if ($.trim(value.ser_esterno) == "2") {
										$('#sl_idennita_edit').find('option[value="sl_servizio_esterno-edit"]').attr('selected', true);
										$('#sl_idennita_edit').find('option[value="sl_doppio_servizio_esterno-edit"]').attr('selected', true);
									};
									// Carico le indennità nella smart select
									if ($.trim(value.recuper_riposo) == "1") {
										$('#recupero_giorno-ck-edit').prop("checked", true);
									};
									if ($.trim(value.ticket_pasto) == "1") {
										$('#sl_idennita_edit').find('option[value="sl_ticket_pasto-edit"]').attr('selected', true);
									};
									if ($.trim(value.mancati_pasto) == "1") {
										$('#sl_idennita_edit').find('option[value="sl_ticket_mancato_pasto-edit"]').attr('selected', true);
									};
									if ($.trim(value.o_p_fuori) == "1") {
										$('#sl_idennita_edit').find('option[value="sl_op_fuori-edit"]').attr('selected', true);
									};
									if ($.trim(value.o_p_cek) == "1") {
										$('#sl_idennita_edit').find('option[value="sl_op_sede-edit"]').attr('selected', true);
									};
									if ($.trim(value.o_p_pernotto) == "1") {
										$('#sl_idennita_edit').find('option[value="sl_op_pernotto-edit"]').attr('selected', true);
									};
									if ($.trim(value.ser_esterno) == "1") {
										$('#sl_idennita_edit').find('option[value="sl_servizio_esterno-edit"]').attr('selected', true);
									};
									if ($.trim(value.reperibilita) == "1") {
										$('#sl_idennita_edit').find('option[value="sl_reperibilita-edit"]').attr('selected', true);
									};
									if ($.trim(value.cambio_turno) == "1") {
										$('#sl_idennita_edit').find('option[value="sl_cambio_turno-edit"]').attr('selected', true);
									};
									if ($.trim(value.rip_com) == "1") {
										$('#sl_idennita_edit').find('option[value="sl_id_compensazione-edit"]').attr('selected', true);
									};
									if ($.trim(value.serv_missione) == "1") {
										$('#sl_idennita_edit').find('option[value="sl_servizio_missione-edit"]').attr('selected', true);
									};
									if ($.trim(value.sl_autostrada) == "1") {
										$('#sl_idennita_edit').find('option[value="sl_autostrada-edit"]').attr('selected', true);
									};
									if ($.trim(value.santo_patrono) == "1") {
										$('#sl_idennita_edit').find('option[value="sl_santo_patrono-edit"]').attr('selected', true);
									};
									// Se presente visualizzo data ultimo aggiornamento
									if ($.trim(value.data_aggiornamento) != "") {
										$("#last_up_turno").html('Ultima modifica effettuata il: ' + value.data_aggiornamento);
									};
								}); //end .each
							}, 1000); //end .timeout
						}, // Fine success
						error: function () {
							// Nasconde il loader
							myApp.hidePreloader();
							console.log('errore ajax loadTurni');
						}
					}); // Fine chiamata ajax
				} else if ((event.tipo == "turno") && (event.scalo == "RC")) {
					mainView.router.loadPage('pages/edit-scalo.html');
					var id_scalo = event.id;
					var id_utente = localStorage['id_user']; // ID utente
					// Visualizza il loader
					//console.log(id_scalo);
					myApp.showPreloader('Caricamento')
					$.ajax({
						type: 'POST',
						url: 'https://turnips.it/db_sql/calendario/singolo-scala_turno.php',
						data: {
							id_utente: id_utente,
							id_scalo: id_scalo,
						},
						dataType: 'json',
						success: function (msg) {
							setTimeout(function () {
								// Nasconde il loader
								myApp.hidePreloader();
								//console.log(msg);
								$.each(msg, function (key, value) {
									$('#id_ass_edit_scalo').val(value.id_turno);
									$('#giorno_ass_edit_scalo').val(value.giorno);
									$('#note_ass_edit_scalo').val(value.nota);
									$('#scalo_strao_edit').val(value.scalo_strao);
									// Se presente visualizzo data ultimo aggiornamento
									if ($.trim(value.data_aggiornamento) != "") {
										$("#last_up_scalo").html('Ultima modifica effettuata il: ' + value.data_aggiornamento);
									};
								}); //end .each
							}, 1000); //end .timeout
						}, // Fine success
						error: function () {
							// Nasconde il loader
							myApp.hidePreloader();
							console.log('errore ajax loadTurni');
						}
					}); // Fine chiamata ajax
				} else if (event.tipo == "assenza") {
					// Quando viene cliccato un assenza
					mainView.router.loadPage('pages/edit-assenza.html');
					var id_assenza = event.id;
					var id_utente = localStorage['id_user']; // ID utente
					//console.log(id_assenza);
					// Visualizza il loader
					myApp.showPreloader('Caricamento')
					$.ajax({
						type: 'POST',
						url: 'https://turnips.it/db_sql/calendario/singolo-assenza.php',
						data: {
							id_utente: id_utente,
							id_assenza: id_assenza,
						},
						dataType: 'json',
						success: function (msg) {
							setTimeout(function () {
								// Nasconde il loader
								myApp.hidePreloader();
								//console.log(msg);
								$.each(msg, function (key, value) {
									$('#id_ass_edit').val(value.id_ass);
									$('#giorno_ass_edit').val(value.giorno_ass);
									$("#tipo_ass_edit").val(value.tipo_ass);
									$('#note_ass_edit').val(value.note_ass);
									// Se presente visualizzo data ultimo aggiornamento
									if ($.trim(value.data_aggiornamento_ass) != "") {
										$("#last_up_ass").html('Ultima modifica effettuata il: ' + value.data_aggiornamento_ass);
									};
								}); //end .each
							}, 1000); //end .timeout
						}, // Fine success
						error: function () {
							// Nasconde il loader
							myApp.hidePreloader();
							console.log('errore ajax loadTurni');
						}
					}); // Fine chiamata ajax
				} else if (event.tipo == "strao-eme") {
					// Quando viene cliccato un assenza
					mainView.router.loadPage('pages/edit-strao-eme.html');
					var id_strao_eme = event.id;
					var id_utente = localStorage['id_user']; // ID utente
					// Visualizza il loader
					myApp.showPreloader('Caricamento')
					$.ajax({
						type: 'POST',
						url: 'https://turnips.it/db_sql/calendario/singolo-strao_emergente.php',
						data: {
							id_utente: id_utente,
							id_strao_eme: id_strao_eme,
						},
						dataType: 'json',
						success: function (msg) {
							setTimeout(function () {
								// Nasconde il loader
								myApp.hidePreloader();
								$.each(msg, function (key, value) {
									$('#id_strao_eme_edit').val(value.id_strao_incr);
									$('#data_strao_eme_edit').val(value.data_strao);
									$('#titolo_strao_eme_edit').val(value.titolo_strao_eme);
									$('#stra_eme_start_edit').val(value.ore_ecc_start);
									$('#stra_eme_end_edit').val(value.ore_ecc_end);
									$('#nota_strao_eme_edit').val(value.nota_strao_eme);
									// Se presente visualizzo data ultimo aggiornamento
									if ($.trim(value.data_edit) != "") {
										$("#last_up_ass_strao_eme").html('Ultima modifica effettuata il: ' + value.data_edit);
									};
								}); //end .each
							}, 1000); //end .timeout
						}, // Fine success
						error: function () {
							// Nasconde il loader
							myApp.hidePreloader();
							console.log('errore ajax loadTurni');
						}
					}); // Fine chiamata ajax
				} else if (event.tipo == "strao-pro") {
					// Quando viene cliccato un assenza
					mainView.router.loadPage('pages/edit-strao-pro.html');
					var id_strao_pr = event.id;
					var id_utente = localStorage['id_user']; // ID utente
					// Visualizza il loader
					myApp.showPreloader('Caricamento')
					$.ajax({
						type: 'POST',
						url: 'https://turnips.it/db_sql/calendario/singolo-strao_programmato.php',
						data: {
							id_utente: id_utente,
							id_strao_pr: id_strao_pr,
						},
						dataType: 'json',
						success: function (msg) {
							setTimeout(function () {
								// Nasconde il loader
								myApp.hidePreloader();
								$.each(msg, function (key, value) {
									$('#id_strao_pr_edit').val(value.id_strao_pr_incr);
									$('#data_strao_pr_edit').val(value.data_strao_pr);
									$('#titolo_strao_pr_edit').val(value.titolo_prog);
									$('#stra_pr_start_edit').val(value.ore_start_pr);
									$('#stra_pr_end_edit').val(value.ore_end_pr);
									$('#nota_strao_pr_edit').val(value.nota_strao_pr);
									// Se presente visualizzo data ultimo aggiornamento
									if ($.trim(value.data_edit_pr) != "") {
										$("#last_up_ass_strao_pr").html('Ultima modifica effettuata il: ' + value.data_edit_pr);
									};
								}); //end .each
							}, 1000); //end .timeout
						}, // Fine success
						error: function () {
							// Nasconde il loader
							myApp.hidePreloader();
							console.log('errore ajax loadTurni');
						}
					}); // Fine chiamata ajax
				}
			}, //Fine Click events
			//////////////////////////////////////////////////////////////////////////////////////////////////////////
			// Grafica etichetta su calendario
			eventRender: function (event, element, view) {

				if (event.tipo == "turno") {
					element.addClass('cl-turni');
					element.css('background', event.colore_etichetta);
				} else if (event.tipo == "t5") {
					element.css('color', '#2c3e50');
					element.addClass('cl-quinta');
				} else if (event.tipo == "strao-eme") {
					element.addClass('cl-stra-eme');
				} else if (event.tipo == "strao-pro") {
					element.addClass('cl-stra-pro');
				} else if (event.tipo == "festivo") {
					element.addClass('cl-festivo');
				} else {
					element.addClass('cl-assenze');
					element.css('background', event.colore_etichetta);
				}
				if ((event.title == "Scalo Strao") || (event.title == "RC")) {
					element.addClass('cl-scalo').removeClass('cl-turni');
				}
				//////////////////////////////////////////////////////////////////////////////////////////////////////////
				//Visualizza il turno in 5°
				switch (event.title) {
					case 's':
						element.find("div.fc-content").html('Sera');
						break;
					case 'p':
						element.find("div.fc-content").html('Pomeriggio');
						break;
					case 'm':
						element.find("div.fc-content").html('Mattina');
						break;
					case 'n':
						element.find("div.fc-content").html('Notte');
						break;
					case 'r':
						element.find("div.fc-content").html('<strong>RIPOSO</strong>');
						break;
				} //end switch per icone turni in quinta"start":"2017-05-01"
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// Visualizza la casella del giorno di colore rosa se si tratta di una festività (es. Natale, Ferragosto etc...)
				if (event.holiday == '1') {
					var dateString = event.start.format("YYYY-MM-DD");
					$('[data-date=' + dateString + ']').css({
						"color": "#e74c3c",
						"font-weight": "bold"
					});
				}
				// Visualizza la casella del giorno di colore azzurro se la reperibilità è = 1
				if (event.reperibilita == '1') {
					var dateString = event.start.format("YYYY-MM-DD");
					$('[data-date=' + dateString + ']').css({
						"background-color": "#daf3ff"
					});
				}
				//////////////////////////////////////////////////////////////////////////////////////////////////////
				if (event.title == "r") {
					var data_evento = event.start.format("YYYY-MM-DD HH:MM:SS");
					var data = new Date(Date.parse(data_evento));
					var giornosettimana = data.getDay();
					if (giornosettimana == 2) {
						element.find("div.fc-content").html('<span style="color:#2196f3;">Agg.Prof.<span>');
					}
				}
			},
			//Inizializzo i dati ricevuti dal database - Variabili che leggono array in localStorage

			eventSources: [storeTurni, storeAssenze, storeTurnario, storeStraoEme, storeStraoPro, Festivi]


		}); // Fine $('#calendar').fullCalendar





	} catch (ex) {
		console.log(ex)
	}


	//Hammer Js swipe
	// var cal = document.getElementById("calendar");
	// Hammer(cal).on("swipeleft", function () {
	// 	$('#calendar').fullCalendar('next');
	// });
	// Hammer(cal).on("swiperight", function () {
	// 	$('#calendar').fullCalendar('prev');
	// });
} // Fine caricaCalendario()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function loadTurni() {
	var id_utente = localStorage['id_user'];
	$.ajax({
		type: "POST",
		url: 'https://turnips.it/db_sql/calendario/10_2020/cal-turni.php',
		data: {
			id_utente: id_utente
		},
		dataType: "json",
		success: function (value) {
			localStorage.setItem("cal-turni", JSON.stringify(value));
		},
		error: function () {
			console.log('errore ajax loadTurni');
		}
	}); // ajax

	if (localStorage.getItem('cal-turni') !== null) {
		return true
	} else {
		return false
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function loadAssenze() {
	var id_utente_ass = localStorage['id_user'];
	$.ajax({
		type: "POST",
		url: 'https://turnips.it/db_sql/calendario/10_2020/cal-assenze.php',
		data: {
			id_utente: id_utente_ass
		},
		dataType: "json",
		success: function (value) {
			localStorage.setItem("cal-assenze", JSON.stringify(value));
		},
		error: function () {
			console.log('errore ajax loadAssenze');
		}
	}); // ajax

	if (localStorage.getItem('cal-assenze') !== null) {
		return true
	} else {
		return false
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function loadTurnario() {
	var id_utente = localStorage['id_user'];
	$.ajax({
		type: "POST",
		url: 'https://turnips.it/db_sql/calendario/4_1_5/cal-data_turnario.php',
		data: {
			id_utente: id_utente
		},
		dataType: "json",
		success: function (value) {

			localStorage.setItem("cal-turnario", JSON.stringify(value));
			setTimeout(function () {
				caricoTurnario();
			}, 500);
		},
		error: function () {
			console.log('errore ajax loadTurnario');
		}
	}); // ajax
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function locadStraoEme() {
	var id_utente = localStorage['id_user'];
	$.ajax({
		type: "POST",
		url: 'https://turnips.it/db_sql/calendario/10_2020/cal-strao_eme.php',
		data: {
			id_utente: id_utente
		},
		dataType: "json",
		success: function (value) {
			localStorage.setItem("cal-strao-eme", JSON.stringify(value));
		},
		error: function () {
			console.log('errore ajax loadTurni');
		}
	}); // ajax

	if (localStorage.getItem('cal-strao-eme') !== null) {
		return true
	} else {
		return false
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function locadStraoProg() {
	var id_utente = localStorage['id_user'];
	$.ajax({
		type: "POST",
		url: 'https://turnips.it/db_sql/calendario/10_2020/cal-strao_prog.php',
		data: {
			id_utente: id_utente
		},
		dataType: "json",
		success: function (value) {
			localStorage.setItem("cal-strao-pro", JSON.stringify(value));
		},
		error: function () {
			console.log('errore ajax loadTurni');
		}
	}); // ajax

	if (localStorage.getItem('cal-strao-pro') !== null) {
		return true
	} else {
		return false
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function etichetteTurnario() {
	var id_utente = localStorage['id_user'];
	$.ajax({
		type: "POST",
		url: 'https://turnips.it/db_sql/calendario/etichette_turnario.php',
		data: {
			id_utente: id_utente
		},
		dataType: "json",
		success: function (value) {
			localStorage.setItem("cal-etichette-turnario", JSON.stringify(value));
		},
		error: function () {
			console.log('errore ajax loadTurni');
		}
	}); // ajax
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function caricoTurnario() {
	var id_utente = localStorage['id_user'];
	$.ajax({
		type: "POST",
		url: 'https://turnips.it/db_sql/calendario/10_2020/carica_turnario.php',
		data: {
			id_utente: id_utente
		},
		dataType: "json",
		success: function (value) {
			localStorage.setItem("turnario-array", JSON.stringify(value[1]));
		},
		error: function () {
			console.log('errore ajax loadTurni');
		}
	}); // ajax
	if (localStorage.getItem('turnario-array') !== null) {
		return true
	} else {
		return false
	}
}

//-------------- Al momento non utilizzate perchè rallentava ora viene fatto lato server -------------------
function caricoTurnarioCICLO() {


	var arrSchemi = window.localStorage.getItem("cal-turnario");

	var schemi = JSON.parse(arrSchemi);

	var tot = new Array();

	var idGiorno = 1;

	for (var s = 0; s < schemi.length; s++) {

		var schema = JSON.parse(schemi[s].schema);
		var start = new Date(schemi[s].data_start);
		var end = new Date(schemi[s].data_end);
		var schemaCont = 0;

		if (isEmpty(schemi[s].data_end)) {

			var date = new Date(start);
			var dataY = date.setFullYear(date.getFullYear() + 3);

			var end = new Date(dataY);

		}



		for (day = start; day <= end; day.setDate(day.getDate() + 1)) {

			var turno = schema[schemaCont];
			schemaCont++

			if (schemaCont > schema.length - 1) {
				schemaCont = 0;
			}

			idGiorno++;

			var giorno = {
				'id': idGiorno,
				'title': turno,
				'start': day.toISOString().substr(0, 10) + 'T12:00:00',
				'tipo': 't5'
			}

			tot.push(giorno);

			localStorage["turnario-array"] = JSON.stringify(tot);

		}

	}

}

function caricoTurnario33() {
	var data_start = window.localStorage.getItem("turnario-data-start");
	var schema_turnario = window.localStorage.getItem("turnario-schema");
	if ((schema_turnario == null) && (data_start == null)) {
		console.log('Turnario non impostato: entrambi null');
	} else if ((schema_turnario == "null") && (data_start == "null")) {
		console.log('Turnario non impostato: entrambi "null"');
	} else if ((schema_turnario == "") && (data_start != "")) {
		setTimeout(function () {
			myApp.alert('Adesso puoi personalizzare<br>lo schema del tuo turnario.<br><span style="color:#e74c3c; font-weight:bold; font-size:14px;">Aggiornalo ora</span>', 'Gestione Turnario', function () {
				mainView.router.loadPage('pages/gestione-turnario.html');
			});
		}, 2000); //end .timeout
	} else {
		console.log('else finale di caricoTurnario()');
		var rounds = JSON.parse(schema_turnario);
		// Divido la data ricevuta in giorno,mese,anno
		var data_split = data_start.split('-');
		var giorno = parseInt(data_split[2]);
		var mese = parseInt(data_split[1]);
		var anno = parseInt(data_split[0]);
		var nGiorni = 730; // 2 anni 730
		var ret = new Array();
		var data = new Date(anno, mese - 1, giorno + 1);
		for (var i = 0; i < nGiorni; i++) {
			ret[i] = {
				'id': parseInt(Math.random() * 1000),
				'title': rounds[i % (rounds.length)],
				'start': data.toISOString().substr(0, 10) + 'T12:00:00',
				'tipo': 't5'
			}
			data = new Date(data.getFullYear(), data.getMonth(), data.getDate() + 1);
		} // fine for i
		//var json = JSON.stringify(ret);
		localStorage.setItem("turnario-array", JSON.stringify(ret));
		//return ret;
	} // else
}
//----------------------------------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Salva turno tramite chiamata ajax
function salvaDataTurnario() {
	// Dati dal form
	var id_utente = localStorage['id_user']; // ID utente
	var ver = localStorage['versione']; // Versione app
	localStorage['turnario-data-start'] = $('#start-data-turnario').val();
	// Schema scelto da utente
	var str = $('#turnario_select').val();
	// Caratteri ammessi per salvataggio schema
	var regexp = /^[spmnr,]+$/i;

	if ($('#start-data-turnario').val() === '') {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;Non hai inserito la data",
			"box-errore", "2000");
		return false;
	} else if ($('#turnario_select').val() === '') {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;Non hai inserito lo schema.",
			"box-errore", "2000");
		return false;
	} else if (!regexp.test($('#turnario_select').val())) {
		showToast(
			"<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;Hai inserito caratteri non validi.",
			"box-errore", "2000");

		myApp.alert('Puoi inserire solo le seguenti lettere:<br><strong>s p m n r</strong><br>e la<strong>&nbsp;,</strong> (virgola);', 'Attenzione!');
		return false;
	} else {

		var rounds = str.toLowerCase().split(','); // tolowercase metto tutto minuscolo
		// Salvo l'array dello schema in localStorage
		localStorage.setItem("turnario-schema", JSON.stringify(rounds));
		//////////////////////////////////////////////////////////////
		// Divido la data ricevuta in giorno,mese,anno
		var data_split = $('#start-data-turnario').val().split('-');
		var giorno = parseInt(data_split[2]);
		var mese = parseInt(data_split[1]);
		var anno = parseInt(data_split[0]);
		var nGiorni = 730; // 2 anni 730
		var ret = new Array();
		var data = new Date(anno, mese - 1, giorno + 1);
		for (var i = 0; i < nGiorni; i++) {
			ret[i] = {
				'id': parseInt(Math.random() * 1000),
				'title': rounds[i % (rounds.length)],
				'start': data.toISOString().substr(0, 10) + 'T12:00:00',
				'tipo': 't5'
			}
			data = new Date(data.getFullYear(), data.getMonth(), data.getDate() + 1);
		} // fine for i
		//var json = JSON.stringify(ret);
		// Salvo in localStorage
		localStorage.setItem("turnario-array", JSON.stringify(ret));
		// Salvo la data e lo schema nel database
		var data_start = $('#start-data-turnario').val();
		var schema = localStorage['turnario-schema'];
		myApp.showPreloader('Caricamento...')
		$.ajax({
			type: "POST",
			url: 'https://turnips.it/sql_db/app/3_8/calendario/salva_data_turnario.php',
			data: {
				id_utente: id_utente,
				data_start: data_start,
				schema: schema
			},
			dataType: "html",
			success: function (msg) {
				//console.log(msg);
				$('#btn-edit-turnario').css('display', 'block');
				$('#btn-salva-turnario').css('display', 'none');
				setTimeout(function () {
					// Nasconde il loader
					myApp.hidePreloader();
					showToast("<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Data Salvata!", "box-ok", "2000");
				}, 1000); //end .timeout
			},
			error: function () {
				console.log('Chiamata ajax fallita!');
			}
		}) // end ajax
		return ret;
	} // end else
} // salvaDataTurnario()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function caricoEventiCalendario() {
	var t = loadTurni(); // Carico turni e li salvo in localstorage
	var a = loadAssenze(); // Carico assenze e li salvo in localstorage
	var e = locadStraoEme(); // Carico straordinario emergente e li salvo in localstorage
	var p = locadStraoProg(); //Carico straordinario programmato e li salvo in localstorage
	var s = caricoTurnario(); // Carico turnario 3/5 e li salvo in localstorage versione multipla 2020

	if (t && a && e && p && s) {
		return true
	} else {
		return false
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////