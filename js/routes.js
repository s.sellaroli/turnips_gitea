/*
 *  TurniPS - verione 4.1.5
 *  Author: Salvatore Sellaroli
 *  Date: 01/05/2019
 */
////////////////////////////////////////////////////////////////////////////////
// Initialize your app
var myApp = new Framework7({
    // swipePanel: 'left',
    // swipePanelThreshold: 80,
    swipeBackPage: false
});
// If we need to use custom DOM library, let's save it to $$ variable:
var $$ = Dom7;
console.log('--------------------->  routes.js');
////////////////////////////////////////////////////////////////////////////////
// Menu inserisci nella home
//- Two groups
$$('.btn-menu-inserisci').on('click', function () {
    var buttons1 = [{
        text: '<div class="label-modal-inserisci"><i class="fa fa-calendar" aria-hidden="true"></i>Turno di Servizio</div>',
        onClick: function () {
            mainView.router.loadPage('pages/view-turni.html');
            // Data corrente per input date
            setTimeout(function () {
                document.getElementById('giorno').value = dataAttuale();
            }, 500);
        }
    }, {
        text: '<div class="label-modal-inserisci"><i class="fa fa-coffee" aria-hidden="true"></i>Assenza</div>',
        onClick: function () {
            mainView.router.loadPage('pages/view-assenze.html');
            // Data corrente per input date
            setTimeout(function () {
                document.getElementById('giorno_ass').value = dataAttuale();
            }, 500);
        }
    }, {
        text: '<div class="label-modal-inserisci"><i class="fa fa-hourglass-end" aria-hidden="true"></i>Straordinario</div>',
        onClick: function () {
            mainView.router.loadPage('pages/view-straordinario.html');
            // Data corrente per input date
            setTimeout(function () {
                document.getElementById('data_straordinario').value = dataAttuale();
            }, 500);
        }
    }, {
        text: '<div class="label-modal-inserisci"><i class="fa fa-calendar-check-o" aria-hidden="true"></i> Turnario</div>',
        onClick: function () {
            mainView.router.loadPage('pages/gestione-turnario.html');
        }
    }];
    var buttons2 = [{
        text: 'Annulla',
        color: 'red'
    }];
    var groups = [buttons1, buttons2];
    myApp.actions(groups);
});

////////////////////////////////////////////////////////////////////////////////
// Add view
var mainView = myApp.addView('.view-main', {
    // Because we want to use dynamic navbar, we need to enable it for this view:
    dynamicNavbar: true
});
/*----------  Al Caricamento della view HOME  ----------*/
myApp.onPageInit('index', function (page) {
    console.log('Index initialized');
    // Click per inserire nuovo turno o servizio
    caricaCalendario();
    sms(); // verifico se ci sono nuovi messaggi in chat
    avvisoHome();
});
/*----------  Al Caricamento della view INSERIMENTO TURNO PERSONALE  ----------*/
myApp.onPageInit('view-add-turni-personali', function (page) {
    //console.log('view-add-turni-personali initialized');
    setTurniPersonali();
});
/*----------  Al Caricamento della view INSERIMENTO TURNO PERSONALE  ----------*/
myApp.onPageInit('view-add-servizi-personali', function (page) {
    //console.log('view-add-servizi-personali initialized');
    SetServiziPersonali();
});
/*----------  Al Caricamento della view DATI UTENTE  ----------*/
myApp.onPageInit('view-dati-utente', function (page) {
    //console.log('view-dati-utente');
    //Attivo il pulsante HOME nella footer-nav
    $$('#button-home-footer').attr('href', 'index.html');
    datiPersonali();
});
/*----------  Al Caricamento della view GESTIONE ASSENZE  ----------*/
myApp.onPageInit('view-gestione-assenze', function (page) {
    //console.log('view-gestione-assenze');
    //Attivo il pulsante HOME nella footer-nav
    $$('#button-home-footer').attr('href', 'index.html');
    caricaSettingAss();
    // Banner Pubblicità
    FullAdv();
    var pickerDescribe = myApp.picker({
        input: '#picker-describe',
        rotateEffect: true,
        cols: [{
                textAlign: 'left',
                values: ('01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31').split(',')
            },
            {
                values: ('Gennaio,Febbraio,Marzo,Aprile,Maggio,Giugno,Luglio,Agosto,Settembre,Ottobre,Novembre,Dicembre').split(',')
            },
        ]
    });
});
/*---------- Al Caricamento della view INSERIMENTO TURNO ----------*/
myApp.onPageInit('view-turni', function (page) {
    //Attivo il pulsante HOME nella footer-nav
    $$('#button-home-footer').attr('href', 'index.html');
    // Banner Pubblicità
    FullAdv(); // 40sec
    // Click per inserire nuovo turno o servizio
    $('#btn-add-turno-per').on('click', function () {
        mainView.router.loadPage('pages/add-turni-personali.html');
    });
    $('#btn-add-servizio-per').on('click', function () {
        mainView.router.loadPage('pages/add-servizi-personali.html');
    });
    // Funzioni da caricare:
    turniUtente(); // Carica i turni default + quelli personali
    serviziUtente(); // Carica i servizi default + quelli personali
    // Scroll textarea per dispositivi Android fix tastiera
    if (!myApp.device.ios) {
        $$(page.container).find('input, textarea').on('focus', function (event) {
            var container = $$(event.target).closest('.page-content');
            var elementOffset = $$(event.target).offset().top;
            var pageOffset = container.scrollTop();
            var newPageOffset = pageOffset + elementOffset - 81;
            setTimeout(function () {
                container.scrollTop(newPageOffset, 300);
            }, 400);
        });
    } // Fine Scroll textarea per dispositivi Android fix tastiera
});
/*---------- Al Caricamento della view INSERIMENTO TURNO MULTIPLO ----------*/
myApp.onPageInit('view-turno-multiplo', function (page) {
    //Attivo il pulsante HOME nella footer-nav
    $$('#button-home-footer').attr('href', 'index.html');
    // Banner Pubblicità
    FullAdv(); // 40sec
    // Funzioni da caricare:
    turniUtente(); // Carica i turni default + quelli personali
    serviziUtente(); // Carica i servizi default + quelli personali
    // Turno Multiplo
    document.getElementById('giorno_inizio_multiplo').value = dataAttuale();
    document.getElementById('giorno_fine_multiplo').value = dataAttuale();
    // Toggle Aggiornamento professionale
    $('#check-agg-multiplo').on('click', function () {
        if ($('#agg-multiplo-ck').is(':checked')) {
            $('#li-agg-multiplo').css('height', '0px');
        } else {
            $('#li-agg-multiplo').css('height', '88px');
        }
    });
    // Scelta colore etichetta turno MULTIPLO
    $('#after-etichetta .item-after').addClass('etichetta-after');
    $('#after-etichetta .item-after').css('background', '#26de81');
    $('#colore_etichetta').on('change', function () {
        var colore = $(this).val();
        $('#after-etichetta .item-after').css('background', colore);
    });
    // Scelta colore etichetta "Agg. Profes."
    $('#after-etichetta-agg .item-after').addClass('etichetta-after');
    $('#after-etichetta-agg .item-after').css('background', '#26de81');
    $('#colore_etichetta_agg_prof').on('change', function () {
        var colore = $(this).val();
        $('#after-etichetta-agg .item-after').css('background', colore);
    });
    // Reset tags schema
    $('#resultTag').empty();
});
/*----------  Al Caricamento della view INSERIMENTO ASSENZA  ----------*/
myApp.onPageInit('view-assenze', function (page) {
    //Attivo il pulsante HOME nella footer-nav
    $$('#button-home-footer').attr('href', 'index.html');
    // Banner Pubblicità
    FullAdv(); // 40sec
    // Carico "Giorni di Riposo"
    caricoDayRiposoAssenza();
    // Visualizzo la data  corrente nei campi input date
    document.getElementById('giorno_ass').value = dataAttuale();
    document.getElementById('start_ass_pro').value = dataAttuale();
    document.getElementById('end_ass_pro').value = dataAttuale();
    // Toggle Assenza prolungata
    $('#check-assenza-pro').on('click', function () {
        if ($('#assenza-pro').is(':checked')) {
            $('#li-ass-lunga').css('height', '0px');
            $('#giorno_ass').attr("disabled", false);
        } else {
            $('#li-ass-lunga').css('height', '44px');
            $('#giorno_ass').attr("disabled", true);
            $('#rec-day-assenza').css('max-height', '0px');
            $('#scala-strao').css('height', '0px');
        }
    });
    // Sblocco il campo ore scalo straordinario solo se viene selezionato l'apposita voce nel menu
    $('#tipo_ass').on('change', function () {
        if (this.value == 'RC') {
            $('#scala-strao').css('height', '44px');
            // Se selezionato i toogle assenza prolungata lo disattibo
            $(':checkbox[name=assenza-pro]').prop('checked', false);
            $('#li-ass-lunga').css('height', '0px');
            $('#giorno_ass').attr("disabled", false);
        } else {
            $('#scala-strao').css('height', '0px');
        }
    })
    // Recupero risposo
    $('#tipo_ass').on('change', function () {
        if (this.value == 'Recupero Riposo') {
            // Visualizzo i giorni disponibili
            $('#rec-day-assenza').css('max-height', '500px');
            // Se il toogle assenza prolungata è attivo lo disattivo
            $(':checkbox[name=assenza-pro]').prop('checked', false);
            $('#li-ass-lunga').css('height', '0px');
            $('#giorno_ass').attr("disabled", false);
        } else {
            $('#rec-day-assenza').css('max-height', '0px');
        }
    })
    // Funzione per verficare ore scalo
    $('#scalo_strao').on('change', function () {
        verificaScaloStrao();
    })
    // Scroll textarea per dispositivi Android fix tastiera
    if (!myApp.device.ios) {
        $$(page.container).find('input, textarea').on('focus', function (event) {
            var container = $$(event.target).closest('.page-content');
            var elementOffset = $$(event.target).offset().top;
            var pageOffset = container.scrollTop();
            var newPageOffset = pageOffset + elementOffset - 81;
            setTimeout(function () {
                container.scrollTop(newPageOffset, 300);
            }, 400);
        });
    }
    // Fine Scroll textarea per dispositivi Android fix tastiera
});
/*----------  Al Caricamento della view Modifica TURNO  ----------*/
myApp.onPageInit('edit-turno', function (page) {
    //console.log('view-MODIFICA DTUNRO');
    //Attivo il pulsante HOME nella footer-nav
    $$('#button-home-footer').attr('href', 'index.html');
    turniUtenteEdit(); // Carico turni personale utente
    serviziUtenteEdit(); // Carico servizi personali utente
    // Scelta colore etichetta turno SINGOLO
    $('#after-etichetta-edit .item-after').addClass('etichetta-after');

    $('#colore_etichetta_edit').on('change', function () {
        var colore = $(this).val();
        $('#after-etichetta-edit .item-after').css('background', colore);
    });
    // Scroll textarea per dispositivi Android fix tastiera
    if (!myApp.device.ios) {
        $$(page.container).find('input, textarea').on('focus', function (event) {
            var container = $$(event.target).closest('.page-content');
            var elementOffset = $$(event.target).offset().top;
            var pageOffset = container.scrollTop();
            var newPageOffset = pageOffset + elementOffset - 81;
            setTimeout(function () {
                container.scrollTop(newPageOffset, 300);
            }, 400);
        });
    }
    // Fine Scroll textarea per dispositivi Android fix tastiera
});
/*----------  Al Caricamento della view edit-turno-rec ----------*/
myApp.onPageInit('edit-turno-rec', function (page) {
    //console.log('view-MODIFICA TUNRO RECUPERO RIPOSO');
    turniUtenteEdit(); // Carico turni personale utente
    serviziUtenteEdit(); // Carico servizi personali utente
    // Scroll textarea per dispositivi Android fix tastiera
    if (!myApp.device.ios) {
        $$(page.container).find('input, textarea').on('focus', function (event) {
            var container = $$(event.target).closest('.page-content');
            var elementOffset = $$(event.target).offset().top;
            var pageOffset = container.scrollTop();
            var newPageOffset = pageOffset + elementOffset - 81;
            setTimeout(function () {
                container.scrollTop(newPageOffset, 300);
            }, 400);
        });
    }
    // Fine Scroll textarea per dispositivi Android fix tastiera
});
/*----------  Al Caricamento della view edit-turno-rec ----------*/
myApp.onPageInit('view-straordinario', function (page) {
    //Attivo il pulsante HOME nella footer-nav
    $$('#button-home-footer').attr('href', 'index.html');
    // Scroll textarea per dispositivi Android fix tastiera
    if (!myApp.device.ios) {
        $$(page.container).find('input, textarea').on('focus', function (event) {
            var container = $$(event.target).closest('.page-content');
            var elementOffset = $$(event.target).offset().top;
            var pageOffset = container.scrollTop();
            var newPageOffset = pageOffset + elementOffset - 81;
            setTimeout(function () {
                container.scrollTop(newPageOffset, 300);
            }, 400);
        });
    }
    // Fine Scroll textarea per dispositivi Android fix tastiera
});
/*----------  Al Caricamento della view INSERIMENTO STRAO EMERGENTE  ----------*/
myApp.onPageInit('view-strao-eme', function (page) {
    //Attivo il pulsante HOME nella footer-nav
    $$('#button-home-footer').attr('href', 'index.html');
    // Visualizzo la data  corrente nei campi input date
    document.getElementById('data_strao_eme').value = dataAttuale();
    // Scroll textarea per dispositivi Android fix tastiera
    if (!myApp.device.ios) {
        $$(page.container).find('input, textarea').on('focus', function (event) {
            var container = $$(event.target).closest('.page-content');
            var elementOffset = $$(event.target).offset().top;
            var pageOffset = container.scrollTop();
            var newPageOffset = pageOffset + elementOffset - 81;
            setTimeout(function () {
                container.scrollTop(newPageOffset, 300);
            }, 400);
        });
    }
    // Fine Scroll textarea per dispositivi Android fix tastiera
});
/*----------  Al Caricamento della view INSERIMENTO STRAO PROGRAMMATO  ----------*/
myApp.onPageInit('view-strao-pr', function (page) {
    //Attivo il pulsante HOME nella footer-nav
    $$('#button-home-footer').attr('href', 'index.html');
    // Visualizzo la data  corrente nei campi input date
    document.getElementById('data_strao_pr').value = dataAttuale();
    // Scroll textarea per dispositivi Android fix tastiera
    if (!myApp.device.ios) {
        $$(page.container).find('input, textarea').on('focus', function (event) {
            var container = $$(event.target).closest('.page-content');
            var elementOffset = $$(event.target).offset().top;
            var pageOffset = container.scrollTop();
            var newPageOffset = pageOffset + elementOffset - 81;
            setTimeout(function () {
                container.scrollTop(newPageOffset, 300);
            }, 400);
        });
    }
    // Fine Scroll textarea per dispositivi Android fix tastiera
});
/*----------  Al Caricamento della view INDENNITA  ----------*/
myApp.onPageInit('ind-indennita', function (page) {
    //Attivo il pulsante HOME nella footer-nav
    $$('#button-home-footer').attr('href', 'index.html');
    inndenitaMese();
    inndenitaMeseScelta();
    // Banner Pubblicità
    FullAdv(); // 30sec
});
/*----------  Al Caricamento della view RIEPILOGO STRAO ----------*/
myApp.onPageInit('ind-strao', function (page) {
    //console.log('ind-strao');
    caricoStraordinario();
    caricoStraordinarioScelta();
    // Banner Pubblicità
    FullAdv(); // 30sec
});
/*----------  Al Caricamento della view RIEPILOGO ASSENZE ----------*/
myApp.onPageInit('ind-assenze', function (page) {
    //console.log('ind-assenze');
    caricaAssenze();
    caricaAssenzeScelta();
    // Banner Pubblicità
    FullAdv(); // 30sec

});
/*----------  Al Caricamento della view RIEPILOGO BUSTA PAGA ----------*/
myApp.onPageInit('ind-paga', function (page) {
    //console.log('busta paga');
    caricoBustaPaga();
    caricoPagaScelta();
    // Banner Pubblicità
    FullAdv(); // 30sec
});
/*----------  Al Caricamento del RIEPILOGO GIORNO DI RIPOSO ----------*/
myApp.onPageInit('recupero_riposo', function (page) {
    //console.log('recupero riposo');
    caricoDayRiposo();
    caricoDayRiposoOpen();
});
/*----------  Al Caricamento della view view-info-app----------*/
myApp.onPageInit('view-info-app', function (page) {
    //Attivo il pulsante HOME nella footer-nav
    $$('#button-home-footer').attr('href', 'index.html');
    $("#ver-app").html(localStorage['versione']);
    $("#ver-db").html(localStorage['versione-db-user']);
    //- Three groups
    $$('.ac-4').on('click', function () {
        var buttons = [{
            text: 'Facebook',
            onClick: function () {
                window.location.assign("https://facebook.com/app.turnips")
            }
        }, {
            text: 'Twitter',
            onClick: function () {
                window.location.assign("https://twitter.com/TurniPs_it")
            }
        }, {
            text: 'Annulla',
            color: 'red',
            onClick: function () {}
        }, ];
        myApp.actions(buttons);
    });

    // Spazio localstorage disponibile
    var localStorageSpace = function () {

        var data = '';

        try {
            for (var key in window.localStorage) {

                if (window.localStorage.hasOwnProperty(key)) {
                    data += window.localStorage[key];
                }
            }
        } catch (ex) {
            console.log(ex);
        }

        var spazioUsato = ((data.length * 16) / (8 * 1024)).toFixed(2);

        $('#barSpazio').val(spazioUsato);

    };

    localStorageSpace();

    $('.popup-info-spazio').on('popup:closed', function () {
        $('#liSpazioLocalstorage').empty();
    });

});
/*----------  Al Caricamento della view GESTIONE TURNARIO ----------*/
myApp.onPageInit('gestione-turnario', function (page) {
    //Attivo il pulsante HOME nella footer-nav
    $$('#button-home-footer').attr('href', 'index.html');

    $('#start-data-turnario').val(dataAttuale());
    $('#end-data-turnario').val(dataMinYear(dataAttuale(), 731));

    getTurnari();

    var idTurnario = $('#id-turnario').val();
    // Gestione Bottoni
    if (isEmpty(idTurnario)) {
        $('#btn-edit-turnario').css('display', 'none');
        $('#btn-salva-turnario').css('display', 'block');
    } else {
        $('#btn-edit-turnario').css('display', 'block');
        $('#btn-salva-turnario').css('display', 'none');
    }

    $('.popup-nuovo-turnario').on('popup:closed', function () {
        $('#form_data_turnario')[0].reset(); //Reset form
        $('#id-turnario').val('');
        $('#start-data-turnario').val(dataAttuale());
        $('#end-data-turnario').val(dataAttuale());
        $('#resultTag').empty();
    });

    // Scroll textarea per dispositivi Android fix tastiera
    if (!myApp.device.ios) {
        $$(page.container).find('input, textarea').on('focus', function (event) {
            var container = $$(event.target).closest('.page-content');
            var elementOffset = $$(event.target).offset().top;
            var pageOffset = container.scrollTop();
            var newPageOffset = pageOffset + elementOffset - 81;
            setTimeout(function () {
                container.scrollTop(newPageOffset, 300);
            }, 400);
        });
    }
});
/*----------  Al Caricamento della view CERCA TURNO TEST ----------*/
myApp.onPageInit('view-cerca', function (page) {
    //Attivo il pulsante HOME nella footer-nav
    $$('#button-home-footer').attr('href', 'index.html');
    var id_utente = localStorage['id_user']; // ID utente
    var ver = localStorage['versione']; // Versione app
    // Se cliccato su annulla elimina tutti i risultati.
    $$('.searchbar-cancel').on('click', function () {
        $("#resul-cerca").empty();
    });
    // Cerca turno
    $("#testo-cerca").keyup(function () {
        $("#resul-cerca").html("");
        var id_utente = localStorage['id_user'];
        var dati = $("#testo-cerca").val();
        $.ajax({
            type: "POST",
            url: 'https://turnips.it/sql_db/app/3_0/cerca_turni.php',
            data: {
                id_utente: id_utente,
                turno_cerca: dati
            },
            dataType: "json",
            success: function (msg) {

                if (msg == "") {
                    $('#resul-cerca').append(
                        '<p class="animated bounceInUp" style="text-align: center;font-weight: bold;">Non ci sono risultati.</p>'
                    ); // end append
                } else {
                    $.each(msg, function (key, value) {
                        //"2017-04-10"
                        var data_split = value.giorno.split('-');
                        var giorno = parseInt(data_split[2]);
                        var mese = data_split[1];
                        var anno = parseInt(data_split[0]);
                        switch (mese) {
                            case '01':
                                mm = "GEN";
                                break;
                            case '02':
                                mm = "FEB";
                                break;
                            case '03':
                                mm = "MAR";
                                break;
                            case '04':
                                mm = "APR";
                                break;
                            case '05':
                                mm = "MAG";
                                break;
                            case '06':
                                mm = "GIU";
                                break;
                            case '07':
                                mm = "LUG";
                                break;
                            case '08':
                                mm = "AGO";
                                break;
                            case '09':
                                mm = "SET";
                                break;
                            case '10':
                                mm = "OTT";
                                break;
                            case '11':
                                mm = "NOV";
                                break;
                            case '12':
                                mm = "DIC";
                                break;
                        }
                        $('#resul-cerca').append(
                            '<div class="timeline-item animated bounceInUp">' +
                            '<div class="timeline-item-date" style="width:90px;">' +
                            giorno + ' <small>' + mm + '  ' + anno + '</small></div>' +
                            '<div class="timeline-item-divider"></div>' +
                            '<div class="timeline-item-content">' +
                            '<div class="timeline-item-inner">' +
                            '<div class="timeline-item-time">' + value.orario + '</div>' +
                            value.tipo_serv +
                            ' </div>' +
                            '</div>' +
                            '</div>'
                        ); // end append
                    }); //end .each
                } // else
            },
            error: function () {
                console.log('errore connessione cerca turno');
            }
        }); //ajax
    });
});
/*----------  Al Caricamento della view CHAT ----------*/
myApp.onPageInit('chat', function (page) {
    //Attivo il pulsante HOME nella footer-nav
    $$('#button-home-footer').attr('href', 'index.html');
    if (localStorage.getItem('push_chat') != null) {
        $(':checkbox[name=ntf-chat-push]').prop('checked', true);
    }
    // Toggle attiva notifica email
    $('#chek-active-ntf-chat').on('click', function () {
        if ($('#active-ntf-chat').is(':checked')) {
            var mail = $('#email_ntf').val();
            upNtfChat(mail, 'N');
        } else {
            var mail = $('#email_ntf').val();
            upNtfChat(mail, 'Y');
        }
    });
    // Toggle attiva notifica push chat
    $('#chek-push-chat').on('click', function () {
        if ($('#ntf-chat-push').is(':checked')) {
            localStorage.removeItem("push_chat");
            FCMPlugin.unsubscribeFromTopic('pushChat');
            //alert('OFF');
        } else {
            //alert('ON');
            FCMPlugin.subscribeToTopic('pushChat');
            localStorage.setItem("push_chat", "1");
        }
    });
    // Funzioni da richiamare
    StartChat(); // Avvio la chat che si aggiorna ogni 7 sec
    ntfChat(); // Prelevo i dati per la notifica via email della chat
});
/*----------  Al Caricamento della view edit Scalo Strao/RC ----------*/
myApp.onPageInit('edit-scalo', function (page) {
    //Attivo il pulsante HOME nella footer-nav
    $$('#button-home-footer').attr('href', 'index.html');
    //console.log('edit scalo');
});
/*----------  Al Caricamento della view GUIDA APP ----------*/
myApp.onPageInit('view-tutorial', function (page) {
    caricaFaq();
});
/*----------  Al Caricamento della view SETTINGS APP ----------*/
myApp.onPageInit('view-settings', function (page) {
    //Attivo il pulsante HOME nella footer-nav
    $$('#button-home-footer').attr('href', 'index.html');
    ntfChat(); // Prelevo i dati per la notifica via email della chat
    if (localStorage.getItem('push_chat') != null) {
        $(':checkbox[name=ntf-chat-push]').prop('checked', true);
    }
    // Toggle attiva notifica email
    $('#chek-active-ntf-chat').on('click', function () {
        if ($('#active-ntf-chat').is(':checked')) {
            var mail = $('#email_ntf').val();
            upNtfChat(mail, 'N');
        } else {
            var mail = $('#email_ntf').val();
            upNtfChat(mail, 'Y');
        }
    });
    // Toggle attiva notifica push chat
    $('#chek-push-chat').on('click', function () {
        if ($('#ntf-chat-push').is(':checked')) {
            localStorage.removeItem("push_chat");
            //FCMPlugin.unsubscribeFromTopic('pushChat');
            //alert('OFF');
        } else {
            //alert('ON');
            // FCMPlugin.subscribeToTopic('pushChat');
            localStorage.setItem("push_chat", "1");
        }
    });
});
/*----------  Al Caricamento della view elimina-turni ----------*/
myApp.onPageInit('view-elimina-turni', function (page) {
    //Attivo il pulsante HOME nella footer-nav
    $$('#button-home-footer').attr('href', 'index.html');
});