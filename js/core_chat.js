/*
 *  TurniPS - verione 4.0
 *  Author: Salvatore Sellaroli
 *  Date: 06/05/2018
 */
// Scroll di tutti i messaggi verso il basso del div
console.log('--------------------->  core_chat.js');

function getMessages(letter) {
    $('#caricamento_chat').css('display', 'block');
    setTimeout(function () {
        $('#caricamento_chat').css('display', 'none');
        $("#result-chat").animate({
            scrollTop: $("#result-chat").prop('scrollHeight')
        }, 500);
    }, 1500);
} // end getMessages
//#############################################################################################################
// Avvio la funzione temporanizate per visualizzare i messaggi e aggiornare i messaggi ogni 4 secondi
function StartChat() {
    ShowMessages();
    setTimeout("getMessages();", 200);
    tm = window.setInterval('ShowMessages()', 1000);
    //console.log('avvio chat');
} // end StartChat
//#############################################################################################################
// Stop della funzione di aggiornamento messaggi
function stopMessages() {
    window.clearInterval(tm);
    console.log('stop chat');
    // Salvo ultimo accesso chat
    var id_utente = localStorage['id_user'];
    $.ajax({
        type: "POST",
        url: "https://turnips.it/db_sql/chat/ultimo_accesso.php",
        data: "id_utente=" + id_utente,
        dataType: "html",
        success: function (msg) {
            console.log(msg);
        },
        error: function () {
            console.log('Chiamata ajax fallita!');
        }
    });
} // end stopMessages
//#############################################################################################################
// Funzione per prelevare i visualizzare i messaggi
function ShowMessages() {
    //console.log('avvio ShowMessages');
    var id_utente = localStorage['id_user'];
    console.log(id_utente);
    $.ajax({
        type: "POST",
        url: "https://turnips.it/db_sql/chat/messaggi_chat.php",
        data: "id_utente=" + id_utente,
        dataType: "html",
        success: function (msg) {
            $("#result-chat").html(msg);
        },
        error: function () {
            console.log('Chiamata ajax fallita!');
        }
    });
} // end ShowMessages
//#############################################################################################################
// Funzione che si occupa di salvare i messaggi della chat
function InvioChat() {
    //Associo variabili del form
    var chat_text = $("#chat_text").val();
    // Dati utente
    var id_utente = localStorage['id_user'];
    var nome_user = localStorage['user'];
    var ver = localStorage['versione']; //versione app
    // Verifico se l'input è vuoto e blocco l'invio visualizzando il msg di errore.
    if (chat_text === '') {
        showToast("Devi inserire un messaggio!", "box-errore", "4000");
        return false;
    } //Se il campo è popolato invio la chiamata ajax per il salvataggio
    else {
        // Libero subito la textarea in modo da evitare doppi messaggi
        document.getElementById("chat_text").value = "";
        $.ajax({
            type: "POST",
            url: "https://turnips.it/db_sql/chat/salva_messaggio.php",
            data: "messaggio=" + chat_text + "&id_utente=" + id_utente + "&nome_user=" + nome_user + "&versione=" + ver,
            dataType: "json",
            success: function (value) {
                /*$('#result-chat').append(
                    '<div class="box_chat_send">' +
                    '<div class="nome-us-chat"><i class="fa fa-user"></i>&nbsp;' + value.nome_user + '</div>' +
                    '<div class="sms">' + value.messaggio + '</div>' +
                    '<div class="data-chat"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;' + value.data + '</div>' +
                    '</div>'
                    );*/
                $("#result-chat").animate({
                    scrollTop: $("#result-chat").prop('scrollHeight')
                }, 500);
            },
            error: function () {
                console.log('Chiamata ajax fallita!');
            }
        });
    } // end else
} //InvioChat
//#############################################################################################################
function sms() {
    var id_utente = localStorage['id_user'];
    $.ajax({
        type: 'POST',
        url: "https://turnips.it/db_sql/chat/verifca_nuovi_messaggi_chat.php",
        data: "id_utente=" + id_utente,
        dataType: 'json',
        success: function (value) {
            //console.log(value);
            // Data ultimo accesso in chat
            var last_accesso = value.accesso;
            // Data ultimo messaggio in chat
            var last_msg_chat = value.data_msg;
            // In base a risultato vaggio lampeggiare l'icona della chat
            if (last_msg_chat > last_accesso) {
                $('.notifica-chat').addClass('notifica-chat-attiva');
                //$('#buzzer').get(0).play(); notifica suono disabilitata dalla versione 4
                setTimeout(function () {
                    $('.notifica-chat').removeClass('notifica-chat-attiva');
                }, 5000);
            } else {
                console.log('chat niente');
                //alert(last_accesso+'-'+last_msg_chat);
            }
        }
    });
} // end sms()
//#############################################################################################################
function ntfChat() {
    // Id Utente
    var id_utente = localStorage['id_user'];
    $.ajax({
        type: 'POST',
        url: "https://turnips.it/db_sql/chat/notifica_chat.php",
        data: "id_utente=" + id_utente,
        dataType: 'json',
        success: function (value) {
            // console.log(value);
            // Data ultimo accesso in chat
            var mail = value.mail;
            // Data ultimo messaggio in chat
            var notifica = value.notifica;
            // In base a risultato vaggio lampeggiare l'icona della chat
            $('#email_ntf').val(mail);
            if (notifica === 'Y') {
                $('#active-ntf-chat').prop("checked", true);
            } else {
                $('#active-ntf-chat').prop("checked", false);
            }
        }
    });
}
//#############################################################################################################
function upNtfChat(mail, stato) {
    // Id Utente
    var id_utente = localStorage['id_user'];
    var email = mail;
    var stato_nt = stato;
    $.ajax({
        type: 'POST',
        url: "https://turnips.it/db_sql/chat/edit_notifica_chat.php",
        data: "id_utente=" + id_utente + "&email=" + email + "&stato_nt=" + stato_nt,
        dataType: "html",
        success: function (value) {
            console.log(value);
        },
        error: function () {
            console.log("Errore...");
        }
    });
}
//#############################################################################################################